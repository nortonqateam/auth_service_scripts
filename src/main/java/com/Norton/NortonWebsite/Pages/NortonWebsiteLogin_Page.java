package com.Norton.NortonWebsite.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.FakerNames;
import Utilities.LogUtil;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.google.gson.JsonObject;

public class NortonWebsiteLogin_Page {

	WebDriver driver;
	WebDriverWait wait;
	String UserName;
	String Password;

	// Finding Web Elements on WWNorton-WebSite page using PageFactory.

	@FindBy(how = How.XPATH, using = "//span[@id='profileLogin']")
	public WebElement Norton_Log_in;

	@FindBy(how = How.ID, using = "loginEmail")
	public WebElement loginEmail;
	@FindBy(how = How.ID, using = "loginPassword")
	public WebElement loginPassword;
	@FindBy(how = How.ID, using = "LoginSubmitButton")
	public WebElement LoginSubmitButton;
	@FindBy(how = How.XPATH, using = "//div[@class='panel-body']/ul/li/div/a[contains(text(),'Profile')]")
	public WebElement ProfileLink;

	@FindBy(how = How.XPATH, using = "//div[@class='panelsection']/div[contains(text(),'Name')]/following-sibling::div/div[@class='account-left-section']")
	public WebElement FirstName_LastName;

	@FindBy(how = How.XPATH, using = "//div[@class='panelsection']/div[contains(text(),'Email')]/following-sibling::div/div[@class='account-left-section']")
	public WebElement Email;

	@FindBy(how = How.ID, using = "creatAccountEmail")
	public WebElement CreateaccountEmail;
	@FindBy(how = How.ID, using = "CreateAccountSubmitButton")
	public WebElement CreateAccountSubmitButton;

	@FindBy(how = How.ID, using = "accountFirstName")
	public WebElement accountFirstName;

	@FindBy(how = How.ID, using = "accountLastName")
	public WebElement accountLastName;
	@FindBy(how = How.ID, using = "accountReTypeEmail")
	public WebElement accountReTypeEmail;

	@FindBy(how = How.ID, using = "accountPassword")
	public WebElement accountPassword;

	@FindBy(how = How.XPATH, using = "//span[@class='headerText emailHeaderText']")
	public WebElement EmailText;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement ContinueButton;

	@FindBy(how = How.ID, using = "globalInput")
	public WebElement SearchText;
	@FindBy(how = How.XPATH, using = "//button[@class='searchButton']")
	public WebElement searchButton;

	@FindBy(how = How.XPATH, using = "//div[@class='ResultTitle']/a")
	public WebElement BookTitle;

	@FindBy(how = How.XPATH, using = "//*[@id='avaliableFormatPanelGrp-body-1']/div/div[2]/div/button")
	public WebElement ViewAllOptionsbutton;

	@FindBy(how = How.XPATH, using = "//div/a[contains(text(),'Log Out')]")
	public WebElement LogOut;

	@FindBy(how = How.LINK_TEXT, using = "Forgot your password?")
	public WebElement Forgotyourpasswordlink;

	@FindBy(how = How.LINK_TEXT, using = "Change Password")
	public WebElement ChangePasswordlink;

	@FindBy(how = How.XPATH, using = "//div[@id='currentPasswordContainer']/div/input[@id='currentPassword']")
	public WebElement currentPasswordTextbox;

	@FindBy(how = How.ID, using = "accountPassword")
	public WebElement accountPasswordTextbox;

	@FindBy(how = How.ID, using = "accountRetypePassword")
	public WebElement accountRetypePasswordTextbox;

	@FindBy(how = How.ID, using = "LoginSubmitButton")
	public WebElement loginSubmitButton;

	@FindBy(how = How.ID, using = "accountEmail")
	public WebElement accountEmail;

	@FindBy(how = How.XPATH, using = "//div[@id='btnConfirmationClose']/div[@id='buttonContainer']/div/button[contains(text(),'Close')]")
	public WebElement Closebutton;

	@FindBy(how = How.XPATH, using = "//div[@class='forgotPasswordPage']/div/span[contains(text(),'Password Reset Email Has Been Sent')]")
	public WebElement PasswordResetEmailtext;

	@FindBy(how = How.XPATH, using = "//div/button[contains(text(),'Open Instructor Resources')]")
	public WebElement OpenInstructorResourcesbutton;

	@FindBy(how = How.XPATH, using = "//button[@class='Samplebutton'][contains(.,'Demo')]")
	public WebElement DemoButton;

	@FindBy(how = How.XPATH, using = "//button[@class='ViewButton'][contains(.,'View Full Site')]")
	public WebElement ViewFullSitebutton;

	// Initializing Web Driver and PageFactory.
	public NortonWebsiteLogin_Page(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// Call ReadJsonobject method to read and set node values from Json testData
	// file.

	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	// Allure Steps Annotations and Methods
	@Step("Click on Login on WWNorton Website application,  Method: {method} ")
	public void Login_NortonApplication(String userName, String Password)
			throws InterruptedException {
		this.UserName = userName;
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.elementToBeClickable(Norton_Log_in));
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Norton_Log_in);
		SignIn.click();
		SignIn.perform();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		loginEmail.sendKeys(UserName);

		loginPassword.sendKeys(Password);
		LoginSubmitButton.click();
		wait.until(ExpectedConditions.visibilityOf(ProfileLink));
		ProfileLink.click();

	}

	// Allure Steps Annotations and Methods
	@Step("Click on Login on WWNorton Website application through Dashboard,  Method: {method} ")
	public void Login_NortonApplication_Dashboard() throws InterruptedException {
		Login_NortonApplication(UserName, Password);

	}

	// Allure Steps Annotations and Methods
	@Step("Create a New Account on WWNorton Website application,  Method: {method} ")
	public String NewAccount_NortonApplication() throws InterruptedException {
		String EmailID = null;
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.elementToBeClickable(Norton_Log_in));
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Norton_Log_in);
		SignIn.click();
		SignIn.perform();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		CreateaccountEmail.click();
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();

		EmailID = FirstName + "_" + LastName + "@mailinator.com";
		// String AccountEmailID =
		// jsonobject.getAsJsonObject("NortonWebsiteLogin").get("NewAccountEmailID").getAsString();
		CreateaccountEmail.sendKeys(EmailID);
		CreateAccountSubmitButton.click();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(accountFirstName));

		accountFirstName.sendKeys(FirstName);

		accountLastName.sendKeys(LastName);
		accountReTypeEmail.sendKeys(EmailID);
		accountPassword.sendKeys(jsonobject
				.getAsJsonObject("NortonWebsiteLogin").get("Password")
				.getAsString());
		LoginSubmitButton.click();
		return EmailID;

	}

	@Step("LogOut WWNorton Website application,  Method: {method} ")
	public void Logout_NortonApplication() throws InterruptedException {
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Norton_Log_in);
		SignIn.click();
		SignIn.perform();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.elementToBeClickable(LogOut));
		Actions logout = new Actions(driver);
		logout.moveToElement(LogOut);
		logout.click();
		logout.perform();

	}

	@Step("Click Search text box,  Method: {method} ")
	public void clickSearchtextbox() {
		ReusableMethods.clickElement(driver, SearchText);

	}

	@Step("Click Search Buttonx,  Method: {method} ")
	public void clickSearchbutton() {
		ReusableMethods.clickElement(driver, searchButton);
	}

	@Step("Click Book Title from Norton WebPage,  Method: {method} ")
	public void clickBookTitle(String bookName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@class='ResultTitle']/a")));

		WebElement getExactBooName = driver.findElement(By
				.xpath("//div[@class='ResultTitle']/a/h2[./text()='" + bookName
						+ "']"));
		getExactBooName.click();
	}

	@Step("Click View All options Button from Norton WebPage,  Method: {method} ")
	public void clickViewAllOptiosnButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//*[@id='avaliableFormatPanelGrp-body-1']/div/div[2]/div/button")));
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//*[@id='avaliableFormatPanelGrp-body-1']/div/div[2]/div/button"));
		ViewAllOptionsbutton.click();
	}

	@Step("Click Change Password link,  Method: {method} ")
	public void clickChangePasswordlink() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.linkText("Change Password")));
		ReusableMethods.scrollIntoView(driver, ChangePasswordlink);
		ChangePasswordlink.click();
	}

	@Step("Enter Current Password and New Password,  Method: {method} ")
	public String enterPasswordInfo() throws InterruptedException {
		Thread.sleep(3000);
		// currentPasswordTextbox.click();
		ReusableMethods.clickElement(driver, currentPasswordTextbox);
		currentPasswordTextbox.sendKeys(jsonobject
				.getAsJsonObject("NortonWebsiteLogin").get("Password")
				.getAsString());
		String newPassword = jsonobject.getAsJsonObject("NortonWebsiteLogin")
				.get("Password").getAsString();
		ReusableMethods.clickElement(driver, accountPasswordTextbox);
		accountPasswordTextbox.sendKeys(newPassword + "1234");
		ReusableMethods.clickElement(driver, accountRetypePasswordTextbox);
		accountRetypePasswordTextbox.sendKeys(newPassword + "1234");
		loginSubmitButton.click();
		return newPassword + "1234";
	}

	@Step("Click Close button from Password Reset Window,  Method: {method} ")
	public void clickClosebutton() throws InterruptedException {
		Thread.sleep(3000);
		Closebutton.click();
	}

	@Step("Click Forgot Password Link,  Method: {method} ")
	public void clickForgotyourPassword() throws InterruptedException {
		Thread.sleep(3000);
		Forgotyourpasswordlink.click();
	}

	@Step("Enter Forgot Password Email,  Method: {method} ")
	public void submitForgotPasswordEmail(String EmailID)
			throws InterruptedException {
		Thread.sleep(3000);
		accountEmail.click();
		accountEmail.sendKeys(EmailID);
		loginSubmitButton.click();
	}

	@Step("Verify Password Reset Email Has Been Sent page is displayed,  Method: {method} ")
	public String resetPasswordText(String Text) {
		return Text = PasswordResetEmailtext.getText();
	}

	@Step("Enter New  Password after clicking the reset link from Mailinator,  Method: {method} ")
	public String enterNewPasswordInfo() throws InterruptedException {
		Thread.sleep(3000);
		// currentPasswordTextbox.click();
		ReusableMethods.clickElement(driver, accountPasswordTextbox);
		String newPassword = jsonobject.getAsJsonObject("NortonWebsiteLogin")
				.get("Password").getAsString();
		accountPasswordTextbox.sendKeys(newPassword + "1234");
		ReusableMethods.clickElement(driver, accountRetypePasswordTextbox);
		accountRetypePasswordTextbox.sendKeys(newPassword + "1234");
		boolean cookiepopup = driver.findElements(By.xpath("//*[name()='svg' and @class='svg-search-clear']")).size()!=0;
		if(cookiepopup==true){
			driver.findElement(By.xpath("//*[name()='svg' and @class='svg-search-clear']")).click();
			Thread.sleep(2000);
			loginSubmitButton.click();
		} else {
		loginSubmitButton.click();
		}
		return newPassword + "1234";
	}

	@Step("Click the Open Instructor resources Buttonr,  Method: {method} ")
	public void clickOpenInstructorResourcesButton() {
		List<WebElement> openInstResources = driver
				.findElements(By
						.xpath("//div[@class='headerDefault ']/h4[contains(text(),'Instructor Resources')]/following::div[@class='row']/div/div/div/button"));
		for (int i = 0; i < openInstResources.size();) {
			ReusableMethods.scrollIntoView(driver, openInstResources.get(i));
			openInstResources.get(0).click();
			break;
		}

	}

	@Step("Click the Demo Button from Instructor resources page,  Method: {method} ")
	public void clickDemobutton() {
		ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='Samplebutton'][contains(.,'Demo')]"));
		DemoButton.click();
	}

	@Step("Click the View Full site  Button from Instructor resources page,  Method: {method} ")
	public void clickViewFullSite() {
		ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='ViewButton'][contains(.,'View Full Site')]"));
		ViewFullSitebutton.click();
	}
	
	@Step("Get IIG demo text,  Method: {method} ")
	public String getDemoTextonIIG() {
		WebElement getDemoText= driver.findElement(By.xpath("//div[@class='status-label']/div"));
		return getDemoText.getText();
	}
	
	@Step("Verify  Header Title on IIG,  Method: {method} ")
	public String getHeaderTitleIIG() {
		WebElement getHeaderTitlet= driver.findElement(By.xpath("//div[@class='header-title']/a"));
		return getHeaderTitlet.getText();
	}
	
	@Step("Verify  Header Title on IIG,  Method: {method} ")
	public String getUserInfoIIG() {
		String userName =driver.findElement(By.className("loginText")).getText();
		LogUtil.log(userName);
		return userName;
		
	}
	
	@Step("LogOut IIG,  Method: {method} ")
	public void logOutIIG() throws InterruptedException{
		driver.findElement(By.className("dropbtn")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//a[@id='lnkLogout']")).click();	
	}
	
	@Step("get Book details from Norton page,  Method: {method} ")
	public String getBookdetails(){
		WebElement getBookinfo = driver.findElement(By.xpath("//div[@id='bookDetailsText']/header/div/h1"));
		return getBookinfo.getText();
	}
}
