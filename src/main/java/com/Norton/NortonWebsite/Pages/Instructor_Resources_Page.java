package com.Norton.NortonWebsite.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Instructor_Resources_Page {

	
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using="//div[@class='overview']/h1")
	public WebElement instructor_resources_title; 
	
	@FindBy(how = How.XPATH, using="//div/h2[@class='irbooktitle']")
	public WebElement book_title; 
	
	
	// Initializing Web Driver and PageFactory.
		public Instructor_Resources_Page(WebDriver driver){
			
			this.driver = driver;
			PageFactory.initElements(driver, this);
		
		}
}
