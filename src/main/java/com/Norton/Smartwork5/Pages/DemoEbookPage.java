package com.Norton.Smartwork5.Pages;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.ReadJsonFile;

import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;

public class DemoEbookPage {
	WebDriver driver;
	WebDriverWait wait;
	NciaSignInPage signinpage;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Ebook')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement ClickEbook; 
	@FindBy(how = How.XPATH, using ="//a[@class='product_info_demo live_activity ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span")
	public	WebElement DemoOption; 
	@FindBy(how =How.ID, using ="name")
	public WebElement StudentName;
	@FindBy(how =How.ID, using ="email")
	public WebElement Studentemail;
	@FindBy(how =How.ID, using ="school")
	public WebElement Studentschool;
	@FindBy(how = How.XPATH, using ="//button[@id='login_dialog_button']/span")
	public WebElement ViewDemoButton;
	@FindBy(how = How.XPATH, using ="//button[@id='account_menu_btn']/span")
	public WebElement DemoUserName;
	
	@FindBy(how =How.XPATH, using ="//button[@id='account_menu_btn']")
	public WebElement account_menu_button;
	
	@FindBy(how =How.XPATH, using ="//button[@id='account_menu_0']/span[contains(text(),'Sign in')]")
	public WebElement Sign_in_button;
	
public DemoEbookPage(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
	
ReadJsonFile readJasonObject = new ReadJsonFile();
JsonObject jsonobject = readJasonObject.readJason();


//Demo Page
	@Step("Click on Ebook Demo Button,  Method: {method} ")
	public void clickDemoLink() throws Exception {
		
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions.elementToBeClickable(ClickEbook));
		
		Actions ebook = new Actions(driver);
		ebook.moveToElement(ClickEbook).click().build().perform();
		
		
		wait.until(ExpectedConditions.elementToBeClickable(DemoOption));
		
		Actions demo = new Actions(driver);
		demo.moveToElement(DemoOption).click().build().perform();
		
	}
	
	//Demo User Page and Click the SIgn in Button
	@Step("Navigate to Demo Page and Click the SignIn Button,  Method: {method} ")
	public void DemoPage() throws Exception {
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(10);
		String ChildWindow = null;
        Set<String> Windowhandles= driver.getWindowHandles();
        String ParentWindow = driver.getWindowHandle();
        Windowhandles.remove(ParentWindow);
        String winHandle=Windowhandles.iterator().next();
        if (winHandle!=ParentWindow){
        	ChildWindow=winHandle;
        }
        driver.switchTo().window(ChildWindow);
        //wait.until(ExpectedConditions.visibilityOf(StudentName));
        Thread.sleep(8000);
 
        /*StudentName.sendKeys(jsonobject.getAsJsonObject("EbookDemoUser").get("Studentname").getAsString());
        Studentemail.sendKeys(jsonobject.getAsJsonObject("EbookDemoUser").get("StudentEmail").getAsString());
        Studentschool.sendKeys(jsonobject.getAsJsonObject("EbookDemoUser").get("StudentSchool").getAsString());
        ViewDemoButton.click();*/
        WebDriverWait wait = new WebDriverWait(driver, 5000L);
        wait.until(ExpectedConditions.elementToBeClickable(DemoUserName));
        String User_Name =DemoUserName.getText();
        Assert.assertEquals("Demo User", User_Name);
        Thread.sleep(3000);
        wait.until(ExpectedConditions.visibilityOf(account_menu_button));
        account_menu_button.click();
        Thread.sleep(3000);
        Sign_in_button.click();
        driver.switchTo().window(ParentWindow);
	}
	
	//After clicking Sign in from Demo Page , User Navigates back to DLP Page
	@Step("Navigate Back to DLP Page,  Method: {method} ")
	public void SignIn() throws Exception {
		wait = new WebDriverWait(driver,50);
		TimeUnit.SECONDS.sleep(10);
        signinpage = new NciaSignInPage(driver);
        wait.until(ExpectedConditions.visibilityOf(signinpage.Sign_in_or_Register));
        signinpage.clickSignin_Register();
        Thread.sleep(2000);
        
        
	}
	
	
	
	
	
   
}
