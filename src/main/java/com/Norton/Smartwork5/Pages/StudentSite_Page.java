package com.Norton.Smartwork5.Pages;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class StudentSite_Page {
	WebDriver driver;
	
	@FindBy(how =How.XPATH, using ="//ul[@class='lv-1 content-list']/li[1]")
	public	WebElement ClickContentlink; 
	
	@FindBy(how =How.XPATH, using ="//div[@class='buttons row']/a[@data-content_id='1ConceptChallengeQue']")
	public	WebElement DataContentIDlink; 
		
	@FindBy(how =How.XPATH, using="/html/body/div/h2")
	public	WebElement GetText; 
	
	
	
public StudentSite_Page(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}

public void VerifyContentTable() throws InterruptedException{
	ClickContentlink.click();
	DataContentIDlink.click();
	String ChildWindow = null;
    Set<String> Windowhandles= driver.getWindowHandles();
    String ParentWindow = driver.getWindowHandle();
    Windowhandles.remove(ParentWindow);
    String winHandle=Windowhandles.iterator().next();
    if (winHandle!=ParentWindow){
    	ChildWindow=winHandle;
    }
    driver.switchTo().window(ChildWindow);
    Thread.sleep(9000);
    String url = driver.getCurrentUrl();
	System.out.println(url);
	String title = driver.getTitle();
	System.out.println(title);
	String Text =GetText.getText();
	System.out.println(Text);
	driver.switchTo().window(ChildWindow).close();
	driver.switchTo().window(ParentWindow);
}


}
