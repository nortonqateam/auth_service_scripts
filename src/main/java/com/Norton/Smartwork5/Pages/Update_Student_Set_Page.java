package com.Norton.Smartwork5.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Update_Student_Set_Page {
	
WebDriver driver;
	
	@FindBy(how =How.XPATH, using ="//button[@id='edit_student_set_add_member']/span[contains(text(),'Add Member')]")
	public WebElement AddMemberButton;
	
	@FindBy(how =How.ID, using ="edit_student_set_title")
	public WebElement StudentSetTitle;
	
	//Add New Member to Student Set elements
	@FindBy(how =How.ID, using ="member-role-select")
	public WebElement Memberroleselect;
	
	@FindBy(how =How.ID, using="edit_student_set_add_member_email")
	public WebElement EmailAddressTextbox;
	
	@FindBy(how =How.ID, using="edit_student_set_lookup_button")
	public WebElement LookupButton;
	
	@FindBy(how =How.ID, using="edit_student_set_member_names")
	public WebElement StudentMemeberName;
	
	@FindBy(how =How.XPATH, using="//button[@type='button']/span[contains(text(),'Add')]")
	public WebElement AddButton;
	
		
	@FindBy(how =How.XPATH, using="//a[@class='link-button']/b[contains(text(),'Instructors/TAs')]")
	public WebElement InstructorsTAslinktab;
	
	@FindBy(how =How.XPATH, using ="//table[@id='edit_student_set_instructor_table']/tbody")
	public WebElement studentsetinsttable;
		
	@FindBy(how =How.NAME, using="chk-instructor")
	public WebElement InstructorsTAscheckbox;
	
	@FindBy(how =How.XPATH, using="//button[@type='button']/span[contains(text(),'Save')]")
	public WebElement SaveButton;
	
	@FindBy(how =How.XPATH, using ="//table[@id='student_set_data_table']/tbody/tr")
	public WebElement student_set_data_table;
	
	
}
