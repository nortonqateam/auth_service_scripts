package com.Norton.Smartwork5.Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.ReadJsonFile;

import com.google.gson.JsonObject;

public class GearMenu_Page {
	
	WebDriver driver;
	
	@FindBy(how =How.XPATH, using ="//li[@role='menuitem']/a[contains(text(),'Change Your Email')]")
	public WebElement Change_Your_Email_link;
	
	@FindBy(how =How.XPATH, using ="//li[@role='menuitem']/a[contains(text(),'Change Your Password')]")
	public WebElement Change_Your_Password_link;
	
	@FindBy(how =How.XPATH, using ="//li[@role='menuitem']/a[contains(text(),'Connected to LMS')]")
	public WebElement ConnectedtoLMS_link;
	
	@FindBy(how =How.XPATH, using ="//li[@role='menuitem']/a[contains(text(),'Add Yourself to a Student Set')]")
	public WebElement Add_Yourself_Student_set_link;
	
	@FindBy(how =How.XPATH, using ="//li[@role='menuitem']/a[contains(text(),'Request Support')]")
	public  WebElement Request_Support_link;
	
	@FindBy(how =How.XPATH, using ="//li[@role='menuitem']/a[contains(text(),'Help')]")
	public  WebElement Help_link;
	
	@FindBy(how =How.ID, using ="change_email_current")
	public  WebElement Current_email_address_Textbox;
	
	@FindBy(how =How.ID, using ="change_email_new")
	public  WebElement change_email_new_Textbox;
	
	@FindBy(how =How.ID, using ="change_email_password")
	public  WebElement change_email_password_Textbox;

	@FindBy(how =How.XPATH, using ="//button[@type='button']/span[contains(text(),'Submit')]")
	public  WebElement Submit_button;
	
	@FindBy(how =How.ID, using ="ncp_current_password")
	public  WebElement current_password_Textbox;
	
	@FindBy(how =How.ID, using ="ncp_new_password")
	public  WebElement new_password_Textbox;
	
	@FindBy(how =How.ID, using ="ncp_new_password_repeat")
	public  WebElement reset_new_password_Textbox;
	
	@FindBy(how =How.XPATH, using ="//button[@type='button']/span[contains(text(),'Change Password')]")
	public  WebElement changePassword_button;
	
	@FindBy(how =How.XPATH, using ="//div[@id='alert_dialog_outer_1']")
	public  WebElement PasswordChangedText;
	
	@FindBy(how =How.XPATH, using ="//button[@type='button']/span[contains(text(),'OK')]")
	public  WebElement Ok_button;
	
	//Manage Student Sets link on gearMenu
	@FindBy(how =How.XPATH, using ="//li[@class='ui-menu-item']/a[contains(text(),'Manage Student Sets')]")
	public WebElement ManageStudetSets;
	
	//Authorize an Instructor Account link on gearMenu
	@FindBy(how =How.XPATH, using ="//li[@class='ui-menu-item']/a[contains(text(),'Authorize an Instructor Account')]")
	public  WebElement AuthorizeInstructorAccount;
	
	
	
public GearMenu_Page(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}

ReadJsonFile readJasonObject = new ReadJsonFile();
JsonObject jsonobject = readJasonObject.readJason();


//Allure Steps Annotations and Methods
	@Step("Click Change your Email,  Method: {method} ")
	public void change_Email() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(Change_Your_Email_link));
		Change_Your_Email_link.click();
	}
	
	@Step("Click update your Email,  Method: {method} ")
	public void update_Email() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(Current_email_address_Textbox));
		String currentEmailID=jsonobject.getAsJsonObject("Change_Your_Email").get("Current_EmailID").getAsString();
		Current_email_address_Textbox.sendKeys(currentEmailID);
		String newEmailID=jsonobject.getAsJsonObject("Change_Your_Email").get("New_EmailID").getAsString();
		change_email_new_Textbox.sendKeys(newEmailID);
		String password=jsonobject.getAsJsonObject("Change_Your_Email").get("Password").getAsString();
		change_email_password_Textbox.sendKeys(password);
		Submit_button.click();
	}
	
	@Step("Click Change your Passwordlink,  Method: {method} ")
	public void change_password() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(Change_Your_Password_link));
		Change_Your_Password_link.click();
	}
	
	@Step("Click reset your Password,  Method: {method} ")
	public void reset_password() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		
		wait.until(ExpectedConditions.visibilityOf(current_password_Textbox));
		String currentpassword=jsonobject.getAsJsonObject("Change_Your_Password").get("CurrentPassword").getAsString();
		current_password_Textbox.sendKeys(currentpassword);
		String newpassword=jsonobject.getAsJsonObject("Change_Your_Password").get("NewPassword").getAsString();
		new_password_Textbox.sendKeys(newpassword);
		String resetpassword=jsonobject.getAsJsonObject("Change_Your_Password").get("ResetPassword").getAsString();
		reset_new_password_Textbox.sendKeys(resetpassword);
		changePassword_button.click();
		wait.until(ExpectedConditions.visibilityOf(PasswordChangedText));
		Ok_button.click();
	}
	
	@Step("Click Connected to LMS link,  Method: {method} ")
	public void clickConnectedtoLMSlink() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(ConnectedtoLMS_link));
		ConnectedtoLMS_link.click();
	}
	
	@Step("Click on Manage StudentSet Link button,  Method: {method} ")
	public void clickManageStudentSetlink() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ManageStudetSets));
		ManageStudetSets.click();
	}
}
