package com.Norton.Smartwork5.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;

public class NciaSignInPage {

	WebDriver driver;
	static String UserName_Top;
	WebDriverWait wait;

	// Finding Web Elements on NCIA Home page using PageFactory.

	@FindBy(how = How.XPATH, using = "//div[@id='loading_screen']")
	public WebElement Loader;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public WebElement Sign_in_or_Register;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_login']/span/span[@class='ncia_button_color_background']")
	public WebElement register_Login_choice;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	public WebElement Username;

	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	public WebElement Password;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton;

	@FindBy(how = How.XPATH, using = "//span[@id='gear_button_username']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_register']/span/span")
	public WebElement Need_to_register;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_login']/span/span")
	public WebElement yes_SignIn;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_login']")
	public WebElement yes_SignIn1;

	@FindBy(how = How.XPATH, using = "//*[@id='gear_button_username']")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;

	@FindBy(how = How.XPATH, using = "//button[@id='purchase_options_button']")
	public WebElement purchase_options_button;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_top_msg']")
	public WebElement login_Dialog_msg;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_purchase_info']/span[@id='login_dialog_purchase_info_msg']")
	public WebElement purchase_text;

	@FindBy(how = How.XPATH, using = "//td[@class=' title_td']/a")
	public WebElement Assignmentlink;

	@FindBy(how = How.XPATH, using = "//*[@id='main-content']/div/div[2]/div[2]/h1/span[1]")
	public WebElement AssignmentText;

	@FindBy(how = How.XPATH, using = "//div[@id='activity_list_table_wrapper']/table/tbody/tr/td[@class=' title_td']/a[1]")
	public WebElement ZAPSAssignmentlink;

	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement TrialErrorMessage;

	@FindBy(how = How.ID, using = "register_purchase_choice_register")
	public WebElement registration_code_option;

	@FindBy(how = How.ID, using = "access_code")
	public WebElement AccessCode;
	
	@FindBy(how = How.XPATH, using ="//div[@id='login_dialog_forgot_password_link']/a")
	public WebElement ForgotPasswordlink;
	
	@FindBy(how = How.ID, using ="reset_password_email")
	public WebElement reset_password_emailTextbox;
	
	@FindBy(how = How.XPATH, using ="//button[@type='button']/span[contains(text(),'Reset Password')]")
	public WebElement resetPasswordbutton;
	
	@FindBy(how = How.XPATH, using ="//button[@id='login_dialog_reg_go_button']")
	public WebElement getFreeItemsButton;
	
	@FindBy(how = How.XPATH, using ="//button[@id='login_dialog_reg_go_button']/span[contains(normalize-space(.),'Show Purchasing Options')]")
	public WebElement ShowPurchasingOptionsButton;
	
	// Initializing Web Driver and PageFactory.
	public NciaSignInPage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// Call ReadJsonobject method to read and set node values from Json testData
	// file.

	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	// Allure Steps Annotations and Methods
	@Step("Click on Sign in or Register,  Method: {method} ")
	public void clickSignin_Register() throws Exception {

		wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);

		wait.until(ExpectedConditions.visibilityOf(Sign_in_or_Register));

		Actions act = new Actions(driver);
		act.moveToElement(Sign_in_or_Register).click().build().perform();

	}

	@Step("exiting user having active registration code,  Method: {method} ")
	public void existingUser_RegCode() throws Exception {

		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);

		// wait.until(ExpectedConditions.visibilityOf(yes_SignIn));
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(yes_SignIn);
		SignIn.click();
		SignIn.perform();

		String UserName = jsonobject.getAsJsonObject("ExistingUser_RegCode")
				.get("loginemail").getAsString();
		Username.sendKeys(UserName);
		Password.sendKeys(jsonobject.getAsJsonObject("ExistingUser_RegCode")
				.get("loginpassword").getAsString());
		SignInButton.click();
		String actualUserName = UserName.toLowerCase();
		Thread.sleep(5000);
		String expectedUserName = username.getText();

		Assert.assertEquals(expectedUserName, actualUserName);

	}

	@Step("No, I need to register, purchase, or sign up for trial access.,  Method: {method} ")
	public void need_to_Register() throws Exception {
		Actions act = new Actions(driver);
		act.moveToElement(Need_to_register).click().build().perform();
		SignInButton.click();

	}

	@Step("Signin to User Account,  Method: {method} ")
	public void signInUserAccount(String jsonUserName, String jsonPassword) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);

		Actions act = new Actions(driver);
		act.moveToElement(register_Login_choice).click().build().perform();
		Username.clear();
		Username.sendKeys(jsonUserName);
     	Password.sendKeys(jsonPassword);
				
				
		wait.until(ExpectedConditions.visibilityOf(SignInButton));
		SignInButton.click();

	}

	@Step("Verify User Name,  Method: {method} ")
	public void verify_Username(String userName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(username));
		String actualUserName = userName;

		String expectedUserName = username.getText();

		Assert.assertEquals(expectedUserName.toLowerCase().trim(), actualUserName.toLowerCase().trim());

	}

	@Step("SignOut the User Name,  Method: {method} ")
	public void SignOut() throws InterruptedException {
		NciaSignInPage page = new NciaSignInPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(page.gear_button_username));
		page.gear_button_username.click();
		SignOut_link.click();
	}

	@Step("Click Ok Button,  Method: {method} ")
	public void clickOkButton(){
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'OK')]")));
		WebElement clickOkbutton = driver.findElement(
				By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'OK')]"));
		clickOkbutton.click();
	}
	@Step("User reset the password,  Method: {method} ")
	public void resetPassword(String emailID) throws InterruptedException{
		ForgotPasswordlink.click();
		Thread.sleep(5000);
		reset_password_emailTextbox.sendKeys(emailID);
		resetPasswordbutton.click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	@Step("User Click Purchase Option Button,  Method: {method} ")
	public void clickPurchaseOption() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(purchase_options_button));
		purchase_options_button.click();
	}
	
	@Step("User Purchase the product which is Free,Method: {method} ")
	public void checkPurchaseCheckbox(String optionText) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='login_dialog']/div[@id='login_dialog_inner']")));
		List<WebElement> purchaseOptionList = driver.findElements(By.xpath("//div[@id='login_dialog']/div[@id='login_dialog_inner']//div[@class='login_dialog_purchase_option_div']/table/tbody/tr"));
		for(int i=0; i<purchaseOptionList.size(); i++){
			List<WebElement> colVals = purchaseOptionList.get(i).findElements(By.xpath("//td[@class='login_dialog_purchase_price_td']/span"));
		
			java.util.Iterator<WebElement> purchasecol = colVals.iterator();
			WebElement val = null;
			while(purchasecol.hasNext()) {
	            val = purchasecol.next();
	            System.out.println(val.getText());
	         
			if(val.getText().equalsIgnoreCase(optionText)){
				WebElement purchaseOptionCheckbox =driver.findElement(By.xpath("//td[@class='login_dialog_purchase_price_td']/span[contains(text(),'"+optionText+"')]/following::td[@class='login_dialog_purchase_option_checkbox_td']/input[@class='purchase_option_checkbox']"));
				purchaseOptionCheckbox.click();
				
			}
				
			}
			break;
		}
	}
	@Step("User Click the get free items button,Method: {method} ")
	public void clickGetFreeItemsButton() {
		ReusableMethods.scrollIntoView(driver, getFreeItemsButton);
	getFreeItemsButton.click();
	}
	
	@Step("User Click Show Purchasing option button,Method: {method} ")
	public void clickShowPurchaseOptionButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ShowPurchasingOptionsButton));
		ShowPurchasingOptionsButton.click();
	}
}
