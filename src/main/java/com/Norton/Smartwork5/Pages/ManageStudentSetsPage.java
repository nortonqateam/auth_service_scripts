package com.Norton.Smartwork5.Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ManageStudentSetsPage {
	
WebDriver driver;
	
	@FindBy(how =How.XPATH, using ="//div[@id='add_new_student_set']/span[contains(text(),'Create New Student Set')]")
	public WebElement Create_New_Student_Set_button;
	
	@FindBy(how =How.XPATH, using ="//div[@id='student_set_data_table_filter']/label/input[@type='search']")
	public WebElement Search_Text;
	
	@FindBy(how =How.XPATH, using ="//a[@role='button']/span[contains(text(),'Update')]")
	public WebElement Updatebutton;
	
	@FindBy(how =How.XPATH, using ="//a[@role='button']/span[contains(text(),'x')]")
	public WebElement Deletebutton;
	
	@FindBy(how =How.CSS, using =".link-close")
	public WebElement closelink;
  
	@FindBy(how =How.XPATH, using ="//div[@id='general_dialog_outer_2']/a[1]")
	public WebElement closelink1;

public ManageStudentSetsPage(WebDriver driver){
	
	this.driver = driver;
	PageFactory.initElements(driver, this);

}

public void searchStudentID(){
	Search_Text.click();
	Updatebutton.click();
}

public void clickcloselink() throws InterruptedException{
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("$('a[class^=\"link-close\"]').click()");
	
}

}