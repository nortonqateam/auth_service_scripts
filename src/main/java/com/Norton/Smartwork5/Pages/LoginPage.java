package com.Norton.Smartwork5.Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.google.gson.JsonObject;


public class LoginPage {

	WebDriver driver;

	// Finding Web Elements on SW5 Login page using PageFactory.

	@FindBy(how = How.XPATH, using = "//button[@id='login_button']/span")
	WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//input[@id='username']")
	public WebElement email;

	@FindBy(how = How.XPATH, using = "//input[@id='password']")
	WebElement password;

	@FindBy(how = How.XPATH, using = "//*[@id=\"login_dialog_button\"]")
	WebElement signInButton;

	@FindBy(how = How.XPATH, using = "//span[@id='gear_button_username']")
	WebElement userName;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	WebElement Signin_or_Register;

	// @FindBy(how = How.XPATH, using =
	// "//button[contains(@class,'activity_group_selector swfb')]//img")
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Smartwork5')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public WebElement SW5Icon;

	@FindBy(how = How.XPATH, using = "/html/body/div[6]")
	WebElement selectSSPopUp;

	// "/html/body/div[6]/div[3]/div/button
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'OK')]")
	WebElement selectSSOkButton;

	// @FindBy(how = How.XPATH, using = "/html/body/div[7]/div[3]/div/button")
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'OK')]")
	WebElement copiedSSOkButton;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text']//img")
	WebElement gearMenuIcon;

	@FindBy(how = How.XPATH, using = "//b[contains(text(),'Sign Out')]")
	WebElement signOut;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_register']/span/span")
	public WebElement Need_to_register;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton;

	@FindBy(how = How.XPATH, using = "//*[@id='gear_button_username']")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;

	@FindBy(how = How.CLASS_NAME, using = "ui-progressbar-overlay")
	public WebElement Overlay;
	
	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement okButton;

	// Initializing Web Driver and PageFactory.

	public LoginPage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// Call to CSV data file to read test data

	ReadJsonFile readJsonObject = new ReadJsonFile();
	JsonObject jsonObj = readJsonObject.readJason();

	@Step("Login to SW5 application,  Method: {method} ")
	public void loginSW5(String userName, String Password) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		TimeUnit.SECONDS.sleep(3);
		ReusableMethods.checkPageIsReady(PropertiesFile.driver);
		try {

//			wait.until(ExpectedConditions.visibilityOf(loginButton));
//			boolean loginButtonExist = loginButton.isDisplayed();
//
//			if (loginButtonExist == true) {
//				loginButton.click();
//			}
			
			wait.until(ExpectedConditions.elementToBeClickable(Signin_or_Register));
			Signin_or_Register.click();			

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username']")));
		email.clear();
		email.sendKeys(userName);
		// Assert.assertTrue(true,"The UserName is displayed as " +userName);
		// saveTextLog("The UserName is displayed as " ,userName);
		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys(Password);

		wait.until(ExpectedConditions.visibilityOf(signInButton));
		signInButton.click();
		wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.invisibilityOf(Overlay));

		try {
			boolean okButtonExist = okButton.isDisplayed();

				if (okButtonExist == true) {
					okButton.click();
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		

	}

	/**
	 * @param string
	 * @param userName2
	 */
	@Step("Verify Signin details with userName: {0}, LName: {1}, for Method:{method} step...")
	public void verifySignin(String uName, String LName) {
		Assert.assertEquals(uName, LName);

	}

	@Step("Click the Smartwork Icon from the DLP page,  Method: {method} ")
	public void clickSW5Icon() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.visibilityOf(SW5Icon));
		SW5Icon.click();

		try {

			wait.until(ExpectedConditions.visibilityOf(copiedSSOkButton));
			boolean selectSSPopUpExist = copiedSSOkButton.isDisplayed();

			if (selectSSPopUpExist == true) {
				copiedSSOkButton.click();
			}

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}

	@Step("Click on Ok button if exist,  Method: {method} ")
	public void clickOKButton() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(3);

		ReusableMethods.checkPageIsReady(PropertiesFile.driver);

		try {

			wait.until(ExpectedConditions.visibilityOf(selectSSPopUp));
			boolean selectSSPopUpExist = selectSSPopUp.isDisplayed();

			if (selectSSPopUpExist == true) {
				selectSSOkButton.click();
			}

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}

	@Step("Logout of SW5 application,  Method: {method} ")
	public void logoutSW5() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.elementToBeClickable(gearMenuIcon));
		gearMenuIcon.click();

		wait.until(ExpectedConditions.elementToBeClickable(signOut));
		signOut.click();
		wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(loginButton),
				ExpectedConditions.elementToBeClickable(Signin_or_Register)));
	}

}
