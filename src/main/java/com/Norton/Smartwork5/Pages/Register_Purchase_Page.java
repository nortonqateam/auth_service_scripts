package com.Norton.Smartwork5.Pages;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.FakerNames;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.google.gson.JsonObject;

public class Register_Purchase_Page {
	
	WebDriver driver;
	String ErrorMessage;
	String StudentEmailID;
	String StudentEmailID_InvalidCode;
	WebDriverWait wait;

	@FindBy(how =How.ID, using="name")
	public WebElement FirstName_LastName;
	
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public WebElement Sign_in_or_Register;
	
	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton;
	
	@FindBy(how =How.XPATH, using = "//input[@type='email']")
	public
	WebElement StudentEmail;
	
	@FindBy(how =How.XPATH, using = "//input[@type='password']")
	public
	WebElement Password;
	
	@FindBy(how =How.ID, using = "password2")
	public
	WebElement Password2;
	
	@FindBy(how =How.XPATH, using = "//label[@id='register_purchase_choice_register']")
	public WebElement registration_code;
	
	@FindBy(how =How.ID, using = "register_purchase_choice_trial")
	public WebElement register_purchase_choice_trial;
	
	@FindBy(how =How.ID, using = "access_code")
	public WebElement access_code;
	
	@FindBy(how =How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement register_My_code_Button;
	
	@FindBy(how =How.XPATH, using = "//span[contains(text(),'Confirm')]/ancestor::button")
	public WebElement Confirm_Button;
	
	@FindBy(how =How.XPATH, using = "//input[@type='checkbox']")
	public WebElement Checkbox;
	
	@FindBy(how =How.ID, using = "login_dialog_school_type_select")
	public WebElement school_dropdown;
	
	@FindBy(how =How.ID, using = "login_dialog_country_select")
	public WebElement Select_Country;
	
	@FindBy(how =How.ID, using = "login_dialog_state_select")
	public WebElement Select_State;
	
	@FindBy(how =How.ID, using ="login_dialog_school_text_input")
	public WebElement School_Institution;
	
	@FindBy(how =How.XPATH, using = "//ul[@id='ui-id-4']/li")
	public WebElement School_Selection;
	
	@FindBy(how =How.XPATH, using = "//span[contains(text(),'Continue')]/ancestor::button")
	public WebElement Continue_Button;
	
	@FindBy(how =How.ID, using ="login_dialog_error_msg")
	public WebElement Error_Msg;
	
	/*@FindBy(how =How.XPATH, using="//table[@id='login_dialog_reg_table']/tbody/tr/td[@class='login_dialog_reg_table_cell_right']{0}")
	static List <WebElement> Registration_code;*/
	
	@FindBy(how =How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public
	WebElement oKbutton;
	
	@FindBy(how =How.XPATH, using ="//button[@id='login_dialog_confirmation_go_button']")
	public WebElement Get_Started;
	
	@FindBy(how =How.XPATH, using ="//button[@type='button']/span[contains(text(),'OK, Sign Up for Trial Access')]")
	public WebElement OK_SignUpforTrialAccess_button;
	
	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement MaintenanceButton;
	
	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement Sign_Up_for_Trial_AccessButton;

	
	// Initializing Web Driver and PageFactory.
		public Register_Purchase_Page(WebDriver driver){
			
			this.driver = driver;
			PageFactory.initElements(driver, this);
		
		}
		
// Call ReadJsonobject method to read and set node values from Json testData file. 	
					ReadJsonFile readJasonObject = new ReadJsonFile();
					JsonObject jsonobject = readJasonObject.readJason();
				
		
		
		//Allure Steps Annotations and Methods
		@Step("Register, Purchase, or Sign Up for Trial Access,  Method: {method} ")
		public String register_Access_ValidCode() throws Exception {
			
			wait = new WebDriverWait(driver,10);
			TimeUnit.SECONDS.sleep(10);
			wait.until(ExpectedConditions.visibilityOf(FirstName_LastName));
			
			String FirstName = FakerNames.getStudentFName();
			String LastName = FakerNames.getStudentFName();
			FirstName_LastName.clear();
			Thread.sleep(3000);
			FirstName_LastName.sendKeys(FirstName + " " + LastName);
			String EmailID = FirstName + "_" + LastName + "@mailinator.com";
			StudentEmail.clear();
			StudentEmail.sendKeys(EmailID);
			String studentPassword = "Tabletop1";
			Password.sendKeys(studentPassword);
			Password2.sendKeys(studentPassword);
			oKbutton.click();
			try {
				boolean okButtonExist = MaintenanceButton.isDisplayed();
				if (okButtonExist == true) {
					MaintenanceButton.click();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.getMessage();
			}
			
			
			//registration_code.click();
			Thread.sleep(5000);
			access_code.sendKeys(jsonobject.getAsJsonObject("StudentRegistration").get("Registration_Code").getAsString());
			
			register_My_code_Button.click();
			return EmailID;
		}
		
		@Step("Register, Purchase, or Sign Up for Trial Access,  Method: {method} ")
		public String register_Access_InValidCode() throws Exception {
			
			wait = new WebDriverWait(driver,10);
			TimeUnit.SECONDS.sleep(10);
			wait.until(ExpectedConditions.visibilityOf(FirstName_LastName));
			
			String FirstName = FakerNames.getStudentFName();
			String LastName = FakerNames.getStudentFName();
			FirstName_LastName.clear();
			Thread.sleep(3000);
			FirstName_LastName.sendKeys(FirstName + " " + LastName);
			String EmailID = FirstName + "_" + LastName + "@mailinator.com";
			StudentEmail.clear();
			StudentEmail.sendKeys(EmailID);
			String studentPassword = "Tabletop1";
			Password.sendKeys(studentPassword);
			Password2.sendKeys(studentPassword);
			//StudentEmailID_InvalidCode= jsonobject.getAsJsonObject("StudentRegistration_ExpiredCode").get("EmailID").getAsString();
			//registration_code.click();
			oKbutton.click();
			try {
				boolean okButtonExist = MaintenanceButton.isDisplayed();
				if (okButtonExist == true) {
					MaintenanceButton.click();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.getMessage();
			}
			Thread.sleep(5000);
			access_code.sendKeys(jsonobject.getAsJsonObject("StudentRegistration_ExpiredCode").get("Expired_Registration_Code").getAsString());
			
			register_My_code_Button.click();
			
			return EmailID;
		}
		
		@Step("Before we complete your registration,  Method: {method} ")
		public void confirm_Registration(String studentEmailID) throws Exception {
			wait = new WebDriverWait(driver,10);
			TimeUnit.SECONDS.sleep(10);
			wait.until(ExpectedConditions.visibilityOf(StudentEmail));
			StudentEmail.sendKeys(studentEmailID);
			Confirm_Button.click();
}
		
		@Step("Please read our Terms of Use and Privacy Policy.,  Method: {method} ")
		public void term_privacypolicy_popup() throws Exception {
			wait = new WebDriverWait(driver,8);
			TimeUnit.SECONDS.sleep(8);
			//wait.until(ExpectedConditions.visibilityOf(Checkbox));
			Checkbox.click();
			Select drop = new Select(school_dropdown);
			drop.selectByValue("HIGH_SCHOOL");
			
			Select country = new Select(Select_Country);
			country.selectByValue("USA");
			
			Select state = new Select(Select_State);
			state.selectByValue("AZ");
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(School_Institution));
			School_Institution.click();
			School_Institution.sendKeys("Agua Fria High School");
			
			Thread.sleep(2000);
			Continue_Button.click();
			Thread.sleep(2000);
			
		}
		
		@Step("Verify Error message, Method: {method}")
		public void verifyErrorMessage(){
			String ErrorText = Error_Msg.getText();
			String[] arrOfStr = ErrorText.split("[(]");
			ErrorMessage=arrOfStr[0];
			Assert.assertEquals(ErrorMessage, PropertiesFile.ExpiredCodeErrorMessage);
		}
		
			
		@Step("You’re good to go! pop up, Method: {method} ")
		public void getStarted_popup() throws Exception {	
			wait = new WebDriverWait(driver,8);
			TimeUnit.SECONDS.sleep(8);
			wait.until(ExpectedConditions.visibilityOf(Get_Started));
			Get_Started.click();
			
		}
		@Step("Create a New user, Method: {method} ")
		public String  createNewUser() throws InterruptedException{
			ReusableMethods.waitElementClickable(driver, By.id("gear_button"));
			Sign_in_or_Register.click();
			SignInButton.click();
			Thread.sleep(5000);
			String FirstName = FakerNames.getStudentFName();
			String LastName = FakerNames.getStudentFName();
			FirstName_LastName.clear();
			Thread.sleep(3000);
			FirstName_LastName.sendKeys(FirstName + " " + LastName);
			String EmailID = FirstName + "_" + LastName + "@mailinator.com";
			StudentEmail.clear();
			StudentEmail.sendKeys(EmailID);
			String studentPassword = "Tabletop1";
			Password.sendKeys(studentPassword);
			Password2.sendKeys(studentPassword);
			oKbutton.click();
			try {
				boolean okButtonExist = MaintenanceButton.isDisplayed();
				if (okButtonExist == true) {
					MaintenanceButton.click();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.getMessage();
			}
			register_purchase_choice_trial.click();
			Sign_Up_for_Trial_AccessButton.click();
			Thread.sleep(2000);
			OK_SignUpforTrialAccess_button.click();
			StudentEmail.sendKeys(EmailID);
			Confirm_Button.click();

			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);
			wait.until(ExpectedConditions.visibilityOf(Checkbox));
			Checkbox.click();
			Select drop = new Select(school_dropdown);
			drop.selectByValue("HIGH_SCHOOL");

			Select country = new Select(Select_Country);
			country.selectByValue("USA");

			Select state = new Select(Select_State);
			state.selectByValue("AZ");
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login_dialog_school_text_input")));
			School_Institution.click();
			Thread.sleep(1000);
			School_Institution.sendKeys("Agua Fria High School");

			Thread.sleep(2000);
			Continue_Button.click();

			wait = new WebDriverWait(driver, 100);
			wait.until(ExpectedConditions.elementToBeClickable(Get_Started));
			Get_Started.click();
			return EmailID;

		}
		@Step("User Enters the purchase option details, Method: {method} ")
		public void  purchaseOptiondetails(String EmailID) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(StudentEmail));
		StudentEmail.click();	
		StudentEmail.sendKeys(EmailID);
		Thread.sleep(2000);
		Confirm_Button.click();

		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");
		Select state = new Select(Select_State);
		state.selectByValue("AZ");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login_dialog_school_text_input")));
		School_Institution.click();
		Thread.sleep(1000);
		School_Institution.sendKeys("Agua Fria High School");

		Thread.sleep(2000);
		Continue_Button.click();

		}
		
		public void clickMaintenanceButton(){
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		}
		@Step("Verify Already Purchase Text on Purchase Table,  Method: {method} ")
		public void VerifyPurchaseData() throws Exception{
			List<WebElement> divtable= driver.findElements(By.xpath("//div[@id='login_dialog_inner']/div[@class='login_dialog_purchase_option_div login_dialog_purchase_option_div_already_purchased']"));
			//List<WebElement> divtable= driver.findElements(By.xpath("//div[@id='login_dialog_inner']/div"));
			int divtablecount=divtable.size();
			for(int i=0; i<divtablecount; i++){
				List<WebElement> colVals = divtable.get(i).findElements(By.tagName("td"));
				java.util.Iterator<WebElement> a = colVals.iterator();
				//System.out.println(colNum);
				WebElement val = null;
				while(a.hasNext()) {
		            val = a.next();
		            System.out.println(val.getText());
		            
		            
		        } 
				if(val.getText().equalsIgnoreCase("ALREADY PURCHASED"));
				LogUtil.log("ALREADY PURCHASED text is displayed");
			}
		}
		
	public String createUser() throws InterruptedException{
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.clear();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String emailID = FirstName + "_" + LastName + "@mailinator.com";
		StudentEmail.clear();
		StudentEmail.sendKeys(emailID);
		String studentPassword = "Tabletop1";
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);
		
		oKbutton.click();
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		Thread.sleep(5000);
		return emailID;
	}
}
