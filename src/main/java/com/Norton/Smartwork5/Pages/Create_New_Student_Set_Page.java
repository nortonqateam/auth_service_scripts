package com.Norton.Smartwork5.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import Utilities.GetDate;
import Utilities.ReadJsonFile;

import com.google.gson.JsonObject;

public class Create_New_Student_Set_Page {
	
	 
	
WebDriver driver;


@FindBy(how =How.XPATH,using="div[@class='ui-progressbar-overlay']")
public WebElement Authorize_overlay;

	@FindBy(how =How.ID, using ="create_class_start_radio_new")
	public WebElement create_New_Student_Set_radioOption;
	
	@FindBy(how =How.ID, using ="create_class_start_radio_copy")
	public WebElement create_class_start_radio_copy_radioOption;
	
	@FindBy(how =How.ID, using ="create_class_start_radio_child")
	public WebElement create_class_start_radio_child_radioOption;
	
	
	@FindBy(how =How.XPATH, using ="//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'Next')]")
	public WebElement Next_Button;
	
	@FindBy(how =How.ID, using ="new_student_set_title")
	public WebElement new_student_set_title_textbox;
	
	@FindBy(how =How.ID, using ="new_student_set_state")
	public WebElement new_student_set_state_listbox;
	
	@FindBy(how =How.ID, using ="new_student_set_school_text_input")
	public WebElement new_student_set_school_text_input;
	
	@FindBy(how =How.ID, using ="new_student_set_start_date")
	public WebElement student_set_start_date;
	
	@FindBy(how =How.XPATH, using ="//a[@class='ui-state-default ui-state-highlight']")
	public WebElement Student_SelectDate;
	
	@FindBy(how =How.ID, using ="new_student_set_end_date")
	public WebElement student_set_end_date;
	
	@FindBy(how =How.XPATH, using ="//div[@id='manage_student_sets_div']/h3")
	public WebElement clickoutside;
	
	@FindBy(how =How.ID, using ="schoolIDNo")
	public WebElement schoolIDNo;
	
	@FindBy(how =How.XPATH, using ="//div[@id='add_new_student_set']/span[contains(text(),'Create New Student Set')]")
	public WebElement Create_New_Student_Set_Button;
	
	@FindBy(how =How.XPATH, using ="//button[@type='button']/span[contains(text(),'Create Student Set')]")
	public WebElement Create_Student_Set_Button;
	
	@FindBy(how =How.ID, using ="alert_dialog_outer_1")
	public WebElement Student_alert_Text;
	
	@FindBy(how =How.XPATH, using ="//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'OK')]")
	public WebElement Ok_button;
	
	
public Create_New_Student_Set_Page(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
ReadJsonFile readJasonObject = new ReadJsonFile();
JsonObject jsonobject = readJasonObject.readJason();



//Create a new Student Set from scratch.
public void createNewStudentset(){
	create_New_Student_Set_radioOption.click();
	Next_Button.click();
}
//Create a New Student Set General information.
public void createStudentset_information() throws InterruptedException{
	String Title = jsonobject.getAsJsonObject("StudentSetGeneralInfo").get("Title").getAsString();
	new_student_set_title_textbox.sendKeys(Title);
	Select drpCountry = new Select(new_student_set_state_listbox);
	drpCountry.selectByValue("US_AZ");
	WebDriverWait wait = new WebDriverWait(driver,8);
	TimeUnit.SECONDS.sleep(8);
	wait.until(ExpectedConditions.visibilityOf(new_student_set_school_text_input));
	String SchoolName =jsonobject.getAsJsonObject("StudentSetGeneralInfo").get("SchoolName").getAsString();
	new_student_set_school_text_input.sendKeys(SchoolName);
	student_set_start_date.click();
	//Student_SelectDate.click();
	student_set_start_date.sendKeys(GetDate.getCurrentDate());
	student_set_end_date.click();
	//Student_SelectDate.click();
	student_set_end_date.sendKeys(GetDate.getNextmonthDate());
	clickoutside.click();
	Thread.sleep(3000);
	List<WebElement> oRadiobuttons = driver.findElements(By.name("schoolID"));
	boolean bvalue=false;
	bvalue = oRadiobuttons.get(0).isSelected();
	if(bvalue==true){
		oRadiobuttons.get(0).click();
	}else {
		oRadiobuttons.get(1).click();
	}
	
	Create_Student_Set_Button.click();
}


public String createStudentset_ID() throws InterruptedException {
	String studentsetID = null;
	WebDriverWait wait = new WebDriverWait(driver,8);
	TimeUnit.SECONDS.sleep(8);
	wait.until(ExpectedConditions.visibilityOf(Student_alert_Text));
	String str = Student_alert_Text.getText();
	Matcher m = Pattern.compile("\\d+").matcher(str);
	while (m.find()) {
	  studentsetID =m.group(0);
	}
	
	Ok_button.click();
	return studentsetID;
}

}
