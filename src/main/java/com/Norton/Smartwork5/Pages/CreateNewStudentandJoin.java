package com.Norton.Smartwork5.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.FakerNames;
import Utilities.GetRandomId;
import Utilities.LogUtil;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.Norton.Smartwork5.Test.LoginAsInstructor;
import com.google.gson.JsonObject;


public class CreateNewStudentandJoin {
	static final Logger log = Logger.getLogger(CreateNewStudentandJoin.class);

	WebDriver driver;
	WebDriverWait wait;
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public WebElement Sign_in_or_Register;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_register']/span/span")
	public WebElement Need_to_register;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton;

	@FindBy(how = How.ID, using = "name")
	public WebElement FirstName_LastName;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	public WebElement StudentEmail;

	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	public WebElement Password;

	@FindBy(how = How.ID, using = "password2")
	public WebElement Password2;

	@FindBy(how = How.ID, using = "register_purchase_choice_trial")
	public WebElement register_purchase_choice_trial;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement Sign_Up_for_Trial_AccessButton;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK, Sign Up for Trial Access')]")
	public WebElement OK_SignUpforTrialAccess_button;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Confirm')]/ancestor::button")
	public WebElement Confirm_Button;

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	public WebElement Checkbox;

	@FindBy(how = How.ID, using = "login_dialog_school_type_select")
	public WebElement school_dropdown;

	@FindBy(how = How.ID, using = "login_dialog_country_select")
	public WebElement Select_Country;

	@FindBy(how = How.ID, using = "login_dialog_state_select")
	public WebElement Select_State;

	@FindBy(how = How.ID, using = "login_dialog_school_text_input")
	public WebElement School_Institution;

	@FindBy(how = How.XPATH, using = "//ul[@id='ui-id-4']/li")
	public WebElement School_Selection;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Continue')]/ancestor::button")
	public WebElement Continue_Button;

	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement Error_Msg;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement oKbutton;
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span/span[@id='gear_button_username']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_confirmation_go_button']")
	public WebElement Get_Started;

	@FindBy(how = How.XPATH, using = "//button[@id='login_button']")
	WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button'][//span[@id='gear_button_username']]")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;

	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement ErrorMessage;

	@FindBy(how = How.CLASS_NAME, using = "ui-progressbar-overlay")
	public WebElement Overlay;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Smartwork5')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public WebElement SW5Icon;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'OK')]")
	WebElement copiedSSOkButton;

	@FindBy(how = How.ID, using = "swfb_no_student_set_btn")
	public WebElement JoinStudentSetIDbutton;

	@FindBy(how = How.ID, using = "class_registration_id")
	public WebElement classRegistrationID;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement okButton;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog']/div")
	public WebElement studentinfo;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog']/div")
	public WebElement LoginDialog;
	
	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement MaintenanceButton;

	public CreateNewStudentandJoin(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// Read data from Json File
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	@Step("Create a New User user through Smartwork5 Page and Verify User is created successfully,  Method: {method} ")
	public String CreateNewUser() throws InterruptedException {
		ReusableMethods.waitElementClickable(driver, By.id("gear_button"));
		Sign_in_or_Register.click();
	    ReusableMethods.waitVisibilityOfElement(driver, By.id("login_dialog"));
		List<WebElement> oRadiobuttons = driver.findElements(By
				.name("register_login_choice"));
		 Thread.sleep(3000);
		oRadiobuttons.get(1).click();

		SignInButton.click();
        Thread.sleep(3000);
		//oKbutton.click();
		String guid = GetRandomId.randomAlphaNumeric(10).toLowerCase();
		guid = guid.replaceAll("[^a-zA-Z0-9]+", "a");
         
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.clear();
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String EmailID = FirstName + "_" + LastName + "@mailinator.com";
		StudentEmail.clear();
		StudentEmail.sendKeys(EmailID);
		String studentPassword = jsonobject
				.getAsJsonObject("Users").get("loginpassword")
				.getAsString();
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);
		oKbutton.click();
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		register_purchase_choice_trial.click();
		Sign_Up_for_Trial_AccessButton.click();
		Thread.sleep(2000);
		OK_SignUpforTrialAccess_button.click();
		StudentEmail.sendKeys(EmailID);
		Confirm_Button.click();

		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");

		Select country = new Select(Select_Country);
		country.selectByValue("USA");

		Select state = new Select(Select_State);
		state.selectByValue("AZ");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login_dialog_school_text_input")));
		School_Institution.click();
		Thread.sleep(1000);
		School_Institution.sendKeys("Agua Fria High School");

		Thread.sleep(2000);
		Continue_Button.click();

		wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.elementToBeClickable(Get_Started));
		Get_Started.click();
		
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.visibilityOf(username));
		String actualUserName = EmailID;
	
		String expectedUserName = username.getText();

		log.info("The SW5 User Name is " + expectedUserName);

		Assert.assertEquals(expectedUserName, actualUserName);

		return expectedUserName;
	}

	@Step("Click the Smartwork Icon from the DLP page,  Method: {method} ")
	public void clickSW5Icon() throws Exception {
        
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(SW5Icon));
		SW5Icon.click();
		/*try {

			wait.until(ExpectedConditions.visibilityOf(copiedSSOkButton));
			boolean selectSSPopUpExist = copiedSSOkButton.isDisplayed();

			if (selectSSPopUpExist == true) {
				copiedSSOkButton.click();
			}

		}

		catch (Exception e) {
			LogUtil.log("Exception Handlled: " + e.getMessage());
		}*/

	}

	@Step("New Student clicks the Join a Student Set Now button ,  Method: {method} ")
	public void joinstudentset() throws InterruptedException {
		Thread.sleep(3000);
		JoinStudentSetIDbutton.click();
	}

	@Step("New Student enters the class Registration ID & Click Ok Button ,  Method: {method} ")
	public void enterStudentSetID(String studentSetID) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(okButton));
		classRegistrationID.sendKeys(studentSetID);
		okButton.click();
	}

	@Step("get the student set information,  Method: {method} ")
	public void studentsetinfo(String studentSetID) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//div[@id='login_dialog']/div")));
		String message = LoginDialog.getText();
		if (message == null
				|| message.trim().startsWith(
						"You’ve been successfully added to Student Set") == false) {
			throw new Exception("There is some error in joining Student Set "
					+ studentSetID);
		}
		LogUtil.log("The Joined Student information" + studentinfo.getText());

		okButton.click();
		Thread.sleep(5000);
	}

	@Step("Logout Smartwork5,  Method: {method} ")
	public void logoutSmartwork5() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions
				.elementToBeClickable(gear_button_username));
		Thread.sleep(2000);
		gear_button_username.click();
		wait.until(ExpectedConditions.elementToBeClickable(SignOut_link));
		SignOut_link.click();
		Thread.sleep(5000);
	}
	
	@Step("Create New Student set,  Method: {method}")
	public String createNewStudentSet() throws Exception {
			LoginAsInstructor logininst = new LoginAsInstructor();
			logininst.loginInstructor();
			CreateNewStudentSet CSS = new CreateNewStudentSet(driver);
			Thread.sleep(5000);
			CSS.SignIn_Register();
			CSS.ManageStudentSetlink();
			CSS.CreateStudentSetButton();
			CSS.createNewStudentset();
			CSS.createStudentset_information();
			String studentSetID = CSS.createStudentset_ID();
            LogUtil.log("The StudentSetID", studentSetID);
			ManageStudentSetsPage managestudentsetpage = new ManageStudentSetsPage(driver);
			managestudentsetpage.clickcloselink();
			CSS.logoutSmartwork5();
		

		return studentSetID;
	}

}
