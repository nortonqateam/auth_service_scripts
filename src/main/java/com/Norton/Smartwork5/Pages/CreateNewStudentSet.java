package com.Norton.Smartwork5.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.GetDate;
import Utilities.GetRandomId;
import Utilities.ReadJsonFile;

import com.google.gson.JsonObject;



public class CreateNewStudentSet {
	// com.wwnorton.SW5Automation.utilities.GetDate getsystemdate = new
	// com.wwnorton.SW5Automation.utilities.GetDate();

	WebDriverWait wait;
	WebDriver driver;
	String TitleName;

	@FindBy(how = How.XPATH, using = "div[@class='ui-progressbar-overlay']")
	public WebElement Authorize_overlay;

	@FindBy(how = How.ID, using = "create_class_start_radio_new")
	public WebElement create_New_Student_Set_radioOption;

	@FindBy(how = How.ID, using = "create_class_start_radio_copy")
	public WebElement create_class_start_radio_copy_radioOption;

	@FindBy(how = How.ID, using = "create_class_start_radio_child")
	public WebElement create_class_start_radio_child_radioOption;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'Next')]")
	public WebElement Next_Button;

	@FindBy(how = How.ID, using = "new_student_set_title")
	public WebElement new_student_set_title_textbox;

	@FindBy(how = How.ID, using = "new_student_set_state")
	public WebElement new_student_set_state_listbox;

	@FindBy(how = How.ID, using = "new_student_set_school_text_input")
	public WebElement new_student_set_school_text_input;

	@FindBy(how = How.ID, using = "new_student_set_start_date")
	public WebElement student_set_start_date;

	@FindBy(how = How.XPATH, using = "//a[@class='ui-state-default ui-state-highlight']")
	public WebElement Student_SelectDate;

	@FindBy(how = How.ID, using = "new_student_set_end_date")
	public WebElement student_set_end_date;

	@FindBy(how = How.XPATH, using = "//div[@id='manage_student_sets_div']/h3")
	public WebElement clickoutside;

	@FindBy(how = How.ID, using = "schoolIDNo")
	public WebElement schoolIDNo;

	@FindBy(how = How.XPATH, using = "//div[@id='add_new_student_set']/span[contains(text(),'Create New Student Set')]")
	public WebElement Create_New_Student_Set_Button;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Create Student Set')]")
	public WebElement Create_Student_Set_Button;

	@FindBy(how = How.XPATH, using = "//div[@class='alert_dialog_class ui-dialog-content ui-widget-content' and contains(text(),'Student Set successfully created. The new Student Set ID is:')]")
	public WebElement Student_alert_Text;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'OK')]")
	public WebElement Ok_button;

	@FindBy(how = How.XPATH, using = "//li[@class='ui-menu-item']/a[contains(text(),'Manage Student Sets')]")
	public WebElement ManageStudetSets;

	@FindBy(how = How.ID, using = "gear_button_username")
	public WebElement Sign_in_or_Register;

	@FindBy(how = How.XPATH, using = "//div[@id='add_new_student_set']/span[@class='ui-button-text']")
	public WebElement Create_New_Student_Set_button;

	@FindBy(how = How.XPATH, using = "//button[@id='login_button']")
	WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//*[@id='gear_button_username']")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;

	public CreateNewStudentSet(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	@Step("Click Sign_in_or_Register button,  Method: {method} ")
	public void SignIn_Register() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(Sign_in_or_Register));
		Sign_in_or_Register.click();
	}

	@Step("Click on Manage StudentSet Link button,  Method: {method} ")
	public void ManageStudentSetlink() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ManageStudetSets));
		ManageStudetSets.click();
	}

	@Step("Click on Create Student set button,  Method: {method} ")
	public void CreateStudentSetButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(Create_New_Student_Set_button));
		Create_New_Student_Set_button.click();
	}

	// Create a new Student Set from scratch.
	@Step("Select the Create New Student from Scratch radio option and click the Next button,  Method: {method} ")
	public void createNewStudentset() {
		create_New_Student_Set_radioOption.click();
		Next_Button.click();
	}

	// Create a New Student Set General information.
	@Step("Enter or Select the Student General informations and Click the Save button,  Method: {method} ")
	public void createStudentset_information() throws InterruptedException {
		String Title = jsonobject.getAsJsonObject("StudentSetGeneralInfo").get("Title").getAsString();
		new_student_set_title_textbox.sendKeys(Title + GetRandomId.randomAlphaNumeric(3).toLowerCase());
		Select drpCountry = new Select(new_student_set_state_listbox);
		drpCountry.selectByValue("US_AZ");
		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(new_student_set_school_text_input));
		String SchoolName = jsonobject.getAsJsonObject("StudentSetGeneralInfo").get("SchoolName").getAsString();
		new_student_set_school_text_input.sendKeys(SchoolName);
		student_set_start_date.click();
		// Student_SelectDate.click();
		student_set_start_date.sendKeys(GetDate.getCurrentDate());
		student_set_end_date.click();
		// Student_SelectDate.click();
		student_set_end_date.sendKeys(GetDate.getNextmonthDate());
		clickoutside.click();
		Thread.sleep(3000);
		List<WebElement> oRadiobuttons = driver.findElements(By.name("schoolID"));
		boolean bvalue = false;
		bvalue = oRadiobuttons.get(0).isSelected();
		if (bvalue == true) {
			oRadiobuttons.get(0).click();
		} else {
			oRadiobuttons.get(1).click();
		}

		Create_Student_Set_Button.click();
	}

	@Step("Get the Student ID,  Method: {method} ")
	public String createStudentset_ID() throws Exception {
		String studentsetID = null;
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(Student_alert_Text));
		String str = Student_alert_Text.getText();
		Matcher m = Pattern.compile("\\d+").matcher(str);
		while (m.find()) {
			studentsetID = m.group(0);
		}

		Ok_button.click();

		wait.until(ExpectedConditions.elementToBeClickable(Create_New_Student_Set_Button));

		return studentsetID;
	}

	@Step("Logout Smartwork5,  Method: {method} ")
	public void logoutSmartwork5() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(gear_button_username));
		gear_button_username.click();
		SignOut_link.click();
		wait.until(ExpectedConditions.elementToBeClickable(loginButton));
	}
}
