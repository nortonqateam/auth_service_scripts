package com.Norton.Smartwork5.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Activity_group_selectors {
	WebDriver driver;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Ebook')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement ClickEbook; 
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Instructor Resources')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement ClickInstructorResources; 
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Student Grades')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement ClickStudentGrades;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Smartwork5')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement ClickSmartwork5;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Student Site')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement Student_Site;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Quizzes')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement Quizzes;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'ZAPS')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement ZAPS;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'InQuizitive')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement InQuizitive;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'Getting Started')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement ClickGettingStarted;
	
	@FindBy(how =How.XPATH, using="//table[@class='demo_activity_list_table']/tbody/tr/td/a[1]")
	public	WebElement clickfirstlink_Quizzes;
	
	@FindBy(how =How.XPATH, using="//button[@id='start_quiz_button']/span")
	public	WebElement Start_Quiz_Button;
	
	@FindBy(how =How.XPATH, using="//button[@type='button']/span[contains(text(),'I don’t have a Student Set ID at this time')]")
	public	WebElement StudentSetIDButton;
	
	@FindBy(how =How.XPATH, using ="//div[contains(text(),'The NAEL Archive')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public	WebElement NAELArchive_Tile;
	
	@FindBy(how =How.XPATH, using ="//div[@class='book-cover']")
	public	WebElement BookCover;
	
	@FindBy(how =How.XPATH, using ="//span[@id='return_to_resources_button_text']")
	public	WebElement return_to_resources_button_text;
	
public Activity_group_selectors(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}

}
