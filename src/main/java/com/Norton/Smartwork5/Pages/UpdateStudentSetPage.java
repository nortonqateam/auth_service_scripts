package com.Norton.Smartwork5.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UpdateStudentSetPage {
	
WebDriver driver;
WebDriverWait wait;
	
	@FindBy(how =How.XPATH, using ="//button[@id='edit_student_set_add_member']/span[contains(text(),'Add Member')]")
	public WebElement AddMemberButton;
	
	@FindBy(how =How.ID, using ="edit_student_set_title")
	public WebElement StudentSetTitle;
	
	//Add New Member to Student Set elements
	@FindBy(how =How.ID, using ="member-role-select")
	public WebElement Memberroleselect;
	
	@FindBy(how =How.ID, using="edit_student_set_add_member_email")
	public WebElement EmailAddressTextbox;
	
	@FindBy(how =How.ID, using="edit_student_set_lookup_button")
	public WebElement LookupButton;
	
	@FindBy(how =How.ID, using="edit_student_set_member_names")
	public WebElement StudentMemeberName;
	
	@FindBy(how =How.XPATH, using="//button[@type='button']/span[contains(text(),'Add')]")
	public WebElement AddButton;
	
		
	@FindBy(how =How.XPATH, using="//a[@class='link-button']/b[contains(text(),'Instructors/TAs')]")
	public WebElement InstructorsTAslinktab;
	
	@FindBy(how =How.XPATH, using ="//table[@id='edit_student_set_instructor_table']/tbody")
	public WebElement studentsetinsttable;
	
	@FindBy(how =How.XPATH, using ="//table[@id='edit_student_set_instructor_table']/tbody/tr")
	public By liststudentsetinsttable;
	
	@FindBy(how =How.XPATH, using ="//table[@id='edit_student_set_instructor_table']/tbody/tr/td")
	public By coltable;
		
	@FindBy(how =How.NAME, using="chk-instructor")
	public WebElement InstructorsTAscheckbox;
	
	@FindBy(how =How.XPATH, using="//button[@type='button']/span[contains(text(),'Save')]")
	public WebElement SaveButton;
	
	@FindBy(how =How.XPATH, using ="//table[@id='student_set_data_table']/tbody/tr")
	public WebElement student_set_data_table;
	
	public UpdateStudentSetPage(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
	public void updateStudent() throws InterruptedException{
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(AddMemberButton));
		AddMemberButton.click();
		Select selectRole = new Select(Memberroleselect);
		selectRole.selectByVisibleText("Full Instructor");
		
	}
	
	public String AssignInstructorRole(){
		WebElement insttable = driver.findElement(By.xpath("//table[@id='edit_student_set_instructor_table']/tbody"));
		List<WebElement> rowVals =insttable.findElements(By.tagName("tr"));
		int rowNum =rowVals.size();
		System.out.println(rowNum);
		String cellText = null;
		String EmailName=null;
		for(int row=0; row<rowNum; row++){
			List<WebElement> colVals = rowVals.get(row).findElements(By.tagName("td"));
			int colNum= colVals.size();
			System.out.println(colNum);
			for(int col=0; col<colNum; col++){
				if(col==1){
				cellText= colVals.get(col).getText();
				//System.out.println(cellText);
				Matcher m1 = Pattern.compile("\\((.*?)\\)").matcher(cellText);
				while(m1.find()) {
					EmailName =m1.group(1);
				}
				}
			}
		}
		
		return EmailName;
				
		
	}
	
	
	
	public String VerifyAdditionalInstructore(){
		WebElement insttable = driver.findElement(By.xpath("//table[@id='student_set_data_table']/tbody"));
		List<WebElement> rowVals =insttable.findElements(By.tagName("tr"));
		int rowNum =rowVals.size();
		System.out.println(rowNum);
		String AdditionalInstColName = null;
		for(int row=0; row<rowNum; row++){
			List<WebElement> colVals = rowVals.get(row).findElements(By.tagName("td"));
			int colNum= colVals.size();
			//System.out.println(colNum);
			for(int col=0; col<colNum; col++){
				if(col==2){
					AdditionalInstColName= colVals.get(col).getText();
				//System.out.println(AdditionalInstColName);
				
				}
			}
		}
		
		return AdditionalInstColName;
				
		
	}
	

}
