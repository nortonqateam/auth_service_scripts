package com.Norton.NLT.Pages;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;




import Utilities.LogUtil;
import ru.yandex.qatools.allure.annotations.Step;

public class SetAssignmentGAU {
	
	WebDriver driver;
	
	
	//Finding WebElements on NLT Home page using PageFactory
	
	@FindBy(how = How.XPATH, using = "/html/body/div[6]")
	WebElement selectSSPopUp;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'OK')]")
	WebElement selectSSOkButton;

	@FindBy(how = How.XPATH, using = "//*[@id=\"report_class_menu\"]")
	public WebElement studentSetTitle;
	
	@FindBy(how = How.XPATH, using = "//input[@id='activity_gau_set_datepicker']")
	public WebElement setGAUDate;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Clear')]")
	public WebElement clearGAUDate;
	
	@FindBy(how = How.XPATH, using = "//div[@type='select']/select[@class='body-content form-control']")
	public WebElement SelectTime;
	
	@FindBy(how = How.XPATH, using = "//select[@id='activity_gau_set_timepicker']")
	public WebElement setGAUTimeNCIA;
	
	@FindBy(how = How.XPATH, using = "//div[@type='select']/select[@class='body-content form-control']")
	public WebElement setGAUTimeSW5;
	
	
	@FindBy(how = How.XPATH, using = "//select[@id='activity_gau_set_timezonepicker']")
	public WebElement setGAUTimeZoneNCIA;
	
	@FindBy(how = How.XPATH, using = "//select[@class='body-content timezone-gmt-style form-control']")
	public WebElement setGAUTimeZoneSW5;
	
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Set Grades Accepted Until')]")
	public WebElement setGAUButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']")
	public WebElement calendarDate;
	
	@FindBy(how = How.XPATH, using = "//input[@class='ant-calendar-input  ']")
	public WebElement calendarInput;

	@FindBy(how = How.XPATH, using = "//div[@class='general-settings']/span[@class='main-heading']")
	public WebElement settingsLabel;
	
	@FindBy(how = How.XPATH, using = "//span[@class='check-late-input']/input[@type='checkbox']")
	public WebElement lateworkacceptedcheckbox;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'PUBLISH')]")
	public WebElement PublishButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'UNPUBLISH')]")
	public WebElement unPublishButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'SAVE')]")
	public WebElement saveButton;
	
	@FindBy(how = How.XPATH, using = "//span[@class='glyphicons glyphicons-chevron-left']")
	public WebElement returnToAssignmentList;
	
	
	
	// Initializing Web Driver and PageFactory.
	
	public SetAssignmentGAU(WebDriver driver) throws Exception {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	
	@Step("Set Assignment GAU Date ,  Method: {method} ")
	public String setGAUDate(String GAUDate) throws Exception {
		
		String strTitle = null;
		String strGAUDate = null;
		WebElement assignmentTitle;
		WebElement assignmentGAUTitle = null;
		
		int countAssignmentTitles = driver.findElements(By.xpath("//table[@id='activity_list_table']//tbody//tr")).size();

		for (int i=1; i<=countAssignmentTitles; i++) {
			
			
			try {
					assignmentTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[1]"));
					assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[2]"));
			
					strGAUDate = assignmentGAUTitle.getText();
			
					if (strGAUDate.equalsIgnoreCase("[set]")) {
					
						strTitle = assignmentTitle.getText();
						assignmentGAUTitle.click();

						setGAUDate.sendKeys(GAUDate);
						setGAUDate.sendKeys(Keys.TAB);
					
						Thread.sleep(1000);
						Select drpGAUTime = new Select(setGAUTimeNCIA);
						drpGAUTime.selectByVisibleText("11:59 PM");
					
						Thread.sleep(1000);
						Select drpGAUTimeZone = new Select(setGAUTimeZoneNCIA);
						drpGAUTimeZone.selectByVisibleText("(GMT-05:00) Eastern Time");
					
						setGAUButton.click();
						break;
					}
					
				} catch(StaleElementReferenceException Ex) {
					// put minor fake delay so Javascript on page does its actions with controls
					Thread.sleep(3000); 
					assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[2]"));
					break;
				}
			
		}
		
		return  strTitle;
		
	}
	
	@Step("Get Assignment GAU Date ,  Method: {method} ")
	public String getAssignmentGAU(String strAssignment) throws Exception {
		
		String strTitle;
		String strGAUDate = null;
		WebElement assignmentTitle;
		WebElement assignmentGAUTitle;
		
		int countAssignmentTitles = driver.findElements(By.xpath("//table[@id='activity_list_table']//tbody//tr")).size();

		for (int i=1; i<=countAssignmentTitles; i++) {
				
				Thread.sleep(2000);
				assignmentTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[1]"));
				assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[2]"));
				
				strTitle = assignmentTitle.getText();
			
				if (strAssignment.equalsIgnoreCase(strTitle)) {
						
						strGAUDate = assignmentGAUTitle.getText();
						break;
					}
			
			}
		
		return  strGAUDate;
		
	}
	
	
	@Step("Set Assignment GAU Date for Three Assignments,  Method: {method} ")
	public void setGAUDateAll(String GAUDate) throws Exception {

		//WebElement assignmentTitle;
		WebElement assignmentGAUTitle;
		
		//int countAssignmentTitles = driver.findElements(By.xpath("//table[@id='activity_list_table']//tbody//tr")).size();
		
			int counter=1;
			int tablerow=1;
			
			while (counter<=3) {
			
							try {
					
									//assignmentTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[1]"));
									assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + tablerow + "]//td[2]"));
									if (assignmentGAUTitle.getText().equalsIgnoreCase("[set]")) {
										
										counter++;
						
										WebDriverWait wait = new WebDriverWait(driver, 10);
										wait.until(ExpectedConditions.elementToBeClickable(assignmentGAUTitle));
										assignmentGAUTitle.click();
				
										setGAUDate.sendKeys(GAUDate);
										setGAUDate.sendKeys(Keys.TAB);

										wait.until(ExpectedConditions.elementToBeClickable(setGAUTimeNCIA));
										Select drpGAUTime = new Select(setGAUTimeNCIA);
										drpGAUTime.selectByVisibleText("11:59 PM");

										wait.until(ExpectedConditions.elementToBeClickable(setGAUTimeZoneNCIA));
										Select drpGAUTimeZone = new Select(setGAUTimeZoneNCIA);
										drpGAUTimeZone.selectByVisibleText("(GMT-05:00) Eastern Time");
						
										wait.until(ExpectedConditions.elementToBeClickable(setGAUButton));
										setGAUButton.click();
						
										Thread.sleep(2000);
						
									}
					
								}	
							
								catch(StaleElementReferenceException Ex) {
									// put minor fake delay so Javascript on page does its actions with controls
									Thread.sleep(3000); 
									assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + tablerow + "]//td[2]"));
								}
							
							tablerow ++;
							
						} 
			
				}

		
	
	
	@Step("Remove Assignment GAU Date ,  Method: {method} ")
	public String removeGAUDate() throws Exception {
		
		String strTitle = null;
		WebElement assignmentTitle;
		WebElement assignmentGAUTitle;
		
		int countAssignmentTitles = driver.findElements(By.xpath("//table[@id='activity_list_table']//tbody//tr")).size();

		for (int i=1; i<=countAssignmentTitles; i++) {
			
			assignmentTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[1]"));
			assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[2]"));
				if (!((assignmentGAUTitle.getText().equals("[set]")) || (assignmentGAUTitle.getText().equals("—")))) {
					
					strTitle = assignmentTitle.getText();
					assignmentGAUTitle.click();

					clearGAUDate.click();
					
					Thread.sleep(1000);
					setGAUButton.click();
					break;
				}
					
			}
		
		return strTitle;
	}
	
	
	@Step("Remove all Assignments GAU Date ,  Method: {method} ")
	public void removeAllGAUDates() throws Exception {

		WebElement assignmentGAUTitle = null;
		
		int countAssignmentTitles = driver.findElements(By.xpath("//table[@id='activity_list_table']//tbody//tr")).size();

			for(int i=countAssignmentTitles; i>0; i--) {
					try {	
							assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[2]"));
							if (!((assignmentGAUTitle.getText().equals("[set]")) || (assignmentGAUTitle.getText().equals("—")))) {
					
							WebDriverWait wait = new WebDriverWait(driver, 15);
							wait.until(ExpectedConditions.elementToBeClickable(assignmentGAUTitle));
							assignmentGAUTitle.click();
					
							wait.until(ExpectedConditions.elementToBeClickable(clearGAUDate));
							clearGAUDate.click();
					
							wait.until(ExpectedConditions.elementToBeClickable(setGAUButton));
							setGAUButton.click();
							
							Thread.sleep(2000);

							}
					}
					 
					catch (StaleElementReferenceException Ex) {
						// put minor fake delay so Javascript on page does its actions with controls
						Thread.sleep(3000); 
						assignmentGAUTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[2]"));
				    }
					
				} 
		}
	
		
	@Step("Instructor clicks Edit Assignment Button to Set GAU Date,  Method: {method} ")
	public void editAssignment_SetGAU() throws Exception {
		
		WebElement gauValue;
		WebElement assignmentUpdateButtons;
		WebElement editAssignmentButton;
		WebElement isAdaptive;
		
		
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object rowCountObj = js.executeScript("return $('#activity_list_table tbody tr').length");
		int assignmentRowCount = Integer.valueOf(rowCountObj.toString());
		
		boolean assignmentUpdateButtonsExist = true;
		boolean isAdaptiveExist = true;
		//boolean breakForLoop = false;
		
		for (int i=1; i<=assignmentRowCount; i++) {
			
			try {
					assignmentUpdateButtons = driver.findElement(By.xpath("//tbody/tr[" + i + "]/td[1]/span[1]"));
					assignmentUpdateButtonsExist = assignmentUpdateButtons.isDisplayed();
					
			}
			
			
			catch (NoSuchElementException e) 
			{

				assignmentUpdateButtonsExist = false;
				
			}
			
			catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
		
			}
			
			
			
			try {
				
					isAdaptive = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/img[1]"));
					isAdaptiveExist = isAdaptive.isDisplayed();
				
			}
		
		
			catch (NoSuchElementException e) 
			{

				isAdaptiveExist = false;
			
			}
		
			catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
	
			}
			
			
					if (assignmentUpdateButtonsExist == true) {
					
						//for (int j=1; j<=assignmentRowCount; j++) {
							gauValue = driver.findElement(By.xpath("//tbody/tr[" + i + "]/td[3]"));
							if (!isAdaptiveExist && (gauValue.getText().equalsIgnoreCase("—") || gauValue.getText().equalsIgnoreCase("not assigned"))) {
						
								editAssignmentButton = driver.findElement(By.xpath("//tbody/tr[" + i + "]/td[1]/span[1]/a[1]"));
				
								editAssignmentButton.click();
								//breakForLoop = true;
								break;
						
							}


						//}	if (breakForLoop) {
						//	break;
						}
						
					}
		
			}

	
	@Step("Get Assignment Name,  Method: {method} ")
	public String getAssignmentName() throws Exception {
		
		WebElement editAssignmentName;
		
		Thread.sleep(5000);
		editAssignmentName = driver.findElement(By.xpath("//input[@placeholder = 'Your assignment name']"));
		
		String assignmentTitle = editAssignmentName.getAttribute("value");
		return assignmentTitle;
		
	}
	
	
	@Step("Instructor clicks Edit Assignment Button to Clear GAU Date,  Method: {method} ")
	public void editAssignment_ClearGAU() throws Exception {
		
		//String assignmentTitle = null;
		WebElement gauValue;
		WebElement assignmentUpdateButtons;
		WebElement editAssignmentButton;
		
		
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object rowCountObj = js.executeScript("return $('#activity_list_table tbody tr').length");
		int assignmentRowCount = Integer.valueOf(rowCountObj.toString());
		
		boolean assignmentUpdateButtonsExist = true;
		boolean breakForLoop = false;
		
		for (int i=1; i<=assignmentRowCount; i++) {
			
			try {
					assignmentUpdateButtons = driver.findElement(By.xpath("//tbody/tr[" + i + "]/td[1]/span[1]"));
					assignmentUpdateButtonsExist = assignmentUpdateButtons.isDisplayed();
			}
			
			
			catch (NoSuchElementException e) 
			{

				assignmentUpdateButtonsExist = false;
				
			}
			
			catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
		
			}
			

					if (assignmentUpdateButtonsExist == true) {
					
						for (int j=1; j<=assignmentRowCount; j++) {
							//assignmentTitle = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/div[3]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[" + j + "]/td[1]/a[1]")).getText();
							gauValue = driver.findElement(By.xpath("//tbody/tr[" + j + "]/td[3]"));
							if (!gauValue.getText().equalsIgnoreCase("—") || !gauValue.getText().equalsIgnoreCase("not assigned")) {
						
								editAssignmentButton = driver.findElement(By.xpath("//tbody/tr[" + j + "]/td[1]/span[1]/a[1]"));
				
								editAssignmentButton.click();
								breakForLoop = true;
								break;
						
							}


						}	if (breakForLoop) {
							break;
						}
						
					}
		
			}
				
				//return assignmentTitle;
			
		}
	
	
	@Step("Instructor enters GAU date,  Method: {method} ")
	public void setSW5GAUDate(String SW5GAUDate) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(calendarDate));
		
		calendarDate.click();
		
		Thread.sleep(2000);
		calendarInput.sendKeys(SW5GAUDate);

		settingsLabel.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(setGAUTimeSW5));
		Select drpGAUTime = new Select(setGAUTimeSW5);
		drpGAUTime.selectByVisibleText("11:59 PM");

		wait.until(ExpectedConditions.elementToBeClickable(setGAUTimeZoneSW5));
		Select drpGAUTimeZone = new Select(setGAUTimeZoneSW5);
		drpGAUTimeZone.selectByValue("US/Eastern");
	}
	
	
	@Step("Instructor Clears GAU date,  Method: {method} ")
	public void clearSW5GAUDate() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(calendarDate));
		String zipLen = calendarDate.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			calendarDate.sendKeys(Keys.ARROW_LEFT);
			calendarDate.sendKeys(Keys.DELETE);
			calendarDate.sendKeys(Keys.TAB);
			
		}
	}
	
	
	@Step("Get GAU date for an Assignment,  Method: {method} ")
	public String getGAUDate(String assignmentName) throws Exception {
		
		String assignmentTitle;
		String assignmentGAUDate = null;
		
		int rowCount = driver.findElements(By.xpath("//table[@id='activity_list_table']/tbody/tr")).size();
		
		for(int i=1; i<rowCount; i++ ) {
			
			assignmentTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]/td[1]/a")).getText();
			
			if(assignmentName.equalsIgnoreCase(assignmentTitle)) {
				
				assignmentGAUDate = driver.findElement(By.xpath("//table[@id='activity_list_table']/tbody/tr[" + i + "]/td[3]")).getText();
				break;
			}
			
		}
		
		return assignmentGAUDate;
	}
	
	
	@Step("Validate GAU date for an Assignment,  Method: {method} ")
	public void validateGAUDate(String strTitle, String strGAUDate) throws Exception {
		
		WebElement assignmentTitle;
		String assignmentGAUDate;
		
		int rowCount = driver.findElements(By.xpath("//table[@id='activity_list_table']/tbody/tr")).size();
		
		for(int i=1; i<rowCount; i++ ) {
			
				assignmentTitle = driver.findElement(By.xpath("//table[@id='activity_list_table']//tbody//tr[" + i + "]//td[1]"));
				assignmentGAUDate = driver.findElement(By.xpath("//table[@id='activity_list_table']/tbody/tr[" + i + "]/td[2]")).getText();
				
					if(assignmentTitle.getText().equalsIgnoreCase(strTitle)) {
						
						String editedGAUDate = "0"+assignmentGAUDate;						
						Assert.assertEquals(editedGAUDate.equalsIgnoreCase(strGAUDate), true);
				
				}
			}
			
		}
	
	
	@Step("Instructor click the Late work accepted check box and Add the days,  Method: {method} ")
	public void lateWorkCheckbox(String days, String penalty) {
		WebDriverWait waite = new WebDriverWait(driver, 5);
		waite.until(ExpectedConditions.elementToBeClickable(lateworkacceptedcheckbox));
		WebElement element = lateworkacceptedcheckbox;
		Actions builder2 = new Actions(driver);
		builder2.moveToElement(element).release().perform();

		WebElement latedays = driver.findElement(By.cssSelector(".check-late-input"));
		Actions builder = new Actions(driver);
		builder.moveToElement(latedays).release().perform();
		driver.findElement(By.cssSelector(".check-late-input > input")).click();
		WebDriverWait waitq = new WebDriverWait(driver, 10);
		waitq.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#late-work-days > #late-work-days")));
		driver.findElement(By.cssSelector("#late-work-days > #late-work-days")).click();
		driver.findElement(By.xpath("//div[@id=\'late-work-days\']/input")).sendKeys(days);
		LogUtil.log("The Late work days are entered " + days);
		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#late-work-pr > #late-work-pr")));

		WebElement penaltyelement = driver.findElement(By.cssSelector("#late-work-pr > #late-work-pr"));
		Actions builder1 = new Actions(driver);
		builder1.doubleClick(penaltyelement).perform();
		driver.findElement(By.xpath("//div[@id=\'late-work-pr\']/input")).sendKeys(penalty);
		LogUtil.log("The penalty  entered " + penalty);
	}
	
	
	@Step("Click Publish Button,  Method: {method} ")
	public void publishButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		wait.until(ExpectedConditions.elementToBeClickable(PublishButton));
		PublishButton.click();
		
	}
	
	@Step("Click UnPublish Button,  Method: {method} ")
	public void unPublishButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		wait.until(ExpectedConditions.elementToBeClickable(unPublishButton));
		unPublishButton.click();
		
	}
	
	@Step("Click Save Button,  Method: {method} ")
	public void saveButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		wait.until(ExpectedConditions.elementToBeClickable(saveButton));
		saveButton.click();
		
	}
	

}
