package com.Norton.NLT.Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;



public class LaunchBetaAssignment {
	
	
	WebDriver driver;
	
	
	//Finding WebElements on NLT Home page using PageFactory
	
	@FindBy(how = How.XPATH, using = "//a[@id='new_beta_student_view']")
	public
	WebElement BetaAssignmentViewButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ReactModal__Content ReactModal__Content--after-open global-modal global-modal-small course-select-modal']")
	public
	WebElement SelectCourseModal;
	
	@FindBy(how = How.XPATH, using = "//div[@class='default-dropdown__value-container css-1hwfws3']")
	public
	WebElement SelectCourseDropDown;

	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/button[1]")
	WebElement NLTButtonOk;
	
	
	// Initializing Web Driver and PageFactory.
	
	public LaunchBetaAssignment(WebDriver driver) throws Exception {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	@Step("Launch NLT Beta Assignment, Method: {method}")
	public void ClickBetaAssignmentButton() throws Exception {
		
		
		try {
				
				boolean BetaAssignmentButtonExist = BetaAssignmentViewButton.isDisplayed();
				
				if (BetaAssignmentButtonExist == true) {
				
					BetaAssignmentViewButton.click();
			
				}	
			
			}
		
			catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
	}
	
	
	@Step("Select NLT Course from Select a Course dropdown, Method: {method}")
	public void SelectNLTCourse(String jsonCourseName) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(5);
		
		try {
			
				wait.until(ExpectedConditions.visibilityOf(SelectCourseModal));
				boolean SelectCourseModalExist = SelectCourseModal.isDisplayed();
				
				WebElement CourseOption;
				String CourseName;
			
				if(SelectCourseModalExist == true) {
				
					SelectCourseDropDown.click();
					
					//Thread.sleep(2000);
					//JavascriptExecutor js = (JavascriptExecutor) driver;
					//Object rowCountObj = js.executeScript("return $('default-dropdown_menu css-26l39y-menu').length");
					//Object rowCountObj = js.executeScript("return document.getElementsByClassName('default-dropdown_menulist css-1ml51p6-MenuList').length");
					//int menuListCount = Integer.valueOf(rowCountObj.toString());
					
					//int MenuListCount = driver.findElements(By.className("default-dropdown_menu css-26l39y-menu")).size();
					//int MenuListCount = driver.findElements(By.className("default-dropdown_menulist css-1ml51p6-MenuList")).size();
				
					//int MenuListCount = driver.findElements(By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div/div[2]/div")).size();
					
					//int MenuListCount = driver.findElements(By.xpath("//div[@class='default-dropdown_menu css-26l39y-menu']")).size();
					
					for(int i=0; i<6; i++) {
						
						CourseOption = driver.findElement(By.id("react-select-2-option-" + i));
						CourseName = CourseOption.getText();
						
						if(CourseName.equals(jsonCourseName))
						{
							CourseOption.click();
							break;
						}
						
					}
					
					Thread.sleep(2000);
					NLTButtonOk.click();
			
				}	
			
			}
		
			catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
	}
	
}
