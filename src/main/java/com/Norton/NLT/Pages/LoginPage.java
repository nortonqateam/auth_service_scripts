package com.Norton.NLT.Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.ReadJsonFile;

import com.google.gson.JsonObject;





public class LoginPage {

	WebDriver driver;

	// Finding Web Elements on NLT Login page using PageFactory.

	@FindBy(how = How.XPATH, using = "//button[@id = 'login_button']/span[@class='ui-button-text']")
	public WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//input[@id='username']")
	public WebElement email;

	@FindBy(how = How.XPATH, using = "//input[@id='password']")
	public WebElement password;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']//span[@class='ui-button-text'][contains(text(),'Sign In')]")
	public WebElement signInButton;
	
	@FindBy(how = How.CLASS_NAME, using = "ui-progressbar-overlay")
	public WebElement Overlay;
	
	@FindBy(how = How.XPATH, using = "//*[@id='gear_button_username']")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text']//img")
	public WebElement gearMenuIcon;	
	
	@FindBy(how = How.XPATH, using = "//b[contains(text(),'Sign Out')]")
	public WebElement signOut;
	
	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'user-icon___')]")
	public WebElement userIcon; //*[matches(@id="user-icon___.*)"]
	
	@FindBy(how = How.XPATH, using = "//button[@class='item-inner' and contains(text(), \"Sign Out\")]")
	public WebElement nltSignOutButton;
	
	@FindBy(how = How.XPATH, using = "//h2[@id='product_title']")
	public WebElement productTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@id='product_edition']")
	public WebElement productEdition;
	
	// Initializing Web Driver and PageFactory.

	public LoginPage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// Call to Json data file to read test data

	ReadJsonFile readJsonObject = new ReadJsonFile();
	JsonObject jsonObj = readJsonObject.readJson();

	@Step("Login to NLT application,  Method: {method} ")
	public void loginNLT(String userName, String Password) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(5);


		try {

			wait.until(ExpectedConditions.visibilityOf(loginButton));
			boolean loginButtonExist = loginButton.isDisplayed();

			if (loginButtonExist == true) {
				loginButton.click();
			}

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

		wait.until(ExpectedConditions.visibilityOf(email));
		email.clear();
		email.sendKeys(userName);

		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys(Password);

		wait.until(ExpectedConditions.visibilityOf(signInButton));
		signInButton.click();

	}

	
	
	/**
	 * @param string
	 * @param userName2
	 */
	@Step("Verify Signin details with userName: {0}, LName: {1}, for Method:{method} step...")
	public void verifySignin(String uName, String LName) {
		Assert.assertEquals(uName, LName);

	}
	
	

	@Step("Logout of NCIA application,  Method: {method} ")
	public void logoutNCIA() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.elementToBeClickable(gearMenuIcon));
		gearMenuIcon.click();

		wait.until(ExpectedConditions.elementToBeClickable(signOut));
		signOut.click();

	}
	
	
	@Step("Logout of Beta Assignment application,  Method: {method} ")
	public void logoutNLT() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(userIcon));
		
		Actions actions = new Actions(driver);
		actions.moveToElement(userIcon).click().build().perform();
		
		wait.until(ExpectedConditions.elementToBeClickable(nltSignOutButton));
		actions.moveToElement(nltSignOutButton).click().build().perform();

	}
	
	
	@Step("Validate DLP Product Title & Edition and Sign In Button,  Method: {method} ")
	public void validateDLPPage() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(5);
		
		try {

				wait.until(ExpectedConditions.visibilityOf(loginButton));
				boolean loginButtonExist = loginButton.isDisplayed();

				if (loginButtonExist == true) {
					
					System.out.println("Sign In Button exist in DLP page.");
					
				}

			}

			catch (Exception e) {
					System.out.println("Exception Handlled: " + e.getMessage());
		}
		
		
		

	}

}
