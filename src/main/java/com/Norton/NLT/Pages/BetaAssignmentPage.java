package com.Norton.NLT.Pages;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;

public class BetaAssignmentPage {

	WebDriver driver;

	// Finding WebElements on NLT Home page using PageFactory

	@FindBy(how = How.XPATH, using = "//h1[@class='course-name']")
	public WebElement SelectedCourseName;

	@FindBy(how = How.XPATH, using = "//table[@class='course-table assign-due']//tr[@class='no-records-found']")
	public WebElement noDueAssignments;

	@FindBy(how = How.XPATH, using = "//div[@class='user-email']")
	public WebElement userLogin;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//div[contains(@class,'sort-arrows active')]")
	public WebElement dueDateSortIcons_AssignDue;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//th[contains(@class,'due col lg-table-col-4 md-col-2')]//div[contains(@class,'bottom-arrow')]")
	public WebElement dueDateSortIcons_BottomArrow_AssignDue;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//th[contains(@class,'assign col lg-table-col-8 md-col-2')]//div[contains(@class,'sort-arrows')]")
	public WebElement assignmentNameSortIcons_AssignDue;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//th[contains(@class,'assign col lg-table-col-8 md-col-2')]//div[contains(@class,'bottom-arrow')]")
	public WebElement assignmentNameSortIcon_BottomArrow_AssignDue;

	@FindBy(how = How.XPATH, using = "/table[contains(@class,'course-table assign-due')]//th[contains(@class,'assign col lg-table-col-8 md-col-2')]//div[contains(@class,'top-arrow')]")
	public WebElement assignmentNameSortIcon_TopArrow_AssignDue;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//th[contains(@class,'type col lg-table-col-4 md-col-1')]//div[contains(@class,'sort-arrows')]")
	public WebElement typeSortIcons_AssignDue;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//th[contains(@class,'type col lg-table-col-4 md-col-1')]//div[contains(@class,'bottom-arrow')]")
	public WebElement assignmentTypeSortIcon_BottomArrow_AssignDue;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//th[contains(@class,'status col lg-table-col-4 md-col-1')]//div[contains(@class,'sort-arrows')]")
	public WebElement statusSortIcons_AssignDue;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-due')]//th[contains(@class,'grade col lg-table-col-4 md-col-1')]//div[contains(@class,'sort-arrows')]")
	public WebElement gradeSortIcons_AssignDue;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//div[contains(@class,'sort-arrows active')]")
	public WebElement dueDateSortIcons_AssignPast;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//th[contains(@class,'due col lg-table-col-4 md-col-2')]//div[contains(@class,'bottom-arrow')]")
	public WebElement dueDateSortIcons_BottomArrow_AssignPast;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//th[contains(@class,'assign col lg-table-col-8 md-col-2')]//div[contains(@class,'sort-arrows')]")
	public WebElement assignmentNameSortIcons_AssignPast;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//th[contains(@class,'assign col lg-table-col-8 md-col-2')]//div[contains(@class,'bottom-arrow')]")
	public WebElement assignmentNameSortIcon_BottomArrow_AssignPast;

	@FindBy(how = How.XPATH, using = "/table[contains(@class,'course-table assign-complete')]//th[contains(@class,'assign col lg-table-col-8 md-col-2')]//div[contains(@class,'top-arrow')]")
	public WebElement assignmentNameSortIcon_TopArrow_AssignPast;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//th[contains(@class,'type col lg-table-col-4 md-col-1')]//div[contains(@class,'sort-arrows')]")
	public WebElement typeSortIcons_AssignPast;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//th[contains(@class,'type col lg-table-col-4 md-col-1')]//div[contains(@class,'bottom-arrow')]")
	public WebElement assignmentTypeSortIcon_BottomArrow_AssignPast;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//th[contains(@class,'status col lg-table-col-4 md-col-1')]//div[contains(@class,'sort-arrows')]")
	public WebElement statusSortIcons_AssignPast;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'course-table assign-complete')]//th[contains(@class,'grade col lg-table-col-4 md-col-1')]//div[contains(@class,'sort-arrows')]")
	public WebElement gradeSortIcons_AssignPast;
	
	@FindBy(how = How.XPATH, using = "//div[@class='today']")
	public WebElement currentDay;
	
	@FindBy(how = How.XPATH, using = "//div[@class='tomorrow']")
	public WebElement currentDayPlusOne;
	
	@FindBy(how = How.XPATH, using = "//table[@class='course-table assign-due']//div[@class='open-assignment-icon']")
	public WebElement assignmentNameLink;
	
	@FindBy(how = How.XPATH, using = "//div[@class='error-message-generic']")
	public WebElement oopsMsgBox;
	
	@FindBy(how = How.XPATH, using = "//a[@class='linkout']")
	public WebElement originalViewLink;
	

	// Initializing Web Driver and PageFactory.

	public BetaAssignmentPage(WebDriver driver) throws Exception {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@Step("Verify the email address which is used at the time of login, Method: {method}")
	public void validateLoginEmailID(String loginEmail) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(userLogin));
		String userLoginID = userLogin.getText();

		Assert.assertEquals(loginEmail, userLoginID);

	}
	
	@Step("Verify the selected Course Name, Method: {method}")
	public void checkSelectedBetaAssignment(String jsonCourseName, String strCourseName) throws Exception {

		Assert.assertEquals(strCourseName, jsonCourseName);

	}

	@Step("Click on Assignment Name Link in Due Assignment list, Method: {method}")
	public void clickAssignmentNameLink(String strName) throws Exception {
		
		String strAssignmentName;
		WebElement assignmentName;

		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();

		for (int i = 1; i <= countAssignments; i++) {

			assignmentName = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tbody/tr[" + i + "]/td[1]"));
			strAssignmentName = assignmentName.getText();

			if (strAssignmentName.equals(strName)) {
				
				assignmentNameLink.click();
	
			}
		}

	}
	
	
	@Step("Get Opps Message for invalid course id, Method: {method}")
	public void getOopsMsg() throws Exception {
		
		String strMsg = oopsMsgBox.getText();
		Assert.assertEquals(strMsg.equalsIgnoreCase("Sorry! Something went wrong."), true);

	}
	
	
	@Step("Get Opps Message for invalid course id, Method: {method}")
	public void validateOriginalViewLink() throws Exception {
		
		boolean originalVieLinkExist;
		
		try {
			
				originalVieLinkExist = originalViewLink.isDisplayed();
				
				if(originalVieLinkExist == true) {
					
					originalViewLink.click();	
				}
		
			} catch (Exception e) {

					System.out.println("Return to Original View Link is NOT displayed.");
			}

	}

	
	@Step("Validate Assignment Name Exist, Method: {method}")
	public void validateAssignmentNameExist(String strName) throws Exception {

		String strAssignmentName;
		WebElement assignmentName;

		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();

		for (int i = 1; i <= countAssignments; i++) {

			assignmentName = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tbody/tr[" + i + "]/td[1]"));
			strAssignmentName = assignmentName.getText();

			if (strAssignmentName.equals(strName)) {
				
					Assert.assertTrue(true); 
					System.out.println("Assignment Name - " + strName + " exist in the Assignment list");
	
			}
			
			else {
					
					//Assert.assertTrue(false); 
					System.out.println("Assignment Name - " + strName + " does NOT exist in the Assignment list");
				
			}

		}
	}

	@Step("Validate Assignment Name Do NOT Exist, Method: {method}")
	public void validateAssignmentNameNOTExist(String strName) throws Exception {

		String strAssignmentName;
		WebElement assignmentName;

		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();

		for (int i = 1; i <= countAssignments; i++) {

			assignmentName = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tbody/tr[" + i + "]/td[1]"));
			strAssignmentName = assignmentName.getText();
			boolean assignmentNameExist = strAssignmentName.equals(strName);

			if (assignmentNameExist != true) {
				
				Assert.assertTrue(true);
				System.out.println("Assignment Name - " + strName + " does not exist in the Assignment list");
			}
			
			else {
				
				//Assert.assertTrue(false);
				System.out.println("Assignment Name - " + strName + " exist in the Assignment list");
				
			}

		}
	}

	@Step("Get Due Assignments in Beta View Assignment List, Method: {method}")
	public String[] getAssignmentList_AssignDue() throws Exception {

		WebElement assignmentName;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();

		String[] assignmentTitles_Due = new String[countAssignments];

		for (int i = 1; i <= countAssignments; i++) {

			assignmentName = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tbody/tr[" + i + "]/td[1]"));

			assignmentTitles_Due[i - 1] = assignmentName.getText();

		}

		return assignmentTitles_Due;

	}

	@Step("Get Completed Assignments in Beta View Assignment List, Method: {method}")
	public String[] getAssignmentList_AssignPast() throws Exception {

		WebElement assignmentName;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-complete']//tbody/tr")).size();

		String[] assignmentTitles_Complete = new String[countAssignments];

		for (int i = 1; i <= countAssignments; i++) {

			assignmentName = driver
					.findElement(By.xpath("//table[@class='course-table assign-complete']//tbody/tr[" + i + "]/td[1]"));

			assignmentTitles_Complete[i - 1] = assignmentName.getText();

		}

		return assignmentTitles_Complete;

	}

	@Step("Get Due Assignment Types in Beta View Assignment List, Method: {method}")
	public String[] getAssignmentTypeList_AssignDue() throws Exception {

		WebElement assignmentType;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();

		String[] assignmentTypes_Due = new String[countAssignments];

		for (int i = 1; i <= countAssignments; i++) {

			assignmentType = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tbody/tr[" + i + "]/td[2]"));

			assignmentTypes_Due[i - 1] = assignmentType.getText();

		}

		return assignmentTypes_Due;

	}

	@Step("Get Completed Assignments Types in Beta View Assignment List, Method: {method}")
	public String[] getAssignmentTypeList_AssignPast() throws Exception {

		WebElement assignmentType;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-complete']//tbody/tr")).size();

		String[] assignmentTypes_Complete = new String[countAssignments];

		for (int i = 1; i <= countAssignments; i++) {

			assignmentType = driver
					.findElement(By.xpath("//table[@class='course-table assign-complete']//tbody/tr[" + i + "]/td[2]"));

			assignmentTypes_Complete[i - 1] = assignmentType.getText();

		}

		return assignmentTypes_Complete;

	}

	@Step("Get Due Date under Due Assignments in Beta View Assignment List, Method: {method}")
	public List<Date> getDueDateList_AssignDue() throws Exception {

		WebElement assignmentDueDate;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();

		List<Date> assignmentDueDate_Due = new ArrayList<Date>();

		for (int i = 1; i <= countAssignments; i++) {

			assignmentDueDate = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tr[" + i + "]//td[3]//div[1]//div[1]"));

			String strDueDate = assignmentDueDate.getText();
			String[] dateArray = strDueDate.split("at");

			if (null != dateArray) {

				String date = dateArray[0];
				String time = dateArray[1];

				DateFormat df = new SimpleDateFormat("MMM dd, yyyy hh:mm a");

				try {
						Date dt = df.parse(date + " " + time);
						assignmentDueDate_Due.add(dt);

				} 	catch (Exception e) {

					e.printStackTrace();
				}
			}

		}

		return assignmentDueDate_Due;

	}
	
	
	
	@Step("Validate Due Dates for InQuizzitive in Beta View Assignment List, Method: {method}")
	public void validateDueDate_InQuizzitive(String assignmentTitle, String dueDate) throws Exception {
		
		WebElement assignmentName;
		WebElement assignmentDueDate;
		WebElement assignmentType;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();
		
		String strName;
		String strType;
		String assignmentDueDate_Due;
		
		DateFormat formatter = null;

		for (int i = 1; i <= countAssignments; i++) {

			assignmentName = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tr[" + i + "]//td[1]//a[1]"));
			
			assignmentType = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tr[" + i + "]//td[2]"));

			assignmentDueDate = driver
					.findElement(By.xpath("//table[@class='course-table assign-due']//tr[" + i + "]//td[3]//div[1]"));
			
			strName = assignmentName.getText();
			strType = assignmentType.getText();
			
			String strDueDate = assignmentDueDate.getText();
			String[] dateArray = strDueDate.split("at");

			if (null != dateArray) {

				String date = dateArray[0];
				String time = dateArray[1];

				try {
						
						formatter = new SimpleDateFormat("MMM dd, yyyy");
						Date tempGAU = formatter.parse(date);
					
						formatter = new SimpleDateFormat("MM/dd/yy");
						String tempDate = formatter.format(tempGAU);
						assignmentDueDate_Due = (tempDate+ "" + time);
						
						if(strName.equalsIgnoreCase(assignmentTitle) && strType.equals("InQuizitive")) {
							
						Assert.assertEquals(dueDate.equalsIgnoreCase(assignmentDueDate_Due), true);
						break;
					
					}

				} 	catch (Exception e) {

					e.printStackTrace();
				}
			}

		}

	}
	
	
	@Step("Validate Due Dates for SmartWork5 in Beta View Assignment List, Method: {method}")
	public void validateDueDate_SmartWork5(String assignmentTitle, String dueDate) throws Exception {

		WebElement assignmentName;
		WebElement assignmentDueDate;
		WebElement assignmentType;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr")).size();
		
		String strName;
		String strType;
		String assignmentDueDate_Due;
		
		DateFormat formatter = null;

		for (int i = 1; i <= countAssignments; i++) {

		assignmentName = driver
				.findElement(By.xpath("//table[@class='course-table assign-due']//tr[" + i + "]//td[1]//a[1]"));
		
		assignmentType = driver
				.findElement(By.xpath("//table[@class='course-table assign-due']//tr[" + i + "]//td[2]"));

		assignmentDueDate = driver
				.findElement(By.xpath("//table[@class='course-table assign-due']//tr[" + i + "]//td[3]//div[1]"));
		
		strName = assignmentName.getText();
		strType = assignmentType.getText();
		
		String strDueDate = assignmentDueDate.getText();
		String[] dateArray = strDueDate.split("at");

		if (null != dateArray) {

			String date = dateArray[0];
			String time = dateArray[1];

			try {
					
					formatter = new SimpleDateFormat("MMM dd, yyyy");
					Date tempGAU = formatter.parse(date);
				
					formatter = new SimpleDateFormat("MM/dd/yy");
					String tempDate = formatter.format(tempGAU);
					assignmentDueDate_Due = (tempDate+ "" + time);
		
					if(strName.equalsIgnoreCase(assignmentTitle) && strType.equals("Smartwork5")) {

						Assert.assertEquals(dueDate.equalsIgnoreCase(assignmentDueDate_Due), true);
						break;
					}

				} 	catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

	}
	
	
	@Step("Get Due Date under Due Assignments in Beta View Assignment List, Method: {method}")
	public List<Date> getDueDateList_AssignPast() throws Exception {

		WebElement assignmentDueDate;
		int countAssignments = driver.findElements(By.xpath("//table[@class='course-table assign-due']//tbody/tr"))
				.size();

		List<Date> assignmentDueDate_Past = new ArrayList<Date>();

		for (int i = 1; i <= countAssignments; i++) {

			assignmentDueDate = driver
					.findElement(By.xpath("//table[@class='course-table assign-complete']//tr[" + i + "]//td[3]//div[1]//div[1]"));

			String strDueDate = assignmentDueDate.getText();
			String[] dateArray = strDueDate.split("at");

			if (null != dateArray) {

				String date = dateArray[0];
				String time = dateArray[1];

				DateFormat df = new SimpleDateFormat("MMM dd, yyyy hh:mm a");

				try {
						Date dt = df.parse(date + " " + time);
						assignmentDueDate_Past.add(dt);

				} 	catch (Exception e) {

					e.printStackTrace();
				}
			}

		}

		return assignmentDueDate_Past;

	}
	
	

	@Step("Validate the presence of Sort Icons in Beta View Assignment List, Method: {method}")
	public void validateSortIcons_AssignDue() throws Exception {

		boolean nameSortIconsExist_AssignDue = true;
		boolean typeSortIconsExist_AssignDue = true;
		boolean dueDateSortIconsExist_AssignDue = true;
		boolean statusSortIconsExist_AssignDue = true;
		boolean gradeSortIconsExist_AssignDue = true;

		try {

			dueDateSortIconsExist_AssignDue = dueDateSortIcons_AssignDue.isDisplayed();
			nameSortIconsExist_AssignDue = assignmentNameSortIcons_AssignDue.isDisplayed();
			typeSortIconsExist_AssignDue = typeSortIcons_AssignDue.isDisplayed();
			statusSortIconsExist_AssignDue = statusSortIcons_AssignDue.isDisplayed();
			gradeSortIconsExist_AssignDue = gradeSortIcons_AssignDue.isDisplayed();

		}

		catch (NoSuchElementException e) {

			dueDateSortIconsExist_AssignDue = false;
			nameSortIconsExist_AssignDue = false;
			typeSortIconsExist_AssignDue = false;
			statusSortIconsExist_AssignDue = false;
			gradeSortIconsExist_AssignDue = false;

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());

		}

		if (dueDateSortIconsExist_AssignDue == true && nameSortIconsExist_AssignDue == true
				&& typeSortIconsExist_AssignDue == true && statusSortIconsExist_AssignDue == true
				&& gradeSortIconsExist_AssignDue == true) {

			System.out.println("All sort icons are displayed correctly under Due Assignment list");
		}
	}

	@Step("Validate the presence of Sort Icons in Beta View Assignment List, Method: {method}")
	public void validateSortIcons_AssignPast() throws Exception {

		boolean nameSortIconsExist_AssignPast = true;
		boolean typeSortIconsExist_AssignPast = true;
		boolean dueDateSortIconsExist_AssignPast = true;
		boolean statusSortIconsExist_AssignPast = true;
		boolean gradeSortIconsExist_AssignPast = true;

		try {

			dueDateSortIconsExist_AssignPast = dueDateSortIcons_AssignDue.isDisplayed();
			nameSortIconsExist_AssignPast = assignmentNameSortIcons_AssignDue.isDisplayed();
			typeSortIconsExist_AssignPast = typeSortIcons_AssignDue.isDisplayed();
			statusSortIconsExist_AssignPast = statusSortIcons_AssignDue.isDisplayed();
			gradeSortIconsExist_AssignPast = gradeSortIcons_AssignDue.isDisplayed();

		}

		catch (NoSuchElementException e) {

			dueDateSortIconsExist_AssignPast = false;
			nameSortIconsExist_AssignPast = false;
			typeSortIconsExist_AssignPast = false;
			statusSortIconsExist_AssignPast = false;
			gradeSortIconsExist_AssignPast = false;

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());

		}

		if (dueDateSortIconsExist_AssignPast == true && nameSortIconsExist_AssignPast == true
				&& typeSortIconsExist_AssignPast == true && statusSortIconsExist_AssignPast == true
				&& gradeSortIconsExist_AssignPast == true) {

			System.out.println("All sort icons are displayed correctly under Past Assignment list");
		}
	}

	@Step("Verify Grade Column as blank in Due Assignment: {method}")
	public void checkGradeColumnDueAssignment() throws Exception {

		WebElement DueAssignment, Status, Grade;
		DueAssignment = driver.findElement(
				By.xpath("/html[1]/body[1]/div[1]/div[1]/main[1]/div[2]/div[1]/table[1]/tbody[1]/tr[1]/td[2]"));

		try {

			boolean DueAssignmentExist = DueAssignment.isDisplayed();

			if (DueAssignmentExist == true) {

				int DueAssignmentCount = driver
						.findElements(By.xpath("//table[@class='course-table assign-due']/tbody/tr")).size();

				for (int i = 1; i <= DueAssignmentCount; i++) {

					Status = driver.findElement(
							By.xpath("/html[1]/body[1]/div[1]/div[1]/main[1]/div[2]/div[1]/table[1]/tbody[1]/tr[" + i
									+ "]/td[4]"));

					if (Status.getText().equalsIgnoreCase("Not Started")) {

						Grade = driver.findElement(
								By.xpath("/html[1]/body[1]/div[1]/div[1]/main[1]/div[2]/div[1]/table[1]/tbody[1]/tr["
										+ i + "]/td[5]"));
						String strGrade = Grade.getText();
						Assert.assertEquals(strGrade, "--");

					}
				}
			}

			else {

				System.out.println("Either Due Assignment is not Availble or Assignment is In Progress status");
			}

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}

	@Step("Verify Grade Column as blank in Past Assignment: {method}")
	public void checkGradeColumnPastAssignment() throws Exception {

		WebElement PastAssignment, Status, Grade;
		PastAssignment = driver.findElement(
				By.xpath("/html[1]/body[1]/div[1]/div[1]/main[1]/div[2]/div[2]/table[1]/tbody[1]/tr[1]/td[2]"));

		try {

			boolean PastAssignmentExist = PastAssignment.isDisplayed();

			if (PastAssignmentExist == true) {

				int PastAssignmentCount = driver
						.findElements(By.xpath("//table[@class='course-table assign-complete']/tbody/tr")).size();

				for (int i = 1; i <= PastAssignmentCount; i++) {

					Status = driver.findElement(
							By.xpath("/html[1]/body[1]/div[1]/div[1]/main[1]/div[2]/div[2]/table[1]/tbody[1]/tr[" + i
									+ "]/td[4]"));

					if (Status.getText().equalsIgnoreCase("Not Started")) {

						Grade = driver.findElement(
								By.xpath("/html[1]/body[1]/div[1]/div[1]/main[1]/div[2]/div[2]/table[1]/tbody[1]/tr["
										+ i + "]/td[5]"));
						String strGrade = Grade.getText();
						Assert.assertEquals(strGrade, "0%");

					}
				}
			}

			else {

				System.out.println("Either Past Assignment is not Availble or Assignment is In Progress status");
			}

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}

}
