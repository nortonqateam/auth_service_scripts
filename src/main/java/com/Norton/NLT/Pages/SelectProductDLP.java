package com.Norton.NLT.Pages;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class SelectProductDLP {
	
static WebDriver driver;
	
	
	//Finding WebElements on NLT Home page using PageFactory
	
	@FindBy(how = How.XPATH, using = "/html/body/div[6]")
	static WebElement selectSSPopUp;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'OK')]")
	static WebElement selectSSOkButton;

	@FindBy(how = How.XPATH, using = "//select[@id='report_class_menu']")
	public WebElement studentSetTitle;
	
	
	@FindBy(how = How.XPATH, using = "//select[@id='student_active_class_menu']")
	public WebElement studentSetTitle_activeMenu;
	
	
	@FindBy(how = How.XPATH, using = "//div[@id='return_to_resources_button']//img[@class='return_arrow']")
	public WebElement returnArrowIcon;
	
	
	// Initializing Web Driver and PageFactory.
	
	public SelectProductDLP(WebDriver driver) throws Exception {
		
		SelectProductDLP.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	@Step("Click on Product Tile in NCIA Home Page, Method: {method}")
	public static void clickProductImage(String productName) throws Exception {
		
		WebElement productTiles;
		
		int countProductImages = driver.findElements(By.xpath("//div[@id='activity_group_selectors']//button")).size();
		
		for (int i=1; i<=countProductImages; i++) {
		
				productTiles = driver.findElement(By.xpath("//div[@id='activity_group_selectors']//button[" + i + "]"));
				if (productTiles.getText().equalsIgnoreCase(productName)) {
					
					productTiles.click();
					break;
				}
					
			}
	}
	
	
	@Step("Click on Ok button if exist,  Method: {method} ")
	public static void clickOKButton() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(3);

		try {

			wait.until(ExpectedConditions.visibilityOf(selectSSPopUp));
			boolean selectSSPopUpExist = selectSSPopUp.isDisplayed();

			if (selectSSPopUpExist == true) {
				selectSSOkButton.click();
			}

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}
	
	
	@Step("Select Student Set ID by Visible Text on Product DLP page,  Method: {method} ")
	public void selectSSByTitle(String SSTitle) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.elementToBeClickable(studentSetTitle));
		Select drpStudentSet = new Select(studentSetTitle);
		drpStudentSet.selectByVisibleText(SSTitle);

	}
	
	
	@Step("Select Active Student Set ID by Visible Text on Product DLP page,  Method: {method} ")
	public void selectActiveSSByTitle(String SSTitle) throws Exception {
	
	boolean studentSetDrpDownExist;
	
	try {
			studentSetDrpDownExist = studentSetTitle_activeMenu.isDisplayed();
			
			if (studentSetDrpDownExist == true) {
		
				Select drpStudentSet = new Select(studentSetTitle_activeMenu);
				drpStudentSet.selectByVisibleText(SSTitle);
		
			}
		}
			
		catch (NoSuchElementException e) 
			{

				studentSetDrpDownExist = false;
		
			}
	
			catch (Exception e) {
				
				System.out.println("Exception Handlled: " + e.getMessage());

			}
	}

}
