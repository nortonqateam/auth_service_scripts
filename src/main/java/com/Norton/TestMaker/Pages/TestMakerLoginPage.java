package com.Norton.TestMaker.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.LogUtil;
import Utilities.ReadJsonFile;
import ru.yandex.qatools.allure.annotations.Step;

public class TestMakerLoginPage 
{
	WebDriver driver;

	// Finding Web Elements on New Question page using PageFactory.

	@FindBy(how = How.ID, using = "loginEmail")
	public WebElement TestMakerLoginEmail;
	
	@FindBy(how = How.ID, using = "loginPassword")
	public WebElement TestMakerLoginPassword;
	
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement TestMakerLogInButton;
	
	@FindBy(how = How.XPATH, using = "//button[@name='profile']")
	public WebElement ProfileName;
	
	@FindBy(how = How.XPATH, using = "//div[@id='regionYourTestBank']/div/div[@class='left']")
	public WebElement bookdetails;
	
	@FindBy(how = How.XPATH, using ="//main[@class='full-height full-width clearfix flex flex-column items-center pt4']/p[@class='h2']")
	public WebElement YouDontacessHeaderText;
	
	@FindBy(how = How.XPATH, using ="//main[@class='full-height full-width clearfix flex flex-column items-center pt4']/p[@class='large-text']")
	public WebElement LargeText;
	
	@FindBy(how = How.XPATH, using ="//main[@class='full-height full-width clearfix flex flex-column items-center pt4']/p/button[contains(text(),'contact us')]")
	public WebElement ContactUsbutton;
	
	@FindBy(how = How.ID, using ="__link__Click__here__to__Request__Access__0")
	public WebElement RequestAccessbutton;
	
	
	ReadJsonFile readJsonObject = new ReadJsonFile();
	com.google.gson.JsonObject jsonObj = readJsonObject.readJason();	
	
	// Initializing Web Driver and PageFactory.
		public TestMakerLoginPage(WebDriver driver) throws Exception {

			this.driver = driver;
			PageFactory.initElements(driver, this);
			
		}
		
		@Step("Instructor Login to Test Maker Application,  Method: {method} ")
		public void loginTestMakerApp() {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("loginEmail")));
			String userName = jsonObj.getAsJsonObject("TMInstructorLoginCredentials").get("userName").getAsString();
			TestMakerLoginEmail.sendKeys(userName);
			String passWord = jsonObj.getAsJsonObject("TMInstructorLoginCredentials").get("password").getAsString();
			TestMakerLoginPassword.sendKeys(passWord);
			TestMakerLogInButton.click();
		}
		
		@Step("Unauthorised User logs to Test Maker Application,  Method: {method} ")
		public void unAuthLoginTestMakerApp() {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("loginEmail")));
			String userName = jsonObj.getAsJsonObject("TMNONInstructorLoginCredentials").get("userName").getAsString();
			TestMakerLoginEmail.sendKeys(userName);
			String passWord = jsonObj.getAsJsonObject("TMNONInstructorLoginCredentials").get("password").getAsString();
			TestMakerLoginPassword.sendKeys(passWord);
			TestMakerLogInButton.click();
		}
		
		@Step("Verify Profile Information,  Method: {method} ")
		public String profileTestMakerApp() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("__input__button__profile__0")));
			String userName = ProfileName.getText();
			LogUtil.log(userName);
			return userName;
		}
		
		@Step("get the book details,  Method: {method} ")
		public String getbookinformation() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			String bookInfo = bookdetails.getText();
			System.out.println(bookInfo);
			LogUtil.log(bookInfo);
			return bookInfo;
			
		}
		
		@Step("Verify Unauthorized Message,  Method: {method} ")
		public String getUnauthorizedMessage() throws InterruptedException {
			Thread.sleep(5000);
			String getUnauthorizedMessage;
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("__input__button__profile__0")));
			String youdonthaveaccess =YouDontacessHeaderText.getText();
			String largeText =LargeText.getText();
			//String contactus =ContactUsbutton.getText();
			String requestAccess =RequestAccessbutton.getText();
			getUnauthorizedMessage =youdonthaveaccess+"\n"+largeText+" "+"\n"+"or"+"\n"+requestAccess+"";
			return getUnauthorizedMessage;
		}
		
		@Step("Click Contact Us button and Application open Smartsheet application in another tab,  Method: {method} ")
		public void clickContactUsButton() throws InterruptedException {
			ContactUsbutton.click();
			String winHandleBefore = driver.getWindowHandle();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			Thread.sleep(5000);
			WebDriverWait  wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='css-h3zj1a']/section[@class='css-474oue']")));
			WebElement testmakerAlphaTestReportsText =driver.findElement(By.xpath("//div[@class='css-h3zj1a']/section[@class='css-474oue']"));
			Assert.assertEquals("Testmaker Alpha Test Reports", testmakerAlphaTestReportsText.getText());
			driver.close();
			driver.switchTo().window(winHandleBefore);
		}
		
		@Step("Click Request Access button and Application open Website application in another tab and respective ISBN Page and displays Request Access button,  Method: {method} ")
		public void clickRequestAccessButton() throws InterruptedException {
			RequestAccessbutton.click();
			String winHandleBefore = driver.getWindowHandle();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			Thread.sleep(5000);
			WebDriverWait  wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/button[@class='center'][contains(text(),'Request Access')]")));
			WebElement requestAccessbutton =driver.findElement(By.xpath("//div/button[@class='center'][contains(text(),'Request Access')]"));
			Assert.assertEquals("Request Access", requestAccessbutton.getText());
			driver.close();
			driver.switchTo().window(winHandleBefore);
		}
		
		@Step("get the book title,  Method: {method} ")
		public String getbooktitle() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			WebElement booktitle = driver.findElement(By.xpath("//div[@class='left']/div/div[@class='bold'][1]"));
			String bookTitle = booktitle.getText();
			LogUtil.log(bookTitle);
			return bookTitle;
			
		}
		
		@Step("get the book sub title,  Method: {method} ")
		public String getbooksubtitle() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			WebElement booksubtitle = driver.findElement(By.xpath("//div[@class='left']/div/div[@class='bold'][2]"));
			String bookSubTitle = booksubtitle.getText();
			LogUtil.log(bookSubTitle);
			return bookSubTitle;
			
		}
		
		@Step("get the book Edition,  Method: {method} ")
		public String getbookEdition() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			WebElement bookedition = driver.findElement(By.xpath("//div[@class='left']/div/div[3]"));
			String bookEdition = bookedition.getText().trim();
			LogUtil.log(bookEdition);
			return bookEdition;
			
		}
		
		@Step("get the book Authors,  Method: {method} ")
		public String getbookAuthors() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			WebElement bookauthor = driver.findElement(By.xpath("//div[@class='left']/div/div[@class='italic']"));
			String bookAuthors = bookauthor.getText();
			LogUtil.log(bookAuthors);
			return bookAuthors;
		}
}
