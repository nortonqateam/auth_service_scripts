package com.Norton.TestMaker.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class TestMakerLogOutPage {
	WebDriver driver;
	
	@FindBy(how = How.ID, using ="__link__Log__Out__0")
	public WebElement LogOutTestMaker;
	
	@FindBy(how = How.XPATH, using ="//button[@name='profile']")
	public WebElement clickIconGearButton;
	
	
	
	// Initializing Web Driver and PageFactory.
			public TestMakerLogOutPage(WebDriver driver) throws Exception {

				this.driver = driver;
				PageFactory.initElements(driver, this);
				
			}
			@Step("LogOut TestMaker application,  Method: {method} ")
			public void logOutTestMakerApp() throws InterruptedException {
				WebDriverWait waitgearButton = new WebDriverWait(driver, 50);
				waitgearButton.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@name='profile']")));
				clickIconGearButton.click();
				LogOutTestMaker.click();
				
			}
}
