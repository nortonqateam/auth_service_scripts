package com.Norton.TestMaker.Pages;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.GetRandomId;
import Utilities.LogUtil;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.google.gson.JsonObject;



public class CreateNewTestPage {
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create New Test')]")
	public WebElement createNewTestButton;
	
	@FindBy(how = How.ID, using = "__input__text__testName__0")
	public WebElement TestNameTextbox;
	
	@FindBy(how = How.ID, using = "__input__text__courseName__0")
	public WebElement CourseNameTextbox;
	
	@FindBy(how = How.ID, using = "__input__button__Create__Test__0")
	public WebElement CreateTestButton;
	
	@FindBy(how = How.XPATH, using ="//div[@class='bg-white m1-5 xs-col-4 sm-col-4 md-col-8 lg-col-10 lg-col-12']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']/div")
	public WebElement createdTestName;
	
	@FindBy(how = How.XPATH, using ="//div[@class='bg-white m1-5 xs-col-4 sm-col-4 md-col-8 lg-col-10 lg-col-12']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']/div[@class='h4']")
	public WebElement createdCourseName;
	
	@FindBy(how = How.ID, using = "__input__button__Save__0")
	public WebElement SaveButton;
	
	@FindBy(how = How.ID, using = "__input__button__back__0")
	public WebElement TestmakerBackButton;
	
	
	@FindBy(how = How.XPATH, using = "//div/div[@class='pt1 italic red bold']")
	public WebElement CreateTestErrorMessage;
	
	@FindBy(how = How.ID, using = "__input__button__closeModal__1")
	public WebElement createTestCloseModal;
	
	@FindBy(how = How.XPATH, using = "//span[@class='h1'][contains(.,'Your Tests')]")
	public WebElement yourTestText;
	
	ReadJsonFile readJsonObject = new ReadJsonFile();
	JsonObject jsonObj = readJsonObject.readJason();	
	
	// Initializing Web Driver and PageFactory.
			public CreateNewTestPage(WebDriver driver) throws Exception {

				this.driver = driver;
				PageFactory.initElements(driver, this);
				
			}
			
			@Step("Click Create New Test Button,  Method: {method} ")
			public void clickCreateNewTest() throws InterruptedException {
				ReusableMethods.checkPageIsReady(driver);
				WebDriverWait wait = new WebDriverWait(driver, 5000L);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Create New Test')]")));
				Thread.sleep(3000);
				createNewTestButton.click();
			}
			
			
			@Step("Create a New Test by providing the TestName and Course Name,  Method: {method} ")
			public String createNewTestwithTestNameCourseName() throws InterruptedException {
				ReusableMethods.checkPageIsReady(driver);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("__input__text__testName__0")));
				String testName = jsonObj.getAsJsonObject("TMTestName").get("testName").getAsString();
				String guid = GetRandomId.randomAlphaNumeric(3).toLowerCase();
				guid = guid.replaceAll("[^a-zA-Z0-9]+", "a");
				String createdTestName = testName + guid ;
				TestNameTextbox.sendKeys(createdTestName);
				String courseName = jsonObj.getAsJsonObject("TMTestName").get("courseName").getAsString();
				CourseNameTextbox.sendKeys(courseName);
				CreateTestButton.click();
				return createdTestName;
			}
			
			@Step("Create a New Test by providing the TestName with 100 char and Course Name with 36 char ,  Method: {method} ")
			public String[] createNewTestwithTestNameCourseNameMaxChar() throws InterruptedException {
				ReusableMethods.checkPageIsReady(driver);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("__input__text__testName__0")));
				String testName = jsonObj.getAsJsonObject("TMTestName").get("testName").getAsString();
				String guid = GetRandomId.randomAlphaNumeric(96).toLowerCase();
				guid = guid.replaceAll("[^a-zA-Z0-9]+", "a");
				String createdTestName = testName + guid ;
				TestNameTextbox.sendKeys(createdTestName);
				String courseName = jsonObj.getAsJsonObject("TMTestName").get("courseName").getAsString();
				String courseNameId = GetRandomId.randomAlphaNumeric(36).toLowerCase();
				courseNameId = courseNameId.replaceAll("[^a-zA-Z0-9]+", "a");
				String generatedCourseName = courseName + courseNameId ;
				CourseNameTextbox.sendKeys(generatedCourseName);
				CreateTestButton.click();
				return new String[]{createdTestName,generatedCourseName};
			
			}
			
			
			@Step("Verify Test has been created on Build Test Page,  Method: {method} ")
			public String getTestName() throws InterruptedException {
				Thread.sleep(2000);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='bg-white m1-5 xs-col-4 sm-col-4 md-col-8 lg-col-10 lg-col-12']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']")));
				String testName= createdTestName.getText();
				LogUtil.log(testName);
				return testName;
			}
			
			@Step("Verify Test has been created on Build Test Page get the Test Name and Course Name,  Method: {method} ")
			public String[] getTestCourseName() throws InterruptedException {
				Thread.sleep(2000);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='bg-white m1-5 xs-col-4 sm-col-4 md-col-8 lg-col-10 lg-col-12']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']")));
				String testName= createdTestName.getText();
				LogUtil.log(testName);
				String courseName= createdCourseName.getText();
				return new String[] {testName, courseName};
			}
			
			@Step("Click the Save Button,  Method: {method} ")
			public void clickSave() throws InterruptedException {
				Thread.sleep(2000);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("__input__button__Save__0")));
				SaveButton.click();
			}

			
			@Step("Click the Test Maker Back Button,  Method: {method} ")
			public void clickTestMakerBackButton() throws InterruptedException {
				Thread.sleep(2000);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("__input__button__back__0")));
				TestmakerBackButton.click();
				Thread.sleep(4000);
			}
			
			
			@Step("Verify the Page Title,  Method: {method} ")
			public String pageTitle() throws InterruptedException {
				Thread.sleep(2000);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='h1'][contains(.,'Your Tests')]")));
			    return yourTestText.getText();
			}
			
			
			@Step("Verify the Default Sort Name,  Method: {method} ")
			public String defaultSortBy() throws InterruptedException {
				Thread.sleep(2000);
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='relative inline-block p0 m0']/button/span")));
				WebElement lastModifiedlabelElement = driver.findElement(By.xpath("//div[@class='relative inline-block p0 m0']/button/span"));
				LogUtil.log("Default Sort Name", lastModifiedlabelElement.getText());
			    return lastModifiedlabelElement.getText();
			}
			
			@Step("Verify sort by options,  Method: {method} ")
			public String getSortByOptionsName() {
				List<String> sortOptionslabellist=new ArrayList<>();
				String sortOptionslabel= null;
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='relative inline-block p0 m0']/button/span")));
				WebElement placeHolderlabelElement = driver.findElement(By.xpath("//div[@class='relative inline-block p0 m0']/button/span"));
				placeHolderlabelElement.click();
				List<WebElement> sortOptions = driver.findElements(By.xpath("//div[@class='relative inline-block p0 m0']/div/button"));
				for(int i=0; i<sortOptions.size(); i++){
				sortOptionslabel=sortOptions.get(i).getText();
				sortOptionslabellist.add(sortOptionslabel);
				}
				placeHolderlabelElement.click();
				return sortOptionslabellist.toString();
				 				
			}
			
			@Step("Verify the Sorting Order for Last Modified in descending order,  Method: {method} ")
			public void sortOrderforLastModified(){
				ArrayList<String> obtainedList = new ArrayList<>();
				List<WebElement> elementList= driver.findElements(By.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div[@class='col md-col-6 m0 p0 bg-white flex flex-column justify-start']/span[starts-with(text(),'Last Modified')]"));
				for(WebElement elements:elementList){
					   obtainedList.add(elements.getText());
				}
				ArrayList<String> sortedList = new ArrayList<>();   
				for(String s:obtainedList){
				sortedList.add(s);
				}
				//Collections.reverse(sortedList);
				//Collections.sort(sortedList);
				
				Assert.assertTrue(sortedList.equals(obtainedList));
			}
			
			@Step("Select the Sort options from Sort By Option list,  Method: {method} ")
			public void selectSortByOptionsName(String sortName) {
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='relative inline-block p0 m0']/button/span")));
				WebElement placeHolderlabelElement = driver.findElement(By.xpath("//div[@class='relative inline-block p0 m0']/button/span"));
				placeHolderlabelElement.click();
				WebElement sortNameelement = driver.findElement(By.xpath("//button[@class='bg-white body-text black py1 px2 border-none outline-none text-decoration-none block left-align dropdownOption full-width'][contains(.,'"+sortName+"')]"));
				sortNameelement.click(); 				
			}
			
			@Step("Verify the Sorting Order for Test Name in ascending order,  Method: {method} ")
			public void sortbyTestName(){
			List<WebElement> testName_Webelement = new LinkedList<WebElement>();
			testName_Webelement= driver.findElements(By.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div[@class='col md-col-6 m0 p0 bg-white flex flex-column justify-start']/div/a"));
			LinkedList<String> testNames =  new LinkedList<String>();
			for(int i=0; i<testName_Webelement.size(); i++){
				String testName =testName_Webelement.get(i).getText();
				testNames.add(testName);
			}
			boolean result = sortbyAlpha_order(testNames);
			Assert.assertTrue(result);
}
			
			@Step("Verify the Sorting Order for Course in ascending order,  Method: {method} ")
			public void sortbyCourseName(){
			List<WebElement> course_Webelement = new LinkedList<WebElement>();
			course_Webelement= driver.findElements(By.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div[@class='col md-col-6 m0 p0 bg-white flex flex-column justify-start']/span[1]"));
			LinkedList<String> courseNames =  new LinkedList<String>();
			for(int i=0; i<course_Webelement.size(); i++){
				String courseName =course_Webelement.get(i).getText();
				courseNames.add(courseName);
			}
			boolean courseresult = sortbyAlpha_order(courseNames);
			Assert.assertTrue(courseresult);
}			
	
			
			
			@Step("Verify the Sorting Order for Last Modified in descending order,  Method: {method} ")
			public void sortbyLastModified(){
				List<WebElement> lastModified_Webelement = new LinkedList<WebElement>();
				lastModified_Webelement= driver.findElements(By.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div[@class='col md-col-6 m0 p0 bg-white flex flex-column justify-start']/span[starts-with(text(),'Last Modified')]"));
				LinkedList<String> lastModified =  new LinkedList<String>();
				for(int i=0; i<lastModified_Webelement.size(); i++){
					String lastmodifiedDate =lastModified_Webelement.get(i).getText();
					lastModified.add(lastmodifiedDate);
				}
				boolean lastModifiedresult = sortbyAlpha_order(lastModified);
				Assert.assertTrue(lastModifiedresult);
			}

			private boolean sortbyAlpha_order(LinkedList<String> sortNames) {
				// TODO Auto-generated method stub
				String previous = ""; // empty string
				 for (final String current: sortNames) {
					     if (current.compareToIgnoreCase(previous) < 0)
				            return false;
				            previous = current;
				        
				    }

				return true;
			}
}
