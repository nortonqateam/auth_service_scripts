package com.Norton.TestMaker.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class RegionYourTestsPage {

	WebDriver driver;
	@FindBy(how = How.ID, using = "__input__button__Delete__Test__0")
	public WebElement DeleteTestButton;

	@FindBy(how = How.XPATH, using = "//span[@class='bg-lighterGray mx2 mt2 break-word  py4 px3 center large-body-text'][contains(.,'You haven’t created any tests yet.Create a new test to get started!')]")
	public WebElement youHavenTCreatedAnyTestText;

	// Initializing Web Driver and PageFactory.
	public RegionYourTestsPage(WebDriver driver) throws Exception {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@Step("Verify Created TestName is displayed in Region your Tests Section,  Method: {method} ")
	public String clickTestNamelink(String testName) {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div/div/a")));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> linkstext = driver
				.findElements(By
						.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div/div/a"));
		for (int j = 0; j < linkstext.size(); j++) {
			String createdTestNameText = linkstext.get(j).getText();
			if (createdTestNameText.equalsIgnoreCase(testName)) {
				linkstext.get(j).click();
				break;

			}
		}

		return testName;

	}

	@SuppressWarnings("unused")
	@Step("get Created TestName is displayed in Region your Tests Section,  Method: {method} ")
	public String getTestNamelinkfromList() {
		String createdTestNameText = null;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> linkstext = driver
				.findElements(By
						.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div/div/a"));
		for (int j = 0; j < linkstext.size(); j++) {
			createdTestNameText = linkstext.get(j).getText();
			break;
		}

		return createdTestNameText;
	}

	@Step("Click the Action link against the created Test Name,  Method: {method} ")
	public void clickActionlink(String testName) {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> linkstext = driver
				.findElements(By
						.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div/div/a"));
		List<WebElement> actionlink = driver
				.findElements(By
						.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div/div/a/following::div[@class='col md-col-2 m0 p0 bg-white flex flex-row justify-end']/div[@class='p0 m0 relative inline-block']/button"));
		for (int j = 0; j < linkstext.size(); j++) {
			String createdTestNameText = linkstext.get(j).getText();
			for (int k = 0; k < actionlink.size(); k++) {
				if (createdTestNameText.equalsIgnoreCase(testName)) {
					actionlink.get(k).click();
					break;
				}
			}

		}
	}

	@Step("Click the Export, Copy or Delete link against the created Test Name,  Method: {method} ")
	public void clickExportOrCopyOrDeletelink(String actionsName) {
		WebElement elementtoclick = driver
				.findElement(By
						.xpath("//div[@class='p0 m0 relative inline-block']/div/button[contains(text(),'"
								+ actionsName + "')]"));
		elementtoclick.click();
	}

	@Step("Verify You haven’t created any tests yet.Create a new test to get started!',  Method: {method} ")
	public String getTextCreateNewTest() {
		String text = youHavenTCreatedAnyTestText.getText();
		if (text.equalsIgnoreCase("You haven’t created any tests yet.Create a new test to get started!"))
			;
		return text;
	}
}
