package com.Norton.LMS.Pages;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.GetRandomId;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.google.gson.JsonObject;

public class LMSCANVAS {

	WebDriver driver;
	

	@FindBy(how = How.ID, using = "pseudonym_session_unique_id")
	public WebElement LMS_Email_ID;

	@FindBy(how = How.ID, using = "pseudonym_session_password")
	public WebElement LMS_Email_Password;

	@FindBy(how = How.XPATH, using = "//div[@class='ic-Form-control ic-Form-control--login']/button[@type='submit']")
	public WebElement LoginButton;

	@FindBy(how = How.XPATH, using = "//a[@id='global_nav_dashboard_link']")
	public WebElement DashboardLink;

	
	@FindBy(how = How.XPATH, using = "//div[@id='nav-tray-portal']/span/span/div/div/div/div/ul/li/a/span[contains(text(),'Automation_Data_Home')]")
	public WebElement CoursesLink;

	
	@FindBy(how = How.XPATH, using = "//div[@class='ig-info']/a[1]")
	public WebElement ClickAssignments;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement LoadAssignmentButton;

	@FindBy(how = How.ID, using = "start_new_course")
	public WebElement StartNewCourseButton;

	@FindBy(how = How.ID, using = "course_name")
	public WebElement CourseName;

	@FindBy(how = How.ID, using = "course_license")
	public WebElement Courselicense_list;

	@FindBy(how = How.ID, using = "course_is_public")
	public WebElement course_is_public_checkbox;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Create course')]")
	public WebElement CreateCourseButton;

	@FindBy(how = How.XPATH, using = "//nav[@role='navigation']/ul/li[@class='section']/a[@class='settings']")
	public WebElement Settingslink;

	@FindBy(how = How.XPATH, using = "//div[@id='course_details_tabs']/ul/li[@id='external_tools_tab']/a[@id='tab-tools-link']")
	public WebElement AppsTab;

	@FindBy(how = How.XPATH, using = "//div[@class='externalApps_buttons_container']/a[@href='/courses/2610427/settings/configurations']")
	public WebElement ViewAppConfigurationslink;

	@FindBy(how = How.XPATH, using = "//div[@class='externalApps_buttons_container']/span[@class='AddExternalToolButton']/a[@href='#']")
	public WebElement APPlink;

	@FindBy(how = How.ID, using = "configuration_type_selector")
	public WebElement ConfigurationType;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Name']")
	public WebElement NameTextbox;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Consumer Key']")
	public WebElement ConsumerKey;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Shared Secret']")
	public WebElement SharedSecret;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Launch URL']")
	public WebElement LaunchURL;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Domain']")
	public WebElement Domain;

	@FindBy(how = How.ID, using = "submitExternalAppBtn")
	public WebElement Submitbutton;
	
	@FindBy(how =How.LINK_TEXT, using ="Assignment")
	public WebElement Assignmentlink;
	
	@FindBy(how =How.ID, using ="assignment_name")
	public WebElement assignment_nameTextbox;
	
	@FindBy(how =How.ID, using ="assignment_points_possible")
	public WebElement assignment_points_possibleTextbox;
	
	@FindBy(how =How.ID, using ="assignment_external_tool_tag_attributes_url")
	public WebElement  ExternalToolTextbox;
	
	@FindBy(how =How.XPATH, using ="//button[@class='btn btn-default save_and_publish']")
	public WebElement  SaveAndPublish;

	public LMSCANVAS(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	@Step("Enter an instructor credenatils and click the Login button,  Method: {method} ")
	public void instloginLMSCANVAS() {
		String LMSUserName = jsonobject.getAsJsonObject("LMSUser")
				.get("LMSINSTUserLogin").getAsString();
		LMS_Email_ID.sendKeys(LMSUserName);
		String LMSPassword = jsonobject.getAsJsonObject("LMSUser")
				.get("LMSINSTUserPassword").getAsString();
		LMS_Email_Password.sendKeys(LMSPassword);
		LoginButton.click();
	}
	

	@Step("Click on the link from left menu item list linkName {0}, Method:{method} ")
	public void clicklink(String linkName) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@id='menu']")));
		WebElement menuItemList = driver.findElement(By.xpath("//ul[@id='menu']/li/a/div[contains(text(),'"+linkName+"')]"));
		ReusableMethods.clickElement(driver, menuItemList);
		
	}
	@Step("Click on the Start New Couse Button, Method:{method} ")
	public void clickStartNewCouseButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("start_new_course")));
		StartNewCourseButton.click();
	}
	
	@Step("Enter a Course Name, Method:{method} ")
	public void enterCourseName() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("course_name")));
		CourseName.click();
		CourseName.sendKeys("LMSAutomation"+GetRandomId.randomAlphaNumeric(3));
	}
	
	@Step("Click Create Course Button, Method:{method} ")
	public void clickCreateCourseButton() {
		CreateCourseButton.click();
	}
	
	@Step("Click Navigation link navigationLink {0}, Method:{method} ")
	public void clickNavigationlink(String navigationLink) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='left-side']/nav[@role='navigation']")));
		WebElement navigationItemList = driver.findElement(By.xpath("//div[@id='left-side']/nav[@role='navigation']/ul[@id='section-tabs']/li/a[contains(text(),'"+navigationLink+"')]"));
		ReusableMethods.clickElement(driver, navigationItemList);
		
	}
	
	@Step("Click Course details Tab courseDetailsTab {0} link, Method:{method} ")
	public void clickcourseDetailsTab(String courseDetailsTab) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='course_details_tab']")));
		WebElement courseDetailsTabList = driver.findElement(By.xpath("//div[@id='course_details_tabs']/ul/li/a[contains(text(),'"+courseDetailsTab+"')]"));
		ReusableMethods.clickElement(driver, courseDetailsTabList);
	}
	
	@Step("Click View App Configurations link , Method:{method} ")
	public void clickViewAppConfigurations() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='externalApps_buttons_container']")));
		WebElement ViewAppConfigurations = driver.findElement(By.xpath("//div[@class='externalApps_buttons_container']/a[contains(text(),'View App Configurations')]"));
		ReusableMethods.clickElement(driver, ViewAppConfigurations);
	}
	
	@Step("Click +App Button, Method:{method} ")
	public void clickAppButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='externalApps_buttons_container']")));
		APPlink.click();
	}
	
	@Step("Enter an Information in Add App section, Method:{method} ")
	public void enterAppInfo() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(NameTextbox));
		NameTextbox.click();
		NameTextbox.sendKeys("AppAssignment"+GetRandomId.randomAlphaNumeric(2));
		String ConsumerKeys = jsonobject.getAsJsonObject("LMSKeys").get("ConsumerKey").getAsString();
		ConsumerKey.sendKeys(ConsumerKeys);	
		SharedSecret.sendKeys(jsonobject.getAsJsonObject("LMSKeys").get("SharedSecret").getAsString());
	}
	
	@Step("Enter an launch URL Information in Add App section, Method:{method} ")
	public void enterAppInfoLaunchURL() throws MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(LaunchURL));
		LaunchURL.click();
		String launchURL= jsonobject.getAsJsonObject("LMSKeys").get("LaunchURL").getAsString();
		LaunchURL.sendKeys(launchURL);
		URL u = new URL(launchURL);
		String hostURL =u.getHost();
	    Domain.sendKeys("https://"+hostURL);
	}
	
	@Step("Click Submit Button in Add App section, Method:{method} ")
	public void clickSubmitButton()  {
		Submitbutton.click();
		
	}
	
	@Step("Click Assignment link from Assignment page, Method:{method} ")
	public void clickAssignmentlink()  {
		Assignmentlink.click();
		
	}
	@Step("Enter Assignment Name from Assignment page, Method:{method} ")
	public void enterAssignmentName()  {
		assignment_nameTextbox.click();
		assignment_nameTextbox.sendKeys("TestAssignment"+GetRandomId.randomAlphaNumeric(2));
		
	}
	@Step("Enter Points in Assignment page, Method:{method} ")
	public void enterPoints(String points)  {
		assignment_points_possibleTextbox.clear();
		assignment_points_possibleTextbox.click();
		assignment_points_possibleTextbox.sendKeys(points);
		
	}
	@Step("Select Assignments from Assignment Group  , Method:{method} ")
	public void selectAssignmentGroup(String assignmentgroupname)  {
		Select assignmentGroup = new Select(driver.findElement(By.id("assignment_group_id")));
		assignmentGroup.selectByVisibleText(assignmentgroupname.trim());
		
	}
	@Step("Select Grades from Display grade select list , Method:{method} ")
	public void selectGrades(String gradeName)  {
		Select assignmentGroup = new Select(driver.findElement(By.id("assignment_grading_type")));
		assignmentGroup.selectByVisibleText(gradeName.trim());
		
	}
	@Step("Select Submission Type from list, Method:{method} ")
	public void selectSubmissionType(String submissionType)  {
		Select assignmentGroup = new Select(driver.findElement(By.id("assignment_submission_type")));
		assignmentGroup.selectByVisibleText(submissionType.trim());
		
	}
	
	@Step("Enter or find an External Tool URL, Method:{method}")
	public void enterExternalURL()  {
		ExternalToolTextbox.click();
		ExternalToolTextbox.sendKeys(jsonobject.getAsJsonObject("LMSKeys").get("LaunchURL").getAsString());
	}
	
	@Step("Click the Checkbox Load This Tool In A New Tab,Method:{method}")
	public void clickChekboxLoadThisToolInANewTab()  {
	//	WebElement labelCheckbox = driver.findElement(By.xpath("//label[@class='checkbox']/input[@type='checkbox']"));
		((JavascriptExecutor)driver).executeScript("document.getElementById('assignment_external_tool_tag_attributes_new_tab').click()");
	}
	
	@Step("Click Save and Publish Button ,Method:{method}")
	public void clickSaveandPublishButton()  {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(SaveAndPublish));
		SaveAndPublish.click();	
	}
	@Step("Click the Publish Button, Method:{method}")
	public void clickPublishButton()  {
		WebElement publishButton = driver.findElement(By.xpath("//button[@type='submit']"));
		publishButton.click();
	}
	
	@Step("Click on Assignment Button, Method:(method)")
	public void clickAssignmenthButton(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn']")));
		WebElement AssignmentButton = driver.findElement(By.xpath("//button[@class='btn']"));
		AssignmentButton.click();
	}
	@Step("Get NCIA Digital resource Window,  Method: {method} ")
	public String getNCIAWindow(WebDriver driver) throws Exception {
		String childWindow = null;
		for (String winHandle : driver.getWindowHandles()) {
			childWindow = winHandle;
			driver.switchTo().window(winHandle);
		}
		driver.switchTo().frame("tool_content");
		
		return childWindow;
	}
}
