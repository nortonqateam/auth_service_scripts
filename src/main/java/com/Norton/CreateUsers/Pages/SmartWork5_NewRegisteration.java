package com.Norton.CreateUsers.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.GetRandomId;

public class SmartWork5_NewRegisteration {
	static final Logger log = Logger.getLogger(SmartWork5_NewRegisteration.class);
	
	WebDriver driver;
	WebDriverWait wait;
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public
	WebElement Sign_in_or_Register; 
	
	@FindBy(how =How.XPATH, using="//label[@for='register_login_choice_register']/span/span")
	public WebElement Need_to_register;
	
	@FindBy(how =How.XPATH, using = "//button[@id='login_dialog_button']")
	public
	WebElement SignInButton;
	
	@FindBy(how =How.ID, using="name")
	public WebElement FirstName_LastName;
	
	@FindBy(how =How.XPATH, using = "//input[@type='email']")
	public
	WebElement StudentEmail;
	
	@FindBy(how =How.XPATH, using = "//input[@type='password']")
	public
	WebElement Password;
	
	@FindBy(how =How.ID, using = "password2")
	public
	WebElement Password2;
	
	@FindBy(how =How.ID, using = "register_purchase_choice_trial")
	public WebElement register_purchase_choice_trial;
	
	@FindBy(how =How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement    Sign_Up_for_Trial_AccessButton;
	
	@FindBy(how =How.XPATH, using ="//button[@type='button']/span[contains(text(),'OK, Sign Up for Trial Access')]")
	public WebElement OK_SignUpforTrialAccess_button;
	
	@FindBy(how =How.XPATH, using = "//span[contains(text(),'Confirm')]/ancestor::button")
	public WebElement Confirm_Button;
	
	@FindBy(how =How.XPATH, using = "//input[@type='checkbox']")
	public WebElement Checkbox;
	
	@FindBy(how =How.ID, using = "login_dialog_school_type_select")
	public WebElement school_dropdown;
	
	@FindBy(how =How.ID, using = "login_dialog_country_select")
	public WebElement Select_Country;
	
	@FindBy(how =How.ID, using = "login_dialog_state_select")
	public WebElement Select_State;
	
	@FindBy(how =How.ID, using ="login_dialog_school_text_input")
	public WebElement School_Institution;
	
	@FindBy(how =How.XPATH, using = "//ul[@id='ui-id-4']/li")
	public WebElement School_Selection;
	
	@FindBy(how =How.XPATH, using = "//span[contains(text(),'Continue')]/ancestor::button")
	public WebElement Continue_Button;
	
	@FindBy(how =How.ID, using ="login_dialog_error_msg")
	public WebElement Error_Msg;
	
	@FindBy(how =How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public
	WebElement oKbutton;
	@FindBy(how =How.XPATH, using = "//span[@id='gear_button_username']")
	public
	WebElement username;
		
	@FindBy(how =How.XPATH, using ="//button[@id='login_dialog_confirmation_go_button']")
	public WebElement Get_Started;
	
	@FindBy(how =How.XPATH, using ="//*[@id='gear_button_username']")
	public WebElement gear_button_username;
	@FindBy(how =How.XPATH, using ="//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;
	
	@FindBy(how =How.ID, using ="login_dialog_error_msg")
	public WebElement ErrorMessage;
	
	@FindBy(how =How.CLASS_NAME, using ="ui-progressbar-overlay")
	public WebElement Overlay;
	
public SmartWork5_NewRegisteration(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
@Step("Create a New User user through Smartwork5 Page and Verify User is created successfully,  Method: {method} ")
public void Login_Smartwork5() throws InterruptedException{
	Sign_in_or_Register.click();
	Thread.sleep(3000);
	List<WebElement> oRadiobuttons = driver.findElements(By.name("register_login_choice"));
	boolean bvalue=false;
	bvalue = oRadiobuttons.get(0).isSelected();
	if(bvalue==true){
		oRadiobuttons.get(0).click();
	}else {
		oRadiobuttons.get(1).click();
	}
	
	
	SignInButton.click();
	Thread.sleep(3000);
	String FirstName ="john";
	String LastName ="mercey";
	FirstName_LastName.sendKeys(FirstName+" "+LastName);
	String EmailID =FirstName+"_"+LastName+ GetRandomId.randomAlphaNumeric(3).toLowerCase()+"@mailinator.com";
	StudentEmail.sendKeys(EmailID);
	Password.sendKeys("Tabletop");
	Password2.sendKeys("Tabletop");
	oKbutton.click();
	register_purchase_choice_trial.click();
	Sign_Up_for_Trial_AccessButton.click();
	Thread.sleep(2000);
	OK_SignUpforTrialAccess_button.click();
	StudentEmail.sendKeys(EmailID);
	Confirm_Button.click();
	
	wait = new WebDriverWait(driver,8);
	TimeUnit.SECONDS.sleep(8);
	wait.until(ExpectedConditions.visibilityOf(Checkbox));
	Checkbox.click();
	Select drop = new Select(school_dropdown);
	drop.selectByValue("HIGH_SCHOOL");
	
	Select country = new Select(Select_Country);
	country.selectByValue("USA");
	
	Select state = new Select(Select_State);
	state.selectByValue("AZ");
	Thread.sleep(5000);
	School_Institution.click();
	School_Institution.sendKeys("Agua Fria High School");
	
	Thread.sleep(2000);
	Continue_Button.click();
	Thread.sleep(3000);
	Boolean isPresent = driver.findElements(By.id("login_dialog_confirmation_go_button")).size() > 0;
	if(isPresent==true){
	wait = new WebDriverWait(driver,8);
	TimeUnit.SECONDS.sleep(8);
	wait.until(ExpectedConditions.visibilityOf(Get_Started));
	Get_Started.click();
	} else { 
		wait = new WebDriverWait(driver,5000L);
		wait.until(ExpectedConditions.invisibilityOf(Overlay));
		wait.until(ExpectedConditions.visibilityOf(ErrorMessage));
		System.out.println(ErrorMessage.getText());
		return ;
	}
	wait = new WebDriverWait(driver,8);
	TimeUnit.SECONDS.sleep(8);
	wait.until(ExpectedConditions.visibilityOf(username));
	String actualUserName = EmailID;
    Thread.sleep(5000);
	String expectedUserName =username.getText();
	log.info("The SW5 User Name is " + expectedUserName);
    Assert.assertEquals(expectedUserName, actualUserName);
    gear_button_username.click();
    SignOut_link.click();
}


}
