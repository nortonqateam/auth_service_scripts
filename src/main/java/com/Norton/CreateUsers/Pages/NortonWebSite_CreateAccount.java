package com.Norton.CreateUsers.Pages;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.GetRandomId;

public class NortonWebSite_CreateAccount {
	static final Logger log = Logger.getLogger(NortonWebSite_CreateAccount.class);
	WebDriver driver;
	// Log variable
	WebDriverWait wait;
	@FindBy(how = How.ID, using = "profileLogin")
	public 	WebElement Norton_Profile_LogIN; 
	
	@FindBy(how = How.ID, using = "creatAccountEmail")
	public 	WebElement Norton_creatAccountEmail; 
	
	@FindBy(how = How.ID, using = "CreateAccountSubmitButton")
	public 	WebElement Norton_CreateAccountSubmitButton; 
	
	@FindBy(how = How.ID, using = "accountFirstName")
	public 	WebElement Norton_accountFirstName; 
	
	@FindBy(how = How.ID, using = "accountLastName")
	public 	WebElement Norton_accountLastName; 
	
	@FindBy(how = How.ID, using = "accountEmail")
	public 	WebElement Norton_accountEmail;
	
	@FindBy(how = How.ID, using = "accountReTypeEmail")
	public 	WebElement Norton_accountReTypeEmail; 
	
	@FindBy(how = How.ID, using = "accountPassword")
	public 	WebElement Norton_accountPassword; 
		
	@FindBy(how = How.ID, using = "LoginSubmitButton")
	public 	WebElement Norton_LoginSubmitButton; 
		
	@FindBy(how = How.XPATH, using = "//span[@class='headerText emailHeaderText']")
	public 	WebElement Norton_HeaderText; 
	
	@FindBy(how = How.XPATH, using = "//p[@class='notice']")
	public 	WebElement Norton_Notice; 
	
	@FindBy(how = How.XPATH, using = "//div[@id='buttonContainer']/div/button")
	public 	WebElement Norton_ContinueButton; 
	
	@FindBy(how = How.XPATH, using="//div[@class='panel-title']/a/div/div[contains(text(),'Log Out')]")
	public 	WebElement Norton_Logout; 
	
	@FindBy(how = How.XPATH, using="//div[@class='banner-close']/span")
	public WebElement BannerClose; 

	
public NortonWebSite_CreateAccount(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
@Step("Create a New User user through NortonWebsite Page and Verify User is created successfully,  Method: {method} ")
public void Login_NortonWebSite() throws InterruptedException{
	
	wait = new WebDriverWait(driver,8);
	TimeUnit.SECONDS.sleep(8);
	wait.until(ExpectedConditions.visibilityOf(Norton_Profile_LogIN));	
	BannerClose.click();
	String FirstName="John";
	String LastName="Milton";
	Norton_Profile_LogIN.click();
	Thread.sleep(2000);
	String EmailID =FirstName+"_"+LastName+ GetRandomId.randomAlphaNumeric(3).toLowerCase()+"@mailinator.com";
	Norton_creatAccountEmail.sendKeys(EmailID);
	Thread.sleep(2000);
	Norton_CreateAccountSubmitButton.click();
	Thread.sleep(2000);
	Norton_accountFirstName.sendKeys(FirstName);
	Norton_accountLastName.sendKeys(LastName);
	
	Thread.sleep(5000);
	
	Norton_accountReTypeEmail.click();
	Norton_accountReTypeEmail.sendKeys(EmailID);
	log.info("The User Email ID is " + EmailID );
	Norton_accountPassword.sendKeys("Today123");
	Norton_LoginSubmitButton.click();
	Thread.sleep(5000);
	
	//String HeaderText=Norton_HeaderText.getText();
	
	/*String notice=Norton_Notice.getText();
	
	if(isNullOrEmpty(HeaderText) && isNullOrEmpty(notice) ){
		System.out.println("HeaderText and notice are not displayed " +"\n" +HeaderText+"\n" +notice );
	}*/
	Norton_ContinueButton.click();
	Thread.sleep(2000);
	Norton_Logout.click();
} 

public static boolean isNullOrEmpty(String str) {
    if(str != null && !str.trim().isEmpty())
        return true;
    return false;
}
}
