package com.Norton.Pcat.Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;
import Utilities.ReadJsonFile;

import com.google.gson.JsonObject;

public class Pcat_Page {
	
	WebDriver driver;
	public static String Inst_Email;
	public static String Inst_Password;
	WebDriverWait wait;
	
	@FindBy(how = How.ID, using = "register_login_choice_login")
	public 	WebElement SignIn_Pcat; 
	
	@FindBy(how = How.ID, using = "username")
	public 	WebElement username_Pcat; 
	
	@FindBy(how = How.ID, using = "password")
	public 	WebElement password_Pcat; 
	
	@FindBy(how =How.XPATH, using = "//button[@id='login_dialog_button']")
	public
	WebElement SignInButton_Pcat;
	
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']")
	public WebElement gearButton_Username;
	
	@FindBy(how = How.XPATH, using = "//ul[@id='gear_menu']/li/a[contains(text(),'Authorize an Instructor Account')]")
	public WebElement Authorize_Instructor_Account;
	
		
	@FindBy(how = How.ID, using ="authorize_instructor_first_name")
	public WebElement Authorize_Instructor_FirstName;
	
	@FindBy(how = How.ID, using ="authorize_instructor_last_name")
	public WebElement Authorize_Instructor_LastName;
	
	@FindBy(how = How.ID, using ="authorize_instructor_email")
	public WebElement Authorize_Instructor_Email;
	
	@FindBy(how =How.XPATH, using ="//button[@type='button']/span[contains(text(),'Authorize Instructor')]")
	public WebElement Authorize_Instructor_Button;
	
	@FindBy(how =How.XPATH, using="//div[@class='ui-dialog-content ui-widget-content']/b[1]")
	public  WebElement Instructor_Email;
	
	@FindBy(how =How.XPATH, using="//div[@class='ui-dialog-content ui-widget-content']/b[2]")
	public  WebElement Instructor_Password;
	
	@FindBy(how =How.XPATH, using="//div[@class='ui-dialog-content ui-widget-content']/br[1]")
	public WebElement Instructor_authorization_text;
	
	@FindBy(how =How.XPATH, using="//button[@type='button']/span[contains(text(),'Done')]")
	public WebElement Instructor_Done_Button;
	
	@FindBy(how =How.XPATH, using ="//li[@role='presentation']/a[@role='menuitem']/b[contains(text(),'Sign Out')]")
	public WebElement PcatSignOut_link;
	
public Pcat_Page(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}

public Pcat_Page(String InstEmail, String InstPassword){
	Pcat_Page.Inst_Email= InstEmail;
	Pcat_Page.Inst_Password =InstPassword;
}
ReadJsonFile readJasonObject = new ReadJsonFile();
JsonObject jsonobject = readJasonObject.readJason();

//Allure Steps Annotations and Methods
	@Step("Login to PCAT,  Method: {method} ")
	public void loginto_Pcat() throws Exception {
        wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		
		//wait.until(ExpectedConditions.visibilityOf(SignIn_Pcat));
		Actions PcatIn = new Actions(driver);
		PcatIn.moveToElement(SignIn_Pcat);
		PcatIn.click();
		PcatIn.perform();		
		String UserName=jsonobject.getAsJsonObject("PCAT_LOGIN").get("Pcatloginemail").getAsString();
		username_Pcat.sendKeys(UserName);
		password_Pcat.sendKeys(jsonobject.getAsJsonObject("PCAT_LOGIN").get("Pcatloginpassword").getAsString());
		SignInButton_Pcat.click();
		Thread.sleep(5000);
		gearButton_Username.click();
				
	}
	@Step("Click the Authorize an Instructor Account link,  Method: {method} ")
	public void authorize_Instructor_Account_link() throws InterruptedException {
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Authorize_Instructor_Account));
		Authorize_Instructor_Account.click();
	}

	@Step("Enter an Authorize Instructor details,  Method: {method} ")
	public void authorize_Instructor_Account() throws Exception {
		String Auth_Inst_FirstName = jsonobject.getAsJsonObject("Authorize_Instructor").get("Authorize_Instructor_FirstName").getAsString();
		Authorize_Instructor_FirstName.sendKeys(Auth_Inst_FirstName);
		String Auth_Inst_LastName = jsonobject.getAsJsonObject("Authorize_Instructor").get("Authorize_Instructor_LastName").getAsString();
		Authorize_Instructor_LastName.sendKeys(Auth_Inst_LastName);
		//String Auth_Inst_Email = jsonobject.getAsJsonObject("Authorize_Instructor").get("Authorize_Instructor_Email").getAsString();
		Authorize_Instructor_Email.sendKeys(Auth_Inst_FirstName+"_"+Auth_Inst_LastName+ (int)(Math.random() * (999 -100)+1)+"@hbs.com");
		Authorize_Instructor_Button.click();
	}

	@Step("get the Authorize instructor details,  Method: {method} ")
	public  Pcat_Page authorize_Instructor_getText() throws Exception {
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Instructor_Email));
		Inst_Email=Instructor_Email.getText();
		System.out.println(Inst_Email);
		Inst_Password=Instructor_Password.getText();
		System.out.println(Inst_Password);
		return new Pcat_Page (Inst_Email,Inst_Password);
		
	}
	@Step("Signout PCAT,  Method: {method} ")
	public void SignOut_Pcat() throws Exception {
		Instructor_Done_Button.click();
		gearButton_Username.click();
		PcatSignOut_link.click();
	}

}



