package Utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GetDate {
	 
	static Date currentDate;
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	 
	/* public static void main(String[] args){
		 GetDate f = new GetDate();
		 System.out.println(f.getCurrentDate());
		 System.out.println(f.getNextmonthDate());
	 }*/

	 public static String getCurrentDate(){

	        currentDate = new Date();
	        String SystemDate =(dateFormat.format(currentDate));
	        return SystemDate;
	 }
	        
	 public static String getNextmonthDate(){  
	        // convert date to calendar
	        Calendar c = Calendar.getInstance();
	        c.setTime(currentDate);

	        // manipulate date
	       // c.add(Calendar.YEAR, 1);
	        c.add(Calendar.MONTH, 1);
	        c.add(Calendar.DATE, 1); //same with c.add(Calendar.DAY_OF_MONTH, 1);
	       
	        // convert calendar to date
	        Date currentDatePlusOne = c.getTime();

	        String nextmonthdate=(dateFormat.format(currentDatePlusOne));
			return nextmonthdate;
			

	    }

	 
	 public static String addDays(int numberOfDays) {
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, numberOfDays);

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");		
			String date =  dateFormat.format(cal.getTime());
			
			return date;
			
			}
	



/*	public String getCurrentDate(){
		String currentDate = dateformat.format(date);
		System.out.println(currentDate);
		return currentDate;
				
	}*/

}
