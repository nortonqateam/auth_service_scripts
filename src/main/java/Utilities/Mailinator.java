package Utilities;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import ru.yandex.qatools.allure.annotations.Step;
import org.openqa.selenium.JavascriptExecutor;

public class Mailinator {

	WebDriver driver;

	// Initializing Web Driver and PageFactory.
	public Mailinator(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@Step("Search the Email in the Mailinator Website,  Method: {method} ")
	public String SearchEmail(String Email) {
		 ((JavascriptExecutor)driver).executeScript("window.open()");
		    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(1));
		   
		driver.navigate().to("https://www.mailinator.com/");
		WebElement searchEmail = driver.findElement(By
				.xpath("//div[@class='input-group']/input[@type='text']"));
		searchEmail.sendKeys(Email);
		WebElement clickGoButton = driver
				.findElement(By
						.xpath("//div[@class='input-group-append']/button[@type='button']"));
		clickGoButton.click();
		return Email;

	}

	@Step("Click reset Password link on  Mailinator Website,  Method: {method} ")
	public void clickResetPasswordEmailLink() {
		WebElement resetPasswordEmailLink = driver
				.findElement(By
						.xpath("//table[@class='table table-striped jambo_table']/tbody/tr/td/a[contains(text(),'Reset Password for W. W. Norton Account')]"));
		resetPasswordEmailLink.click();
	}

	@Step("Click reset Password link on  from the User Email,  Method: {method} ")
	public void clickResetPasswordLinkUserEmail() throws InterruptedException {
        Thread.sleep(4000);
        driver.switchTo().frame("msg_body");
		WebElement resetPasswordEmailLink = driver
				.findElement(By.partialLinkText("wwnorton.com"));
		resetPasswordEmailLink.click();
		
	}

}
