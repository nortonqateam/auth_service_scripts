package Utilities;

import com.github.javafaker.Faker;

public class FakerNames {
	static Faker faker = new Faker();
	String name = faker.name().fullName();
	
	public static String  getStudentFName(){
	String firstName = faker.name().firstName();
	return firstName;
	}
	
	public static String getStudentLName(){
	String lastName = faker.name().lastName();
	return lastName;
	}
}
