package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReusableMethods {

	public static void checkPageIsReady(WebDriver driver) {

		boolean isPageReady = false;
		JavascriptExecutor js = (JavascriptExecutor) driver;

		while (!isPageReady) {

			isPageReady = js.executeScript("return document.readyState")
					.toString().equals("complete");

			try {

				Thread.sleep(1000);

			} catch (InterruptedException e) {

				e.printStackTrace();

			}
		}
	}

	public static boolean elementExist(WebDriver driver, String xpath) {

		try {

			driver.findElement(By.xpath(xpath));

		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}

	public static void scrollToBottom(WebDriver driver) {
		try {
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollToElement(WebDriver driver, By by) {
		try {
			WebElement element = driver.findElement(by);
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollIntoView(WebDriver driver, WebElement webElement) {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByXY(WebDriver driver, int xOffset, int yOffset) {
		try {
			String script = "scroll(" + xOffset + ", " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByX(WebDriver driver, int xOffset) {
		try {
			String script = "scroll(" + xOffset + ", 0)";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByY(WebDriver driver, int yOffset) {
		try {
			String script = "scroll(0, " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void waitForPageLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(pageLoadCondition);
	}

	public static void questionCloseLink(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.className("close")));
		driver.findElement(By.className("close")).click();
	}

	public static void waitVisibilityOfElement(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public static void waitElementClickable(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static void loadingWaitDisapper(WebDriver driver, WebElement loader) {
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.visibilityOf(loader)); // wait for loader
																// to appear
		wait.until(ExpectedConditions.invisibilityOf(loader)); // wait for
																// loader to
																// disappear
	}

	// Download the File in download Folder
	public static void downLoadFile() {
		String fileDownloadPath = System.getProperty("user.dir")
				+ "\\Downloads";

		Map<String, Object> prefsMap = new HashMap<String, Object>();
		prefsMap.put("profile.default_content_settings.popups", 0);
		prefsMap.put("download.default_directory", fileDownloadPath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefsMap);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		// driver = new ChromeDriver(options);
	}

	public static void processZipFile(String zipFilePath, String destDir) {
		try {
			File directoryToScan = new File(zipFilePath);
			if (directoryToScan.isDirectory()) {
				for (File file : directoryToScan.listFiles()) {
					unzipFile(file, destDir);
				}
			} else {
				unzipFile(directoryToScan, destDir);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void unzipFile(File zipFilePath, String destDir) {
		File dir = new File(destDir);
		// create output directory if it doesn't exist
		if (!dir.exists())
			dir.mkdirs();
		FileInputStream fis;
		// buffer for read and write data to file
		byte[] buffer = new byte[1024];
		try {
			System.out.println("Unzipping file : "
					+ zipFilePath.getAbsolutePath());
			fis = new FileInputStream(zipFilePath);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				File newFile = new File(destDir + File.separator + fileName);
				System.out.println("Unzipping to " + newFile.getAbsolutePath());
				// create directories for sub directories in zip
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				// close this ZipEntry
				zis.closeEntry();
				ze = zis.getNextEntry();
			}
			// close last ZipEntry
			zis.closeEntry();
			zis.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void deleteFile(String zipFilePath){
		File dir = new File(zipFilePath);
		if(dir.isDirectory() == false) {
			return;
		}
		File[] listFiles = dir.listFiles();
		for(File file : listFiles){
			System.out.println("Deleting "+file.getName());
			file.delete();
		}
	}

	
	public static void clickElement(WebDriver driver, WebElement Element){
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", Element);
	}
	
	public static void switchToNewWindow(int windowNumber, WebDriver driver) {
	    Set < String > s = driver.getWindowHandles();   
	    Iterator < String > ite = s.iterator();
	    int i = 1;
	    while (ite.hasNext() && i < 10) {
	        String popupHandle = ite.next().toString();
	        driver.switchTo().window(popupHandle);
	        System.out.println("Window title is : "+driver.getTitle());
	        if (i == windowNumber) 
	        break;
	        i++;
	    }
	}
}