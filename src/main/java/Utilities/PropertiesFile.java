package Utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;



public class PropertiesFile {
	
	public static WebDriver driver = null;
    public static WebDriverWait wait;
	
    public static String Browser;
	public static String QAurl;
	public static String pcatUrl;
	public static String promotionalurl;
	public static String directurl;
	public static String DriverPath;
	public static String ExpiredCodeErrorMessage;
	public static String UserDir=System.getProperty("user.dir");
	public static String NortonWebSiteurl;
	public static String NortonDashboard;
	public static String NortonPreviewDashboardurl;
	public static String NortonKbetaurl;
	public static String ContenttableURL;
	public static String IIGUrl;
	public static String LMSURL;
	public static String ProductionURL;
	public static String SW5Stg2URL;
	public static String testMakerurl;
	public static String nltUrl;
	public static String bookmarkurl;
	
    public WebDriver getDriver() {
        return driver;
    }
	
    
    // Read Browser name, Test url and Driver file path from config.properties.
    
	public static void readPropertiesFile() throws Exception {
	
		
		Properties prop = new Properties();
		
		try {
			
			InputStream input = new FileInputStream(UserDir +"\\src\\test\\resources\\config.properties");
			prop.load(input);
			
			Browser = prop.getProperty("browsername");
			QAurl = prop.getProperty("testurl");
			pcatUrl=prop.getProperty("pcatURL");
			ExpiredCodeErrorMessage =prop.getProperty("ExpiredCodeErrorMessage");
			//DriverPath = prop.getProperty("driverfile");
			DriverPath =UserDir +"\\Drivers\\";
			NortonWebSiteurl =prop.getProperty("NortonWebSite");
			NortonDashboard=prop.getProperty("NortonWebSiteDashboard");
			NortonPreviewDashboardurl =prop.getProperty("NortonPreviewWebSiteDashboard");
			
			NortonKbetaurl=prop.getProperty("Nortonkbeturl");
			ContenttableURL =prop.getProperty("ContentTableURL");
			IIGUrl =prop.getProperty("IIGUrl");
			LMSURL =prop.getProperty("LMSURL");
			ProductionURL = prop.getProperty("Productionurl");
			SW5Stg2URL = prop.getProperty("SW5stg2URL");
			testMakerurl = prop.getProperty("TestMakerURL");
			nltUrl = prop.getProperty("Nlttesturl");
			bookmarkurl = prop.getProperty("bookmarktesturl");
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
	}	

	
	// Set Browser configurations by comparing Browser name and Diver file path.
	
	public static void setBrowserConfig() {
		
			if(Browser.contains("Firefox")) {
				System.setProperty("webdriver.firefox.marionette", DriverPath + "/geckodriver.exe");
            
				FirefoxOptions firefoxOptions = new FirefoxOptions();
			    firefoxOptions.setHeadless(false);
				firefoxOptions.setCapability("marionette", true);
            
				driver = new FirefoxDriver(firefoxOptions); 
            
			}
			
			if(Browser.contains("Chrome")) {
				System.setProperty("webdriver.chrome.driver", DriverPath +"/chromedriver.exe");
				
				ChromeOptions chromeOptions = new ChromeOptions();
				//chromeOptions.setHeadless(true);
				//chromeOptions.addArguments("--test-type");
				chromeOptions.addArguments("--start-maximized");
				chromeOptions.addArguments("--disable-infobars");
				chromeOptions.addArguments("--disable-extensions");
				chromeOptions.addArguments("--disable-gpu");
				chromeOptions.addArguments("--no-default-browser-check");
				chromeOptions.addArguments("--ignore-certificate-errors");
				chromeOptions.addArguments("--proxy-server='direct://'");
				chromeOptions.addArguments("--proxy-bypass-list=*");
				//chromeOptions.setExperimentalOption("useAutomationExtension", false);*/
				/*DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
				desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);*/
				driver = new ChromeDriver(chromeOptions);
                driver.manage().deleteAllCookies();

           }
			
		}
		

	// Set Test URL based on config.properties file.
	
	public static void setURL(String urlName) {
		//driver.manage().window().maximize();
		driver.get(QAurl+urlName);
		
		
		}
	
	public static void setPcatURL() {
		//driver.manage().window().maximize();
		driver.get(pcatUrl);
			
		}
	public static void setSW5URL() {
		//driver.manage().window().maximize();
		driver.get(SW5Stg2URL);
			
		}
	public static void setTMURL() {
		
		driver.manage().window().maximize();
		driver.get(testMakerurl);
	
	}
	
public static void setBookMarkURL() {
		
		driver.manage().window().maximize();
		driver.get(bookmarkurl);
	
	}

	public static void setNLTURL(String urlName) {
	
	driver.manage().window().maximize();
	driver.get(nltUrl+urlName);

}
		
	// Close the driver after running test script.
	@Step("Closing the application,  Method: {method} ")
	public static void tearDownTest() {
		
			wait = new WebDriverWait(driver,3);
			driver.close();
			driver.quit();
					
		}


	public static void NortonWebSiteurl() {
		// TODO Auto-generated method stub
		driver.get(NortonWebSiteurl);
	}
	public static void Nortonbetaurl() {
		// TODO Auto-generated method stub
		driver.get(NortonKbetaurl);
	}
	
	public static void NortonPreviewDashboardurl() {
		// TODO Auto-generated method stub
		driver.get(NortonPreviewDashboardurl);
	}
	
	public static void NortonDashboardurl() {
		// TODO Auto-generated method stub
		driver.get(NortonDashboard);
	}
	
	public static void getURL(String url) {
		// TODO Auto-generated method stub
			
			driver.get(url);
		}
		
	

}
