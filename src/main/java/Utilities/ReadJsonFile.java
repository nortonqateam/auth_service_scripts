package Utilities;

import java.io.FileReader;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class ReadJsonFile {

	// Read Json test data from testData.json file with the parameters and
	// values.
	public static String UserDir = System.getProperty("user.dir");
	
	public JsonObject readJason() {

		JsonObject jsonobject = null;

		String userdir = System.getProperty("user.dir")
				+ "\\src\\test\\resources\\PRODtestdata16NOV.json";
		try {
			JsonParser jsonParser = new JsonParser();
			JsonReader reader = new JsonReader(new FileReader(userdir));
			reader.setLenient(true);
			JsonElement rootElement = jsonParser.parse(reader);
			jsonobject = rootElement.getAsJsonObject();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject;

	}

	public JsonObject readJson() {

		JsonObject rootObject = null;

		try {
			String varEnv = ReadApplicationProperties.getEenvironmentValue();

			switch (varEnv) {

			case "QA":
				JsonParser parser1 = new JsonParser();
				JsonReader jReader1 = new JsonReader(new FileReader(UserDir
						+ "\\src\\test\\resources\\testData_QA.json"));
				jReader1.setLenient(true);

				JsonElement rootElement1 = parser1.parse(jReader1);
				rootObject = rootElement1.getAsJsonObject();
				break;

			case "Prod":
				JsonParser parser2 = new JsonParser();
				JsonReader jReader2 = new JsonReader(new FileReader(UserDir
						+ "\\src\\test\\resources\\testData_PROD.json"));
				jReader2.setLenient(true);

				JsonElement rootElement2 = parser2.parse(jReader2);
				rootObject = rootElement2.getAsJsonObject();
				break;

			case "Stg":
				JsonParser parser3 = new JsonParser();
				JsonReader jReader3 = new JsonReader(new FileReader(UserDir
						+ "\\src\\test\\resources\\testData_Stg.json"));
				jReader3.setLenient(true);

				JsonElement rootElement3 = parser3.parse(jReader3);
				rootObject = rootElement3.getAsJsonObject();
				break;

			case "UAT":
				JsonParser parser4 = new JsonParser();
				JsonReader jReader4 = new JsonReader(new FileReader(UserDir
						+ "\\src\\test\\resources\\testData_UAT.json"));
				jReader4.setLenient(true);

				JsonElement rootElement4 = parser4.parse(jReader4);
				rootObject = rootElement4.getAsJsonObject();
				break;

			}

		} catch (Exception e)

		{
			e.printStackTrace();
		}

		return rootObject;

	}
}
