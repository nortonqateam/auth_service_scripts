package Utilities;

import java.util.Set;

import org.openqa.selenium.WebDriver;

public class GetChildWindow {
	
	static WebDriver driver;
	public static String ChildWindow = null;
	static String ParentWindow;
	
	public static  void NavigatetoChildWindow(){
	Set<String> Windowhandles= driver.getWindowHandles();
    ParentWindow = driver.getWindowHandle();
    Windowhandles.remove(ParentWindow);
    String winHandle=Windowhandles.iterator().next();
    if (winHandle!=ParentWindow){
    	ChildWindow=winHandle;
    }
    driver.switchTo().window(ChildWindow);
	}
	
	public  void CloseChildWindow(){
		driver.switchTo().window(ChildWindow).close();
	}
	
	public  void SwitchParentWindow(){
		driver.switchTo().window(ParentWindow);
	}
}
