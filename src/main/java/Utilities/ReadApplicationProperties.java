package Utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadApplicationProperties {

    public static String UserDir=System.getProperty("user.dir");
    public static String Environment;

    @SuppressWarnings("unused")
	public static String getEenvironmentValue() throws IOException {
    	
    	Properties prop = new Properties();
    	String appFileName = UserDir + "\\src\\test\\resources\\application.properties";
    	InputStream ipStream = new FileInputStream(appFileName);
    	
    	if(ipStream != null) {
    		
    		prop.load(ipStream);
    		Environment = prop.getProperty("environment");
    	
    	} else {
    		
    		throw new FileNotFoundException ("Application Properties file " + appFileName + "not found in the classpath");
    		
    	}
    	
    	return Environment;
    	
    }
    

}
