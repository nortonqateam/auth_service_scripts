package com.Norton.TestMaker.Test;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.TestMaker.Pages.TestMakerLogOutPage;
import com.Norton.TestMaker.Pages.TestMakerLoginPage;

import Utilities.PropertiesFile;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ Utilities.TestListener.class })
public class LoginTestMaker extends PropertiesFile {

	TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;

	@Parameters("Browser")
	@BeforeTest
	public void setUp() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setTMURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Login to TestMaker application")
	@Stories("Login to Test Maker application as an Instructor and Verify Instructor is logged Successfully and Logout the application")
	@Test()
	public void loginInstructor() throws Exception {
		TMlogin = new TestMakerLoginPage(driver);
		TMlogin.loginTestMakerApp();
		Assert.assertNotNull(TMlogin.profileTestMakerApp());
		Assert.assertNotNull(TMlogin.getbookinformation(),
				"Book information is displayed");

	}

	@AfterTest
	public void closeTest() throws Exception {
		TMlogOut = new TestMakerLogOutPage(driver);
		TMlogOut.logOutTestMakerApp();
		PropertiesFile.tearDownTest();

	}
}