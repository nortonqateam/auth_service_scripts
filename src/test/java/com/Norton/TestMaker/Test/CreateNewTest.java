package com.Norton.TestMaker.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.TestMaker.Pages.CreateNewTestPage;
import com.Norton.TestMaker.Pages.RegionYourTestsPage;
import com.Norton.TestMaker.Pages.TestMakerLogOutPage;
import com.Norton.TestMaker.Pages.TestMakerLoginPage;

import Utilities.PropertiesFile;
import Utilities.TestListener;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


@Listeners({ TestListener.class})

public class CreateNewTest extends PropertiesFile {

	com.Norton.TestMaker.Pages.TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage TMcreateNewTest;
	RegionYourTestsPage TMYourTests;

	@Parameters("Browser")
	@BeforeTest
	public void setUp() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setTMURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Create a New Test from Test Maker Application and Validate the Created test ")
	@Stories("Login to Test Maker application as an Instructor and Create a New Test ")
	@Test()
	public void createNewTest() throws Exception {
		TMlogin = new TestMakerLoginPage(driver);
		TMlogin.loginTestMakerApp();
		TMcreateNewTest = new CreateNewTestPage(driver);
		TMcreateNewTest.clickCreateNewTest();
		String testName = TMcreateNewTest.createNewTestwithTestNameCourseName();
		String buildTestName = TMcreateNewTest.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		TMcreateNewTest.clickSave();
		TMcreateNewTest.clickTestMakerBackButton();
		Thread.sleep(8000);
		TMYourTests = new RegionYourTestsPage(driver);
		TMYourTests.clickTestNamelink(buildTestName);
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='bg-white m1-5 xs-col-4 sm-col-4 md-col-8 lg-col-10 lg-col-12']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']")));
		Assert.assertEquals(TMcreateNewTest.getTestName(), testName);

	}

	@AfterTest
	public void closeTest() throws Exception {
		TMlogOut = new TestMakerLogOutPage(driver);
		TMlogOut.logOutTestMakerApp();
		PropertiesFile.tearDownTest();

	}
}
