package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.DemoEbookPage;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class TC51Ebook_Demo_Access_Test extends PropertiesFile {

	NciaSignInPage Sign_Page;
	DemoEbookPage demolink;
	Register_Purchase_Page purchase_page;
	DemoEbookPage demoEbookpage;
	WebDriverWait wait;
	String EmailID;
	String UserNameText;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("psychsci6");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-51 A user navigates through Ebook with DEMO access, gets oneself sign in with trial access, then buys free product successfully from the application.")
	@Stories("Login in for Existing User and A user navigates through Ebook with DEMO access, gets oneself sign in with trial access, then buys free product successfully from the application")
	@Test
	public void UserClicks_Ebook_Demo_Link() throws Exception {
		demolink = new DemoEbookPage(driver);
		Sign_Page = new NciaSignInPage(driver);
		purchase_page = PageFactory.initElements(driver,
				Register_Purchase_Page.class);;
		demoEbookpage = new DemoEbookPage(driver);
		
			demolink.clickDemoLink();
			demolink.DemoPage();
			demolink.SignIn();
					
			String StudentEmailID =purchase_page.createNewUser();
			LogUtil.log(StudentEmailID);
			wait = new WebDriverWait(driver, 10);
			TimeUnit.SECONDS.sleep(10);
			wait.until(ExpectedConditions
					.visibilityOf(Sign_Page.gear_button_username));
			UserNameText = Sign_Page.gear_button_username.getText().trim();
			Assert.assertEquals(UserNameText.toLowerCase(),
					StudentEmailID.toLowerCase());
			Thread.sleep(5000);
			Sign_Page.clickPurchaseOption();
			Sign_Page.checkPurchaseCheckbox("FREE");
				
			Sign_Page.clickGetFreeItemsButton();
			Sign_Page.clickShowPurchaseOptionButton();
			Thread.sleep(5000);
			purchase_page.purchaseOptiondetails(StudentEmailID);
			Sign_Page.clickGetFreeItemsButton();
			purchase_page.getStarted_popup();
			Thread.sleep(2000);
			Sign_Page.clickPurchaseOption();
			purchase_page.VerifyPurchaseData();
			Thread.sleep(5000);
			Sign_Page.gear_button_username.click();
			Sign_Page.SignOut_link.click();
	

	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}

}
