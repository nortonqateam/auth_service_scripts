package com.Norton.NCIA.Test;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Utilities.PropertiesFile;
import Utilities.TestListener;

import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ TestListener.class })
public class TC53Expired_regi_code_unable_login_Test extends PropertiesFile {
	NciaSignInPage Sign_Page;
	Register_Purchase_Page purchase_Page;

	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("psychsci6");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-53 Verify that user having expired registration code is unable to log in to SW5 application.")
	@Stories("Verify that user having expired registration code is unable to log in to SW5 application")
	@Test
	public void Expired_Registration_Code() throws Exception {

		Sign_Page = new NciaSignInPage(driver);
		purchase_Page = PageFactory.initElements(driver,
				Register_Purchase_Page.class);
		Sign_Page.clickSignin_Register();
		Sign_Page.need_to_Register();
		String emailID =purchase_Page.register_Access_InValidCode();
		purchase_Page.confirm_Registration(emailID);
		purchase_Page.term_privacypolicy_popup();

	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}
}
