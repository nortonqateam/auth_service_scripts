package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class TC81DLP_registration_code_redemption_option_Test extends
		PropertiesFile {

	NciaSignInPage Sign_Page;
	Register_Purchase_Page RegisterAccessPage;
	String StudentEmailID;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	WebDriverWait wait;

	@Parameters("Browser")
	@BeforeTest
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("chem5");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC 81-Verify that a product which allows student to redeem a code, purchase option is not available for student..")
	@Stories("Verify that a product which allows student to redeem a code, purchase option is not available for student")
	@Test
	public void LoginSmartwork5() throws Exception {
		Sign_Page = new NciaSignInPage(driver);
		Sign_Page.clickSignin_Register();
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//label[@for='register_login_choice_register']/span/span")));
		// Sign_Page.Need_to_register.click();
		Sign_Page.SignInButton.click();
		RegisterAccessPage = new Register_Purchase_Page(driver);
		// RegisterAccessPage.oKbutton.click();
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions
				.visibilityOf(RegisterAccessPage.FirstName_LastName));
		String StudentEmailID =RegisterAccessPage.createUser();
		// registration_code.click();
		String RegCode = jsonobject
				.getAsJsonObject("StudentRedemptionoptiononly")
				.get("Registration_Code").getAsString();
		RegisterAccessPage.access_code.sendKeys(RegCode);

		RegisterAccessPage.register_My_code_Button.click();
		RegisterAccessPage.StudentEmail.sendKeys(StudentEmailID);
		RegisterAccessPage.Confirm_Button.click();
		Thread.sleep(5000);
		RegisterAccessPage.term_privacypolicy_popup();
		Thread.sleep(5000);
		RegisterAccessPage.getStarted_popup();
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.username));
		String expectedUserName = Sign_Page.username.getText().trim();
		Assert.assertEquals(expectedUserName.toLowerCase(), StudentEmailID
				.toLowerCase().trim());
		Sign_Page.username.click();
		Sign_Page.SignOut_link.click();
	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}
}
