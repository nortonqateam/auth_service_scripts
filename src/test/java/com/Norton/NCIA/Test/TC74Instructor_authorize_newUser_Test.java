package com.Norton.NCIA.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.Pcat.Pages.Pcat_Page;
import com.Norton.Smartwork5.Pages.Create_New_Student_Set_Page;
import com.Norton.Smartwork5.Pages.GearMenu_Page;
import com.Norton.Smartwork5.Pages.ManageStudentSetsPage;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.UpdateStudentSetPage;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;
@Listeners({TestListener.class})
public class TC74Instructor_authorize_newUser_Test extends PropertiesFile {
	String New_Instructor_Email;
	String StudentSetID;
	String EmailID;
	String InstEmailID;
	String New_Instructor_Password;
	NciaSignInPage signinPage;
	GearMenu_Page gearMenu_links;
	Pcat_Page Pcatpage;
	Create_New_Student_Set_Page studentSetPage;
	ManageStudentSetsPage ManageStudentPage;
	UpdateStudentSetPage updatestudentset;
	
	Create_New_Student_Set_Page createstudentSet;
	
	WebDriverWait wait;
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
	
		//PropertiesFile.setURL("chem4");
		PropertiesFile.setURL("englishlit10abc");
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-74 Verify that an instructor should able to authorize a new user")
	@Stories("Login in as Existing Instructor and Verify that an instructor should able to authorize a new user")
	@Step("Click on Sign in or Register,  Method: {method} ")
	@Test(priority=0)
	public void Login_Smartwork5() throws Exception{
		
		signinPage =new NciaSignInPage(driver);
		signinPage.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(signinPage.yes_SignIn);
		SignIn.click();
		SignIn.perform();
		
		String UserName=jsonobject.getAsJsonObject("InstructorLogin").get("Instloginemail").getAsString();
		signinPage.Username.sendKeys(UserName);
		signinPage.Password.sendKeys(jsonobject.getAsJsonObject("InstructorLogin").get("Instloginpassword").getAsString());
		signinPage.SignInButton.click();
	}
	
	@Step("Click on UserName link from Top and Click Authorize an Instructor Account link,  Method: {method} ")
	@Test(priority=1)
	public void Authorize_Instructor_Account () throws Exception{
		
		gearMenu_links= new GearMenu_Page(driver);
		Thread.sleep(2000);
		wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(signinPage.gear_button_username));
		signinPage.gear_button_username.click();
		gearMenu_links.AuthorizeInstructorAccount.click();
	}
	
	@Step("authorize new user as instructor,  Method: {method} ")
	@Test(priority=2)
	public void Authorize_New_User () throws Exception {
		studentSetPage =new Create_New_Student_Set_Page(driver);
		Pcatpage= new Pcat_Page(driver);
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(10);
		
		wait.until(ExpectedConditions.visibilityOf(Pcatpage.Authorize_Instructor_FirstName));
		try {
			Pcatpage.authorize_Instructor_Account();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		//wait.until(ExpectedConditions.invisibilityOf(studentSetPage.Authorize_overlay));
	  //  wait.until(ExpectedConditions.visibilityOf(Pcatpage.Instructor_Email));
		Pcatpage.authorize_Instructor_getText();
		New_Instructor_Email =Pcat_Page.Inst_Email ;
	//	System.out.println(New_Instructor_Email);
		New_Instructor_Password =Pcat_Page.Inst_Password;
	//	System.out.println(New_Instructor_Password);
		Pcatpage.Instructor_Done_Button.click();
	}
	
	@Step("Click Manage StudentSet link  Method: {method} ")
	@Test(priority=3)
	public void ManageStudentSet () throws Exception {
		Thread.sleep(5000);
		signinPage.gear_button_username.click();
		gearMenu_links.ManageStudetSets.click();
		Thread.sleep(5000);
	}
	
	@Step("Create a Student Set,  Method: {method} ")
	@Test(priority=4)
	public void Create_StudentSet () throws Exception{
		
		createstudentSet= new Create_New_Student_Set_Page(driver);
		createstudentSet.Create_New_Student_Set_Button.click();
		createstudentSet.createNewStudentset();
		createstudentSet.createStudentset_information();
		StudentSetID= createstudentSet.createStudentset_ID();
		System.out.println(StudentSetID);
		
	}
	
	
	@Step("Search and Add New Memeber to Student Set,  Method: {method} ")
	@Test(priority=5)
	public void Update_StudentSet() throws Exception{
		ManageStudentPage= new ManageStudentSetsPage(driver);
		updatestudentset = new UpdateStudentSetPage(driver);
		Thread.sleep(3000);
		ManageStudentPage.Search_Text.click();
		Thread.sleep(3000);
		ManageStudentPage.Search_Text.sendKeys(StudentSetID);
		ManageStudentPage.Updatebutton.click();
		Thread.sleep(3000);
		updatestudentset.updateStudent();
		Thread.sleep(3000);
		updatestudentset.EmailAddressTextbox.sendKeys(New_Instructor_Email);
		updatestudentset.LookupButton.click();
		Thread.sleep(3000);
		updatestudentset.AddButton.click();
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(updatestudentset.InstructorsTAslinktab));
		updatestudentset.InstructorsTAslinktab.click();
		EmailID =updatestudentset.AssignInstructorRole();
		
		System.out.println(EmailID);
		if(EmailID.equalsIgnoreCase(New_Instructor_Email)){
			Actions action = new Actions(driver);
			action.moveToElement(updatestudentset.InstructorsTAscheckbox).click().perform();
	
		}
		updatestudentset.SaveButton.click();
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(ManageStudentPage.Updatebutton));
		InstEmailID =updatestudentset.VerifyAdditionalInstructore();
		//System.out.println(InstEmailID);
		if(InstEmailID.equalsIgnoreCase(EmailID)){
			Assert.assertEquals("Verify for newly created student set newly authorized user is displayed as Additional Instructor.", InstEmailID, New_Instructor_Email);
		}
		ManageStudentPage.clickcloselink();
		
		
	}
	
	
	@Step("Signout the Instructor,  Method: {method} ")
	@Test(priority=6)
	public void SignOutInstructor() throws Exception{
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(signinPage.gear_button_username));
		signinPage.gear_button_username.click();
		signinPage.SignOut_link.click();
	}
	
	
	
	@Step("Signin with newly created the Instructor,  Method: {method} ")
	@Test(priority=7)
	public void Newly_SigninInstructor() throws Exception{
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(signinPage.Sign_in_or_Register));
		signinPage.Sign_in_or_Register.click();
		List<WebElement> oRadiobuttons = driver.findElements(By.xpath("//input[@type='radio']"));
		boolean bvalue=false;
		bvalue = oRadiobuttons.get(0).isSelected();
		if(bvalue==true){
			oRadiobuttons.get(0).click();
		}else {
			oRadiobuttons.get(1).click();
		}
		signinPage.Username.clear();
		signinPage.Username.sendKeys(New_Instructor_Email);
		signinPage.Password.sendKeys(New_Instructor_Password);
		signinPage.SignInButton.click();
		wait = new WebDriverWait(driver,5);
			TimeUnit.SECONDS.sleep(5);
			wait.until(ExpectedConditions.visibilityOf(signinPage.username));
			String expectedUserName =signinPage.username.getText();
			Assert.assertEquals(expectedUserName, New_Instructor_Email);
			signinPage.username.click();
			signinPage.SignOut_link.click();
		
	}
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
	

}
