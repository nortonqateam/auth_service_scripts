package com.Norton.NCIA.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;
import com.google.gson.JsonObject;

@Listeners({ TestListener.class })
public class TC57Expired_trail_access_reg_code_Test extends PropertiesFile {

	NciaSignInPage SignInPage;
	Activity_group_selectors selectTile;
	Register_Purchase_Page regCode;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	WebDriverWait wait;
	String Trial_access_message;
	String ExpectedMessage;
	String accesscode;

	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.getURL(QAurl + "psychsci6");
		// PropertiesFile.getURL(QAurl +"psychsci6");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-57 User having expired trail access, uses new reg. code and verifies that user is able to login to SW5 successfully")
	@Stories("User having expired trail access, uses new reg. code and verifies that user is able to login to SW5 successfully")
	@Test
	public void ExpiredTrialUser_RegCode() throws Exception {
		SignInPage = new NciaSignInPage(driver);
		selectTile = new Activity_group_selectors(driver);
		regCode = new Register_Purchase_Page(driver);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions
				.visibilityOf(SignInPage.Sign_in_or_Register));
		SignInPage.Sign_in_or_Register.click();
		String ExpiredUser = jsonobject
				.getAsJsonObject("ExpiredUser_psychsci6").get("UserLogin")
				.getAsString();
		SignInPage.Username.sendKeys(ExpiredUser);
		String Expired_Password = jsonobject
				.getAsJsonObject("ExpiredUser_psychsci6").get("UserPassword")
				.getAsString();
		SignInPage.Password.sendKeys(Expired_Password);
		SignInPage.SignInButton.click();
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions
				.visibilityOf(SignInPage.gear_button_username));
		selectTile.ZAPS.click();
		SignInPage.Assignmentlink.click();

		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		driver.switchTo().window(ChildWindow);
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions
				.visibilityOf(SignInPage.TrialErrorMessage));
		String url = driver.getCurrentUrl();
		System.out.println(url);
		Trial_access_message = SignInPage.TrialErrorMessage.getText();
		ExpectedMessage = "Your trial access period has expired. Please purchase access or redeem a registration code to continue accessing the product.";

		Assert.assertEquals(Trial_access_message, ExpectedMessage);

		SignInPage.registration_code_option.click();
		accesscode = jsonobject.getAsJsonObject("ExpiredUser_psychsci6")
				.get("RegCode").getAsString();
		SignInPage.AccessCode.sendKeys(accesscode);
		regCode.register_My_code_Button.click();
		regCode.term_privacypolicy_popup();
		regCode.getStarted_popup();
	}
}
