package com.Norton.NCIA.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({TestListener.class})
public class TC76LoginasIIGUser_Test extends PropertiesFile {
	
	Activity_group_selectors selectTile;
	NciaSignInPage signinPage;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		//PropertiesFile.setURL("chem4");
		PropertiesFile.getURL(IIGUrl);
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-76 Verify that existing IIG user is able to login successfully to the application after DB migration")
	@Stories("Verify that existing IIG user is able to login successfully")
	@Step("Login in as IIG user and Click the NEAL Archive tile ,  Method: {method} ")
	@Test(priority=0)
	public void ClickNEALArchiveTile() throws Exception{
		selectTile = new Activity_group_selectors(driver);
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(selectTile.NAELArchive_Tile));
		selectTile.NAELArchive_Tile.click();
	}
	
	@Step("Signin to SW5,  Method: {method} ")
	@Test(priority=1)
	public void Login_Smartwork5() throws Exception{
		Thread.sleep(5000);
		signinPage =new NciaSignInPage(driver);
		String ChildWindow = null;
	    Set<String> Windowhandles= driver.getWindowHandles();
	    String ParentWindow = driver.getWindowHandle();
	    Windowhandles.remove(ParentWindow);
	    String winHandle=Windowhandles.iterator().next();
	    if (winHandle!=ParentWindow){
	    	ChildWindow=winHandle;
	    }
	    driver.switchTo().window(ChildWindow);
		
		//signinPage.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(signinPage.yes_SignIn);
		SignIn.click();
		SignIn.perform();
		boolean usernametextbox = driver.findElement(By.id("username")).isEnabled();
		System.out.println(usernametextbox);
		signinPage.Username.click();
		String UserName=jsonobject.getAsJsonObject("IIGUser").get("IIGUserLogin").getAsString();
		signinPage.Username.sendKeys(UserName);
		signinPage.Password.sendKeys(jsonobject.getAsJsonObject("IIGUser").get("IIGUserPassword").getAsString());
		signinPage.SignInButton.click();
		
		Thread.sleep(12000);
		String url = driver.getCurrentUrl();
		System.out.println(url);
		String title = driver.getTitle();
		System.out.println(title);
		String Text =selectTile.BookCover.getText();
		System.out.println(Text);
		driver.switchTo().window(ChildWindow).close();
		//signinPage.gear_button_username.click();
		//signinPage.SignOut_link.click();
		driver.switchTo().window(ParentWindow);
	}
	
		
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}

}
