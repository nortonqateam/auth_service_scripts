package com.Norton.NCIA.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class TC50ExistingUser_TrialExpired_Login_Test extends PropertiesFile {

	NciaSignInPage SignInPage;
	Activity_group_selectors selectTile;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	WebDriverWait wait;
	String Trial_access_message;
	String ExpectedMessage;

	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		// PropertiesFile.getURL(QAurl +"essgeo5");
		PropertiesFile.setURL("psychsci6");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-50 Verify user having trial expired is not able to login to SW5 application after DB migration.")
	@Stories("Login in for Existing User and Verify user having trial expired is not able to login to SW5 application")
	@Step("Verify user having trial expired is not able to login to SW5 application, Method: {method}")
	@Test
	public void existing_trial_user_unabletologin() throws InterruptedException {
		SignInPage = new NciaSignInPage(driver);
		selectTile = new Activity_group_selectors(driver);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions
				.visibilityOf(SignInPage.Sign_in_or_Register));
		SignInPage.Sign_in_or_Register.click();
		String ExpiredUser = jsonobject
				.getAsJsonObject("ExpiredUser_psychsci6").get("UserLogin")
				.getAsString();
		SignInPage.Username.sendKeys(ExpiredUser);
		String Expired_Password = jsonobject
				.getAsJsonObject("ExpiredUser_psychsci6").get("UserPassword")
				.getAsString();
		SignInPage.Password.sendKeys(Expired_Password);
		SignInPage.SignInButton.click();
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions
				.visibilityOf(SignInPage.gear_button_username));
		selectTile.ClickSmartwork5.click();
		SignInPage.Assignmentlink.click();

		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		driver.switchTo().window(ChildWindow);
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions
				.visibilityOf(SignInPage.TrialErrorMessage));
		String url = driver.getCurrentUrl();
		System.out.println(url);
		Trial_access_message = SignInPage.TrialErrorMessage.getText();
		ExpectedMessage = "Your trial access period has expired. Please purchase access or redeem a registration code to continue accessing the product.";

		Assert.assertEquals(Trial_access_message, ExpectedMessage);
		driver.switchTo().window(ChildWindow).close();
		driver.switchTo().window(ParentWindow);
		SignInPage.gear_button_username.click();
		SignInPage.SignOut_link.click();

	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}
}
