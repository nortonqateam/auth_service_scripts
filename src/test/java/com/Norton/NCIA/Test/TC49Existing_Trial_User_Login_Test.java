package com.Norton.NCIA.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class TC49Existing_Trial_User_Login_Test extends PropertiesFile {

	NciaSignInPage Page;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("englishlit10abc");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-49 Verify existing trial user logs in successfully and upgrades from trail access to paid access")
	@Stories("Login in for Existing User")
	@Test
	public void existing_trial_user_logsin_successfully() throws Exception {
		Page = PageFactory.initElements(driver, NciaSignInPage.class);
		Page.clickSignin_Register();
		String UserName = jsonobject.getAsJsonObject("Users").get("loginemail")
				.getAsString();
		String Password = jsonobject.getAsJsonObject("Users")
				.get("loginpassword").getAsString();
		Page.signInUserAccount(UserName, Password);
		
		boolean loaderEle = driver.findElements(By.xpath("//div[@id='loading_screen']/div[@id='loading_message'][contains(text(),'Logging you in...')]")).size()>0;
		if (loaderEle == true) {
			ReusableMethods.loadingWaitDisapper(driver,
					driver.findElement(By.id("loading_screen")));
		} else {
			Thread.sleep(5000);
			
		}
		Page.verify_Username(UserName);
		Page.SignOut();
		
	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}

}
