package com.Norton.NCIA.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;
@Listeners({TestListener.class})
public class TC82ExistingUserAccessFreeProduct_Test extends PropertiesFile {
	
	NciaSignInPage Sign_Page;
	WebDriverWait wait;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	Activity_group_selectors selectTile;
	
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		//PropertiesFile.getURL(NortonKbetaurl);
		PropertiesFile.getURL(QAurl +"chem5");
		
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-82 Verify user is able to access particular product that is free.")
	@Stories("Verify user is able to access particular product that is free")
	@Step("Login in as Student and Verify user is able to access particular product that is free,Method: {method}")
	@Test
	public void Login_Smartwork5() throws Exception{
		Sign_Page = new NciaSignInPage(driver);
		selectTile = new Activity_group_selectors(driver);
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.Sign_in_or_Register));
		Sign_Page.Sign_in_or_Register.click();
		String UserName=jsonobject.getAsJsonObject("ProductFreeUser").get("UserLogin").getAsString();
		Sign_Page.Username.sendKeys(UserName);
		String Password=jsonobject.getAsJsonObject("ProductFreeUser").get("UserPassword").getAsString();
		Sign_Page.Password.sendKeys(Password);
		Sign_Page.SignInButton.click();
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.invisibilityOf(Sign_Page.Loader));
		wait.until(ExpectedConditions.visibilityOf(selectTile.ClickSmartwork5));
		selectTile.ClickSmartwork5.click();
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.Assignmentlink));
		Sign_Page.Assignmentlink.click();
		String ChildWindow = null;
	    Set<String> Windowhandles= driver.getWindowHandles();
	    String ParentWindow = driver.getWindowHandle();
	    Windowhandles.remove(ParentWindow);
	    String winHandle=Windowhandles.iterator().next();
	    if (winHandle!=ParentWindow){
	    	ChildWindow=winHandle;
	    }
	    driver.switchTo().window(ChildWindow);
	    driver.switchTo().frame("swfb_iframe");
	    String url = driver.getCurrentUrl();
		System.out.println(url);
		String title = driver.getTitle();
		System.out.println(title);
		wait = new WebDriverWait(driver,50);
		TimeUnit.SECONDS.sleep(20);
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.AssignmentText));
		String AssignmentText =Sign_Page.AssignmentText.getText();
		System.out.println(AssignmentText);
		driver.switchTo().defaultContent();
		driver.switchTo().window(ChildWindow).close();
		//signinPage.gear_button_username.click();
		//signinPage.SignOut_link.click();
		driver.switchTo().window(ParentWindow);
	}
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
