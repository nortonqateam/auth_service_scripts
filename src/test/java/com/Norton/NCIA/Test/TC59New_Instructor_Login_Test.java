package com.Norton.NCIA.Test;

import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.TestListener;

import com.Norton.Pcat.Pages.Pcat_Page;
import com.Norton.Smartwork5.Pages.NciaSignInPage;

@Listeners({ TestListener.class })
public class TC59New_Instructor_Login_Test extends PropertiesFile {
	static String Inst_Email;
	static String Inst_Password;
	NciaSignInPage Sign_Page;
	Pcat_Page Pcat_page;

	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-59 Verify that new instructor is able to login successfully after DB migration.")
	@Stories("Verify that new instructor is able to login successfully")
	@Test()
	public void Login_New_Instructor() throws Exception {
		pcat_Login();
		Thread.sleep(5000);
		PropertiesFile.setURL("psychsci6");
		Sign_Page = new NciaSignInPage(driver);
		Sign_Page.clickSignin_Register();

		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Sign_Page.yes_SignIn);
		SignIn.click();
		SignIn.perform();

		Sign_Page.Username.sendKeys(Inst_Email);
		Sign_Page.Password.sendKeys(Inst_Password);
		Sign_Page.SignInButton.click();
		String expectedUsername = Sign_Page.gear_button_username.getText();
		Thread.sleep(5000);
		Assert.assertEquals(Inst_Email.toLowerCase(),
				expectedUsername.toLowerCase());
		Sign_Page.gear_button_username.click();
		Sign_Page.SignOut_link.click();
	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}

	public void pcat_Login() throws Exception {
		PropertiesFile.setPcatURL();
		Pcat_page = new Pcat_Page(driver);
		Pcat_page.loginto_Pcat();
		Pcat_page.authorize_Instructor_Account_link();
		Pcat_page.authorize_Instructor_Account();

		Pcat_page.authorize_Instructor_getText();
		Inst_Email = Pcat_Page.Inst_Email;
		Inst_Password = Pcat_Page.Inst_Password;
		Pcat_page.SignOut_Pcat();
		// driver.close();
	}
}
