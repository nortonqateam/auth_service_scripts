package com.Norton.NCIA.Test;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.StudentSite_Page;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({TestListener.class})
public class TC86ExistingUser_login_NOQUE_Test extends PropertiesFile{
	
	
	Activity_group_selectors selectTile;
	NciaSignInPage signinPage;
	StudentSite_Page studentcontentpage;
	WebDriverWait wait;
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		//PropertiesFile.setURL("americanlit9shorter");
		PropertiesFile.getURL(QAurl + "americanlit9shorter");
		
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-86 Verify that exiting user is able to login to NOQUE")
	@Stories("Login in as Existing Instructor and Verify that exiting user is able to login to NOQUE")
	@Step("Click on Quizzes tile,  Method: {method} ")
	@Test(priority=0)
	public void ClickQuizzesTile() throws Exception{
		Thread.sleep(5000);
		selectTile= new Activity_group_selectors(driver);
		selectTile.Quizzes.click();
		Thread.sleep(5000);
		selectTile.clickfirstlink_Quizzes.click();
		String ChildWindow = null;
	    Set<String> Windowhandles= driver.getWindowHandles();
	    String ParentWindow = driver.getWindowHandle();
	    Windowhandles.remove(ParentWindow);
	    String winHandle=Windowhandles.iterator().next();
	    if (winHandle!=ParentWindow){
	    	ChildWindow=winHandle;
	    }
	    driver.switchTo().window(ChildWindow);
	    Thread.sleep(15000);
	    signinPage =new NciaSignInPage(driver);
	    Actions SignIn = new Actions(driver);
	  /*  WebDriverWait Wait = new WebDriverWait(driver, 50);
	    Wait.until(ExpectedConditions.elementToBeClickable(signinPage.yes_SignIn));
	   
	   */
		SignIn.moveToElement(signinPage.yes_SignIn);
		SignIn.click();
		SignIn.perform();
		boolean usernametextbox = driver.findElement(By.id("username")).isEnabled();
		System.out.println(usernametextbox);
		signinPage.Username.click();
		String UserName=jsonobject.getAsJsonObject("Users").get("loginemail").getAsString();
		signinPage.Username.sendKeys(UserName);
		signinPage.Password.sendKeys(jsonobject.getAsJsonObject("Users").get("loginpassword").getAsString());
		signinPage.SignInButton.click();
		Thread.sleep(5000);
		selectTile.StudentSetIDButton.click();
		Thread.sleep(5000);
		selectTile.Start_Quiz_Button.click();
		Thread.sleep(5000);
		
		driver.switchTo().window(ChildWindow).close();
		driver.switchTo().window(ParentWindow);
		signinPage.gear_button_username.click();
		signinPage.SignOut_link.click();
	}
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}

}
