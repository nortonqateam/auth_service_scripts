package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class TC58Existing_Instructor_Login_Test extends PropertiesFile {

	NciaSignInPage Sign_Page;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	WebDriverWait wait;

	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("psychsci6");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-58 Verify that existing instructor is able to login successfully after DB migration..")
	@Stories("Login in for Existing Instructor and Verify that existing instructor is able to login successfully")
	@Test()
	public void Login_New_Instructor() throws Exception {
		Thread.sleep(5000);
		Sign_Page = new NciaSignInPage(driver);
		Sign_Page.clickSignin_Register();

		System.out.println("Sign in is Completed");
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Sign_Page.yes_SignIn);
		SignIn.click();
		SignIn.perform();

		String Auth_Inst_Email = jsonobject
				.getAsJsonObject("Existing_Instructor")
				.get("Authorize_Instructor_Email").getAsString();

		String Auth_Inst_Password = jsonobject
				.getAsJsonObject("Existing_Instructor")
				.get("Authorize_Instructor_Password").getAsString();

		Sign_Page.Username.sendKeys(Auth_Inst_Email);
		Sign_Page.Password.sendKeys(Auth_Inst_Password);
		Sign_Page.SignInButton.click();
		wait = new WebDriverWait(driver, 50);
		TimeUnit.SECONDS.sleep(30);
		wait.until(ExpectedConditions
				.visibilityOf(Sign_Page.gear_button_username));
		String expectedUsername = Sign_Page.gear_button_username.getText();
		// System.out.println(expectedUsername);
		Assert.assertEquals(Auth_Inst_Email.toLowerCase(),
				expectedUsername.toLowerCase());
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions
				.visibilityOf(Sign_Page.gear_button_username));
		Sign_Page.gear_button_username.click();
		Sign_Page.SignOut_link.click();
	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}
}
