package com.Norton.NCIA.Test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.LogUtil;
import Utilities.Mailinator;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.Norton.NortonWebsite.Pages.NortonWebsiteLogin_Page;
import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;
import com.google.gson.JsonObject;

public class ForgotPasswordandresetPassword extends PropertiesFile {

	NciaSignInPage Sign_Page;
	Activity_group_selectors selectTile;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	WebDriverWait wait;
	Register_Purchase_Page accessPage;
	NortonWebsiteLogin_Page NortonPage;

	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		// PropertiesFile.getURL(QAurl +"essgeo5");
		PropertiesFile.setURL("psychsci6");

	}
	@Severity(SeverityLevel.NORMAL)
	@Description("User resets password through Forgot Password, receives new password and verifies that user is able to login successfully with new password.")
	@Stories("User resets password through Forgot Password, receives new password and verifies that user is able to login successfully with new password.")
	@Test
	public void forgotResetPassword() throws Exception{
		Sign_Page = new NciaSignInPage(driver);
		Sign_Page.clickSignin_Register();
		accessPage = PageFactory.initElements(driver,
				Register_Purchase_Page.class);
		String StudentEmailID =accessPage.createNewUser();
		LogUtil.log(StudentEmailID);
		Sign_Page.SignOut();
		Sign_Page.clickSignin_Register();
		List<WebElement> oRadiobuttons = driver.findElements(By.name("register_login_choice"));
		boolean bvalue = false;
		bvalue = oRadiobuttons.get(0).isSelected();
		if (bvalue == true) {
			oRadiobuttons.get(1).click();
		} else {
			oRadiobuttons.get(1).click();
		}
        Sign_Page.resetPassword(StudentEmailID);
        Thread.sleep(5000);
        WebElement clickOkbutton = driver.findElement(
				By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'Ok')]"));
        clickOkbutton.click();
        Mailinator m = new Mailinator(driver);
 		m.SearchEmail(StudentEmailID.trim());
 		m.clickResetPasswordEmailLink();
 		m.clickResetPasswordLinkUserEmail();
 		Thread.sleep(5000);
 		ReusableMethods.switchToNewWindow(3,driver);
 		NortonPage = new NortonWebsiteLogin_Page(driver);
 		ReusableMethods.checkPageIsReady(driver);
 		String enteredPassword =NortonPage.enterNewPasswordInfo();
 		ReusableMethods.checkPageIsReady(driver);
 		Sign_Page.clickSignin_Register();
 		Sign_Page.signInUserAccount(StudentEmailID, enteredPassword);
 		Sign_Page.verify_Username(StudentEmailID);
 		Sign_Page.SignOut();
	}
	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}
}
