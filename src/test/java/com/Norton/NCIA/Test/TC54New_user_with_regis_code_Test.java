package com.Norton.NCIA.Test;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class TC54New_user_with_regis_code_Test extends PropertiesFile {

	NciaSignInPage Sign_Page;
	Register_Purchase_Page accessPage;

	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("psychsci6");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-54 Create new user, select I have a registration code: and verify that user is able to login successfully to SW5 application")
	@Stories("Create new user, select I have a registration code: and verify that user is able to login successfully to SW5 application")
	@Test
	public void active_Registration_Code_User_Creation() throws Exception {

		Sign_Page = new NciaSignInPage(driver);

		accessPage = PageFactory.initElements(driver,
				Register_Purchase_Page.class);

	
			Sign_Page.clickSignin_Register();
			Sign_Page.need_to_Register();
			String emailID =accessPage.register_Access_ValidCode();
			accessPage.confirm_Registration(emailID);
			accessPage.term_privacypolicy_popup();
			accessPage.getStarted_popup();

		

	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}

}
