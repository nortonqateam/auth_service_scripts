package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.StudentSite_Page;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({TestListener.class})
public class TC85ExistingUser_login_content_table_Test extends PropertiesFile {
	
	Activity_group_selectors selectTile;
	NciaSignInPage signinPage;
	StudentSite_Page studentcontentpage;
	WebDriverWait wait;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		//PropertiesFile.setURL("chem4");
	//	PropertiesFile.getURL(ContenttableURL);
		PropertiesFile.getURL(QAurl + "ecb4");
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-85 Verify that exiting user is able to login to content table")
	@Stories("Login in as Existing Instructor and Verify that exiting user is able to login to content table")
	@Step("Click on Student Site tile,  Method: {method} ")
	@Test(priority=0)
	public void ClickStudentTile() throws Exception{
		selectTile = new Activity_group_selectors(driver);
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(selectTile.Student_Site));
		selectTile.Student_Site.click();
	}
	
	@Step("Signin to SW5,  Method: {method} ")
	@Test(priority=1)
	public void Login_Smartwork5() throws Exception{
		Thread.sleep(5000);
		driver.switchTo().frame("cta_44586");
		signinPage =new NciaSignInPage(driver);
		//signinPage.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(signinPage.yes_SignIn);
		SignIn.click();
		SignIn.perform();
		boolean usernametextbox = driver.findElement(By.id("username")).isEnabled();
		System.out.println(usernametextbox);
		signinPage.Username.click();
		String UserName=jsonobject.getAsJsonObject("InstructorLogin").get("Instloginemail").getAsString();
		signinPage.Username.sendKeys(UserName);
		signinPage.Password.sendKeys(jsonobject.getAsJsonObject("InstructorLogin").get("Instloginpassword").getAsString());
		signinPage.SignInButton.click();
		Thread.sleep(5000);
	}
	
	@Step("Signin to SW5,  Method: {method} ")
	@Test(priority=2)
	public void ClickContentTable() throws Exception{
		Thread.sleep(5000);
		//driver.switchTo().frame("cta_44586");
		studentcontentpage = new StudentSite_Page(driver);
		studentcontentpage.VerifyContentTable();
		
	}
	
	@Step("Signout to SW5,  Method: {method} ")
	@Test(priority=3)
	public void SignOut() throws Exception{
		Thread.sleep(5000);
		//driver.switchTo().frame("cta_44586");
		signinPage.gear_button_username.click();
		signinPage.SignOut_link.click();
		
	}
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
