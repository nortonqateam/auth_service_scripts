package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;




import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({TestListener.class})
public class TC63ExistingStudentFullPurchase_Test extends PropertiesFile {
	NciaSignInPage Sign_Page;
	Register_Purchase_Page Rpg;
	WebDriverWait wait;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("psychsci6");
		//PropertiesFile.getURL(QAUrl +"chem4");
		
		
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-63 Verify that exiting user having full access successfully logs in to application after DB migration.")
	@Stories("Login in as Student and verify Already purchase functionality")
	@Test
	public void Login_Smartwork5() throws Exception{
		Sign_Page = new NciaSignInPage(driver);
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.Sign_in_or_Register));
		Sign_Page.Sign_in_or_Register.click();
		String UserName=jsonobject.getAsJsonObject("FullpurchasedUser").get("UserLogin").getAsString();
		Sign_Page.Username.sendKeys(UserName);
		String Password=jsonobject.getAsJsonObject("FullpurchasedUser").get("UserPassword").getAsString();
		Sign_Page.Password.sendKeys(Password);
		Sign_Page.SignInButton.click();
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.purchase_options_button));
		Sign_Page.purchase_options_button.click();
		wait = new WebDriverWait(driver,10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.login_Dialog_msg));
		Rpg = new Register_Purchase_Page(driver);
		Rpg.VerifyPurchaseData();
		String actualText="You already have access to all digital materials associated with this book.";
		String ExpectedText=Sign_Page.purchase_text.getText();
		Assert.assertEquals(ExpectedText, actualText);
		
	}

	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
