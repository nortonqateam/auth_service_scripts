package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.NciaSignInPage;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class TC52Login_ExistingUser_RegCode_Test extends PropertiesFile {
	NciaSignInPage Sign_Page;

	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("englishlit10abc");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-52 Existing user having a registration code is able to Login to SW5 application ")
	@Stories("Existing user having a registration code is able to Login to SW5 application")
	@Test
	public void Login_ExistingUser_RegCode() throws Exception {
		Sign_Page = new NciaSignInPage(driver);
		Sign_Page.clickSignin_Register();
		Sign_Page.existingUser_RegCode();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions
				.visibilityOf(Sign_Page.gear_button_username));
		Sign_Page.gear_button_username.click();
		Sign_Page.SignOut_link.click();
	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}

}
