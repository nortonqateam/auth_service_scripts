package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class GlobalPassword_Test extends PropertiesFile {
	NciaSignInPage Sign_Page;
	Activity_group_selectors select_Tile;

	WebDriverWait wait;

	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("psychsci6");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-78 Confirm that backdoor password still works to log-in as another user")
	@Stories("Login to validate GlobalPassword User")
	@Test
	public void GlobalPassword_logsin_successfully() throws Exception {
		Sign_Page = new NciaSignInPage(driver);
		select_Tile = new Activity_group_selectors(driver);
		Sign_Page.clickSignin_Register();
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Sign_Page.yes_SignIn);
		SignIn.click();
		SignIn.perform();

		String UserName = jsonobject.getAsJsonObject("GlobalPassword")
				.get("UserLogin").getAsString();
		Sign_Page.Username.sendKeys(UserName);
		Sign_Page.Password.sendKeys(jsonobject
				.getAsJsonObject("GlobalPassword").get("UserPassword")
				.getAsString());
		Sign_Page.SignInButton.click();
		wait = new WebDriverWait(driver, 7);
		TimeUnit.SECONDS.sleep(7);

		wait.until(ExpectedConditions
				.visibilityOf(Sign_Page.gear_button_username));
		select_Tile.ClickEbook.click();
		Thread.sleep(2000);
		select_Tile.return_to_resources_button_text.click();
		select_Tile.InQuizitive.click();
		Thread.sleep(2000);
		select_Tile.return_to_resources_button_text.click();
		select_Tile.ZAPS.click();
		Thread.sleep(2000);
		select_Tile.return_to_resources_button_text.click();
		Sign_Page.SignOut();
	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}
}
