package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

import com.Norton.Smartwork5.Pages.GearMenu_Page;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

@Listeners({TestListener.class})
public class TC60Existing_Student_Change_Password_Test extends PropertiesFile {
	
	NciaSignInPage Sign_Page;
	GearMenu_Page  menuItem;
	String currentEmailID;
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("englishlit10major");
		
	} 
	@Step("Click on Sign in or Register,  Method: {method} ")
	@Test(priority=0)
	public void login_Smartwork5() throws Exception{
		Sign_Page = new NciaSignInPage(driver);
		Sign_Page.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Sign_Page.yes_SignIn);
		SignIn.click();
		SignIn.perform();
        currentEmailID = jsonobject.getAsJsonObject("Change_Your_Password").get("CurrentEmailID").getAsString();
		String password = jsonobject.getAsJsonObject("Change_Your_Password").get("CurrentPassword").getAsString();
		Sign_Page.Username.sendKeys(currentEmailID);
		Sign_Page.Password.sendKeys(password);
		Sign_Page.SignInButton.click();
		WebDriverWait wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.gear_button_username));
		Sign_Page.gear_button_username.click();
		 
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-60 Exiting student logs in, selects Change Your Password and verifies that password is changed successfully")
	@Stories("Login in to Smartwork5 & Change Password")
	@Step("User changes the Password,  Method: {method} ")
	@Test(priority=1)
	public void change_Password() throws Exception{
		menuItem = new GearMenu_Page(driver);
		menuItem.change_password();
		Thread.sleep(3000);
		menuItem.reset_password();
		Thread.sleep(3000);
		Sign_Page.SignOut();
	}
	@Step("Login into the application with the New Password,  Method: {method} ")
	@Test(priority=2)
	public void login_Smartwork5_afterresetpassword() throws Exception{
		Sign_Page.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Sign_Page.yes_SignIn);
		SignIn.click();
		SignIn.perform();
        currentEmailID = jsonobject.getAsJsonObject("Change_Your_Password").get("CurrentEmailID").getAsString();
		String password = jsonobject.getAsJsonObject("Change_Your_Password").get("ResetPassword").getAsString();
		Sign_Page.Username.clear();
		Sign_Page.Username.sendKeys(currentEmailID);
		Sign_Page.Password.sendKeys(password);
		Sign_Page.SignInButton.click();
		Thread.sleep(3000);
		Sign_Page.SignOut();	 
	}
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
