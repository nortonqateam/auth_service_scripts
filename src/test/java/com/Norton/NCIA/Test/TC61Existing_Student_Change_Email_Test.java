package com.Norton.NCIA.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

import com.Norton.Smartwork5.Pages.GearMenu_Page;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

@Listeners({TestListener.class})
public class TC61Existing_Student_Change_Email_Test extends PropertiesFile {
	NciaSignInPage Sign_Page;
	String currentEmailID;
	GearMenu_Page  menuItem;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	WebDriverWait wait;
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL("englishlit10major");
		
		
	} 
	
	@Step("Click on Sign in or Register,  Method: {method} ")
	@Test(priority=0)
		public void login_Smartwork5() throws Exception{
		Sign_Page = new NciaSignInPage(driver);
		Sign_Page.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Sign_Page.yes_SignIn);
		SignIn.click();
		SignIn.perform();
        currentEmailID = jsonobject.getAsJsonObject("Change_Your_Email").get("Current_EmailID").getAsString();
		String password = jsonobject.getAsJsonObject("Change_Your_Email").get("Password").getAsString();
		Sign_Page.Username.sendKeys(currentEmailID);
		Sign_Page.Password.sendKeys(password);
		Sign_Page.SignInButton.click();
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.gear_button_username));
		Sign_Page.gear_button_username.click();
		Thread.sleep(3000);
		 
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-61 Exiting student logs in, selects Change Your Email and verifies that Email is changed successfully")
	@Stories("Login in to Smartwork5 and Exiting student logs in, selects Change Your Email ")
	@Step("Change the Email ID and Verify,  Method: {method} ")
	@Test(priority=1)
	public void change_Email() throws Exception{
		menuItem = new GearMenu_Page(driver);
		Thread.sleep(5000);
		menuItem.change_Email();
		Thread.sleep(5000);
		menuItem.update_Email();
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Sign_Page.gear_button_username));
		String NewEmailID =Sign_Page.gear_button_username.getText().toLowerCase();
		Assert.assertNotEquals(NewEmailID, currentEmailID, "The NewEmailID and Existing ID are changed");
		Sign_Page.SignOut();
	}
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
