package com.Norton.NCIA.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.NortonWebsite.Pages.Instructor_Resources_Page;
import com.Norton.Smartwork5.Pages.Activity_group_selectors;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;
@Listeners({TestListener.class})
public class TC77Instructor_Resources_Test extends PropertiesFile {
	
	Activity_group_selectors selectTile;
	NciaSignInPage signin;
	WebDriverWait wait;
	Instructor_Resources_Page inst_resources;
	
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		//PropertiesFile.setURL("chem4");
		PropertiesFile.setURL("englishlit10abc");
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-77 Verify that Instructor Resources works after DB migration.")
	@Stories("Login in for Existing Instructor and Verify that Instructor Resources functionality")
	
	@Step("Click on Sign in or Register,  Method: {method} ")
	@Test(priority=0)
	public void Login_Smartwork5() throws Exception{
		signin =new NciaSignInPage(driver);
		signin.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(signin.yes_SignIn);
		SignIn.click();
		SignIn.perform();
		
		String UserName=jsonobject.getAsJsonObject("InstructorLogin").get("Instloginemail").getAsString();
		signin.Username.sendKeys(UserName);
		signin.Password.sendKeys(jsonobject.getAsJsonObject("InstructorLogin").get("Instloginpassword").getAsString());
		signin.SignInButton.click();
	}
	@Step("Click on Instructor Resources Tile and Verify the Page details,  Method: {method} ")
	@Test(priority=1)
	public void Click_Instructor_Resources() throws Exception{
		
		selectTile =new Activity_group_selectors(driver);
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOf(selectTile.ClickInstructorResources));
		selectTile.ClickInstructorResources.click();
		Thread.sleep(5000);
		String ChildWindow = null;
        Set<String> Windowhandles= driver.getWindowHandles();
        String ParentWindow = driver.getWindowHandle();
        Windowhandles.remove(ParentWindow);
        String winHandle=Windowhandles.iterator().next();
        if (winHandle!=ParentWindow){
        	ChildWindow=winHandle;
        }
        driver.switchTo().window(ChildWindow);
        Thread.sleep(10000);
      //  WebDriverWait wait = new WebDriverWait(driver, 15);
      //  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='overview']/h1")));
        inst_resources =new Instructor_Resources_Page(driver);
        String url = driver.getCurrentUrl();
		System.out.println(url);
		String title = driver.getTitle();
		System.out.println(title);
		//String instTitle =inst_resources.instructor_resources_title.getText();
		//System.out.println(instTitle);
		WebDriverWait wait = new WebDriverWait(driver, 5000L) ;
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div/h2[@class='irbooktitle']")));
		String bookTitle =inst_resources.book_title.getText();
		System.out.println(bookTitle);
		driver.switchTo().window(ChildWindow).close();
		driver.switchTo().window(ParentWindow);
	}
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}

}
