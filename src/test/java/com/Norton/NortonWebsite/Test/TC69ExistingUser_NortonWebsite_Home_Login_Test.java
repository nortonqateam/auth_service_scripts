package com.Norton.NortonWebsite.Test;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.NortonWebsite.Pages.NortonWebsiteLogin_Page;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;


@Listeners({TestListener.class})
public class TC69ExistingUser_NortonWebsite_Home_Login_Test extends PropertiesFile{
	
	NortonWebsiteLogin_Page NortonPage;
	boolean firstlastname ;
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.NortonWebSiteurl();
		
	}
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-69 Verify that existence user is able to login to Norton website from Homepage after DB migration.")
	@Stories("Login in for Existing User")
	
	 @Test
	    public void NortonLogin() throws InterruptedException
	    {
		String UserName = jsonobject.getAsJsonObject("NortonWebsiteLogin")
				.get("LoginEmailID").getAsString();
		String Password =jsonobject.getAsJsonObject("NortonWebsiteLogin")
		.get("LoginPassword").getAsString();
		    NortonPage = new NortonWebsiteLogin_Page(driver);
			NortonPage.Login_NortonApplication(UserName, Password);
			
			firstlastname=NortonPage.FirstName_LastName.getText() != null;
			Assert.assertTrue(firstlastname);
			String LoginMail = NortonPage.Email.getText();
			String Email = jsonobject.getAsJsonObject("NortonWebsiteLogin").get("LoginEmailID").getAsString();
			Assert.assertEquals(Email, LoginMail);
		
		
	    }
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}

}
