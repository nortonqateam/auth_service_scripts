package com.Norton.NortonWebsite.Test;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.Norton.NortonWebsite.Pages.NortonWebsiteLogin_Page;
import com.google.gson.JsonObject;

public class NortonWebSiteIICGIntegration extends PropertiesFile {
	
	NortonWebsiteLogin_Page NortonPage;
	boolean firstlastname ;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.NortonWebSiteurl();
		
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("As a logged in user, verify user is able to change password.")
	@Stories("As a logged in user, verify user is able to change password.")
	@Test
	public void NortonIIGDemoVerification() throws InterruptedException{
		String UserName = jsonobject.getAsJsonObject("NortonWebsiteLogin")
				.get("LoginEmailID").getAsString();
		String Password =jsonobject.getAsJsonObject("NortonWebsiteLogin")
		.get("LoginPassword").getAsString();
		NortonPage = new NortonWebsiteLogin_Page(driver);
		NortonPage.Login_NortonApplication(UserName, Password);
		ReusableMethods.scrollToElement(driver, By.id("globalInput"));
		NortonPage.SearchText.click();
		String ISBNNumber = jsonobject.getAsJsonObject("SearchBookText")
				.get("ISBN").getAsString();
		NortonPage.SearchText.sendKeys(ISBNNumber);
		LogUtil.log(ISBNNumber);
		NortonPage.clickSearchbutton();
		Thread.sleep(5000);
		String bookText = NortonPage.getBookdetails();
		LogUtil.log(bookText);
		NortonPage.clickOpenInstructorResourcesButton();
		Thread.sleep(5000);
		NortonPage.clickDemobutton();
		Thread.sleep(5000);
		ReusableMethods.switchToNewWindow(2, driver);
		String iigDemoText="Demo";
		Assert.assertEquals(iigDemoText, NortonPage.getDemoTextonIIG());
		Assert.assertNotNull(NortonPage.getHeaderTitleIIG().trim());
		Assert.assertEquals(UserName.toLowerCase().trim(), NortonPage.getUserInfoIIG().toLowerCase().trim());
		driver.close();
		ReusableMethods.switchToNewWindow(1, driver);
		NortonPage.clickViewFullSite();
		Thread.sleep(5000);
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertNotNull(NortonPage.getHeaderTitleIIG().trim());
		Assert.assertEquals(UserName.toLowerCase().trim(), NortonPage.getUserInfoIIG().toLowerCase().trim());
		NortonPage.logOutIIG();
	}
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
