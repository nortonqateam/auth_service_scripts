package com.Norton.NortonWebsite.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.NortonWebsite.Pages.NortonWebsiteLogin_Page;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;

@Listeners({TestListener.class})
public class TC72NortonWebSite_Dashboard_SinglesSIgnOn extends PropertiesFile {

	NortonWebsiteLogin_Page NortonPage;
	NciaSignInPage NciaPage;
	boolean firstlastname;
	WebDriverWait wait;
	String AccountEmailID;
	String EmailID;
	
	@Parameters("Browser")
	@BeforeTest
	
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		//issue with QA Environment hence testing in Preview URL
		//PropertiesFile.NortonPreviewDashboardurl();
		PropertiesFile.NortonWebSiteurl();

	}

	// Call ReadJsonobject method to read and set node values from Json testData
	// file.

	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-72 User  creates new account on Norton website through Dashboard successfully, accesses digital product and successfully logs in to DLP with new credentials using single sign on facility.")
	@Stories("New User Creation through Dashboard and Verify Single Sign On")
	@Test()
	public void NortonCreateAccount() throws InterruptedException {
		NortonPage = new NortonWebsiteLogin_Page(driver);
		EmailID =NortonPage.NewAccount_NortonApplication();
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(NortonPage.EmailText));
		String emailtext="Please Check Your Email";
	    String emailText_popup=NortonPage.EmailText.getText();
	    Assert.assertEquals("The Text is Matched and Account is created", emailtext, emailText_popup);
		NortonPage.ContinueButton.click();
		//NortonPage.Norton_Log_in.click();
		//NortonPage.ProfileLink.click();
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		NortonPage.ProfileLink.click();
		wait.until(ExpectedConditions.visibilityOf(NortonPage.Email));
		firstlastname = NortonPage.FirstName_LastName.getText() != null;
		Assert.assertTrue("FirstName and Last name is displayed", firstlastname);
		String LoginMail = NortonPage.Email.getText();
			
		Assert.assertEquals("The Email ID is matched", EmailID.toLowerCase().trim(), LoginMail.toLowerCase().trim());
		Thread.sleep(5000);
		ReusableMethods.scrollToElement(driver, By.id("globalInput"));
		NortonPage.SearchText.click();
		String BookName = jsonobject.getAsJsonObject("SearchBookText")
				.get("BookName").getAsString();
		NortonPage.SearchText.sendKeys(BookName.trim());
		NortonPage.searchButton.click();
		Thread.sleep(5000);
		NortonPage.clickBookTitle(BookName);
		wait.until(ExpectedConditions.visibilityOf(NortonPage.ViewAllOptionsbutton));
		/*Actions ViewAllOptions = new Actions(driver);
		ViewAllOptions.moveToElement(NortonPage.ViewAllOptionsbutton);
		ViewAllOptions.click();
		ViewAllOptions.perform();*/
		ReusableMethods.scrollToElement(driver, By.id("globalInput"));
		NortonPage.clickViewAllOptiosnButton();
		Thread.sleep(5000);
		NciaPage =new NciaSignInPage(driver);
		String ChildWindow = null;
        Set<String> Windowhandles= driver.getWindowHandles();
        String ParentWindow = driver.getWindowHandle();
        Windowhandles.remove(ParentWindow);
        String winHandle=Windowhandles.iterator().next();
        if (winHandle!=ParentWindow){
        	ChildWindow=winHandle;
        }
       driver.switchTo().window(ChildWindow);
       WebDriverWait waitUserName = new WebDriverWait(driver, 5000L);
       waitUserName.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='gear_button_username']")));
       Thread.sleep(5000);
       String dlpUserName= NciaPage.gear_button_username.getText();
       Assert.assertEquals(EmailID.toLowerCase().trim(), dlpUserName.toLowerCase().trim());
       LogUtil.log(dlpUserName);
       Thread.sleep(5000);
       driver.switchTo().window(ParentWindow);
	}
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
