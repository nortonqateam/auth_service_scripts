package com.Norton.NortonWebsite.Test;

import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.NortonWebsite.Pages.NortonWebsiteLogin_Page;
import com.google.gson.JsonObject;

import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

@Listeners({TestListener.class})
public class TC70NortonWebSite_CreateNewAccount_Test extends PropertiesFile {

	NortonWebsiteLogin_Page NortonPage;
	boolean firstlastname;
	WebDriverWait wait;
	
	@Parameters("Browser")
	@BeforeTest
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.NortonWebSiteurl();

	}

	// Call ReadJsonobject method to read and set node values from Json testData
	// file.

	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	@Severity(SeverityLevel.NORMAL)
	@Description("TC-70 User  creates new account on Norton website through Homepage and successfully logs in to the website after DB migration.")
	@Stories("New User Creation")
	
	@Test
	public void NortonCreateAccount() throws InterruptedException {
		NortonPage = new NortonWebsiteLogin_Page(driver);
		String EmailID =NortonPage.NewAccount_NortonApplication();
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(NortonPage.EmailText));
		String emailtext="Please Check Your Email";
	    String emailText_popup=NortonPage.EmailText.getText();
	    Assert.assertEquals("The Text is Matched and Account is created", emailtext, emailText_popup);
		NortonPage.ContinueButton.click();
		//NortonPage.Norton_Log_in.click();
		NortonPage.ProfileLink.click();

		firstlastname = NortonPage.FirstName_LastName.getText() != null;
		Assert.assertTrue("FirstName and Last name is displayed", firstlastname);
		String LoginMail = NortonPage.Email.getText();
		Assert.assertEquals("The Email ID is matched", EmailID.toLowerCase().trim(), LoginMail.toLowerCase().trim());
		Thread.sleep(5000);
		NortonPage.Logout_NortonApplication();

	}
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
