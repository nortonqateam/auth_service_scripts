package com.Norton.NortonWebsite.Test;

import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.Mailinator;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;

import com.Norton.NortonWebsite.Pages.NortonWebsiteLogin_Page;

public class ForgotPassword extends PropertiesFile{
	
	NortonWebsiteLogin_Page NortonPage;
	boolean firstlastname ;
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.NortonWebSiteurl();
		
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("As a logged in user, Verify user is able to reset password from forgot your password link .")
	@Stories("As a logged in user, Verify user is able to reset password from forgot your password link ")
	@Test
	public void NortonCreateAccountForgotPassword() throws Exception {
		
		NortonPage = new NortonWebsiteLogin_Page(driver);
		String EmailID =NortonPage.NewAccount_NortonApplication();
		wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(NortonPage.EmailText));
		String emailtext="Please Check Your Email";
	    String emailText_popup=NortonPage.EmailText.getText();
	    Assert.assertEquals("The Text is Matched and Account is created", emailtext, emailText_popup);
		NortonPage.ContinueButton.click();
		//NortonPage.Norton_Log_in.click();
		NortonPage.ProfileLink.click();

		firstlastname = NortonPage.FirstName_LastName.getText() != null;
		Assert.assertTrue("FirstName and Last name is displayed", firstlastname);
		String LoginMail = NortonPage.Email.getText();
		Assert.assertEquals("The Email ID is matched", EmailID.toLowerCase().trim(), LoginMail.toLowerCase().trim());
		Thread.sleep(5000);
		NortonPage.Logout_NortonApplication();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@id='profileLogin']")));
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(NortonPage.Norton_Log_in);
		SignIn.click();
		SignIn.perform();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		System.out.println(LoginMail);
		NortonPage.loginEmail.sendKeys(LoginMail.toLowerCase().trim());
		NortonPage.clickForgotyourPassword();
		NortonPage.submitForgotPasswordEmail(LoginMail.trim());
		Thread.sleep(3000);
		String expected = NortonPage.resetPasswordText("Password Reset Email Has Been Sent");
		Assert.assertEquals(expected.trim(), "Password Reset Email Has Been Sent");
		
		Mailinator m = new Mailinator(driver);
		m.SearchEmail(LoginMail.trim());
		m.clickResetPasswordEmailLink();
		m.clickResetPasswordLinkUserEmail();
		Thread.sleep(5000);
		ReusableMethods.switchToNewWindow(3,driver);
		
		String enteredPassword =NortonPage.enterNewPasswordInfo();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@id='profileLogin']")));
		Actions signInagain = new Actions(driver);
		signInagain.moveToElement(NortonPage.Norton_Log_in);
		signInagain.click();
		signInagain.perform();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		NortonPage.loginEmail.sendKeys(LoginMail.toLowerCase().trim());
		NortonPage.loginPassword.sendKeys(enteredPassword);
		NortonPage.LoginSubmitButton.click();
		Thread.sleep(5000);
		NortonPage.Logout_NortonApplication();
		driver.close();
		ReusableMethods.switchToNewWindow(2, driver);
		driver.close();
	}

	
	@AfterTest
    public void Logout(){
		ReusableMethods.switchToNewWindow(1, driver);
		driver.close();
	}

	
	
}
