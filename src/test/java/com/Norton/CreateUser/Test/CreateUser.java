package com.Norton.CreateUser.Test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.CreateUsers.Pages.NortonWebSite_CreateAccount;
import com.Norton.CreateUsers.Pages.SmartWork5_NewRegisteration;

import Utilities.PropertiesFile;

public class CreateUser extends PropertiesFile{ 
	
	static final Logger log = Logger.getLogger(CreateUser.class);
	NortonWebSite_CreateAccount CreateNewUser;
	SmartWork5_NewRegisteration CreateSW5user;
	
	@Parameters("Browser")
	@BeforeTest
	public void callPropertiesFile() throws Exception {

		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		

	}
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify New User gets created to validate the PINE service.")
	@Stories("Login in for New User")
	@Test(priority=0)
	public void NortonCreateAccount() throws InterruptedException {
		PropertiesFile.NortonWebSiteurl();
		CreateNewUser = new NortonWebSite_CreateAccount(driver);
		log.info("Login into Website");
		CreateNewUser.Login_NortonWebSite();
		
	}
	
	@Test(priority=1)
	public void CreateSW5Account() throws InterruptedException{
		PropertiesFile.getURL(SW5Stg2URL);
		Thread.sleep(3000);
		CreateSW5user = new SmartWork5_NewRegisteration(driver);
		log.info("Login into Smartwork5");
		CreateSW5user.Login_Smartwork5();
		
		
	}
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}

}
