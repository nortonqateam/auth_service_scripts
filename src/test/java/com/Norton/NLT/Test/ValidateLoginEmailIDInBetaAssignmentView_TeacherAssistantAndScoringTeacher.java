package com.Norton.NLT.Test;
/* Scoring Teaching Assistant and Teacher Assistant logs in to DLP, launches Beta Assignment View and 
 * verifies that STA and TA remains signed in and system displays same email address 
 * which is used at the time of login.*/




import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Utilities.GetDate;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.TestListener;

import com.Norton.NLT.Pages.BetaAssignmentPage;
import com.Norton.NLT.Pages.LaunchBetaAssignment;
import com.Norton.NLT.Pages.LoginPage;
import com.Norton.NLT.Pages.SelectProductDLP;
import com.Norton.NLT.Pages.SetAssignmentGAU;
import com.Norton.Smartwork5.Pages.Register_Purchase_Page;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class ValidateLoginEmailIDInBetaAssignmentView_TeacherAssistantAndScoringTeacher extends PropertiesFile {
	
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	SelectProductDLP ProductDLP;
	Register_Purchase_Page RPP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String jsonProductName;
	String jsonSSName;
	
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		// Call to Properties file initiate Browser and set Test URL.

		public void callPropertiesFile() throws Exception {

			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setNLTURL("psychsci6");

		}

		// Read login data from Json File
		
		ReadJsonFile readJasonObject = new ReadJsonFile();
		JsonObject jsonobject = readJasonObject.readJson();
		

		// Allure Annotations

		@Severity(SeverityLevel.NORMAL)
		@Description("Verify that system displays same email address which is used at the time of login.")
		@Stories("User Login")
		
		
		@Test
		public void TC02_ValidateLoginEmailIDInBetaAssignmentView_TeacherAssistantAndScoringTeacher() throws Exception { 
			
			//Login to NCIA application as an Instructor.
			
			NLTLogin = new LoginPage(driver);
			
			userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
			Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
			
			NLTLogin.loginNLT(userName, Password);
			
			wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));

			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
			RPP = new Register_Purchase_Page(driver);
			RPP.clickMaintenanceButton();
			
			// Set Assignment GAU Date for InQuizitive.
			
			ProductDLP = new SelectProductDLP(driver);
			
			
			jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName1").getAsString();
			
			String futureDatePlusThirty = GetDate.addDays(30);
			
			Thread.sleep(3000);
			SelectProductDLP.clickProductImage(jsonProductName);
			SelectProductDLP.clickOKButton();
			
			jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
			ProductDLP.selectSSByTitle(jsonSSName);
			
			GAUAssignment = new SetAssignmentGAU(driver);
			
			Thread.sleep(3000);
			GAUAssignment.removeAllGAUDates();
			
			Thread.sleep(3000);
			GAUAssignment.setGAUDate(futureDatePlusThirty);
		
		
			//Logout of NCIA application as an Instructor.
			
			NLTLogin.logoutNCIA();
		
		
			//Login to NLT Application as Scoring Teacher and verify STA is logged in successfully.
			
			NLTLogin = new LoginPage(driver);
			
			userName = jsonobject.getAsJsonObject("InstructorAssistantLoginCredentials").get("userSTA").getAsString();
			Password = jsonobject.getAsJsonObject("InstructorAssistantLoginCredentials").get("password1").getAsString();
			
			Thread.sleep(3000);
			NLTLogin.loginNLT(userName, Password);
			
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
			
			Thread.sleep(3000);
			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
		
		
			//Launch Beta Assignment View after selecting Course.
			
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(3000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
			
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
			Thread.sleep(3000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
		
		
			// As a Scoring Teacher Validate Logged in user email id.
			
			ViewBetaAssignment = new BetaAssignmentPage(driver);
			
			Thread.sleep(3000);
			ViewBetaAssignment.validateLoginEmailID(userName);
				
			
			//Logout of NLT Application as Scoring Teacher.
			
			NLTLogin.logoutNLT();
		

			//Login to NLT application as an Teacher Assistant and verify TA is logged in successfully.
							
			NLTLogin = new LoginPage(driver);
							
			userName = jsonobject.getAsJsonObject("InstructorAssistantLoginCredentials").get("userTA").getAsString();
			Password = jsonobject.getAsJsonObject("InstructorAssistantLoginCredentials").get("password2").getAsString();
							
			NLTLogin.loginNLT(userName, Password);
							
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));

			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
		
			//Launch Beta Assignment View after selecting Course.
							
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(2000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
							
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
			Thread.sleep(2000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
				
		
			// As an Teacher Assistant Validate Logged in user email id. 
			
			ViewBetaAssignment = new BetaAssignmentPage(driver);
			
			Thread.sleep(3000);
			ViewBetaAssignment.validateLoginEmailID(userName);
			
			
			//Login to NLT application as an Instructor.

			NLTLogin.logoutNLT();
		
		}
		
		// Closing driver and test.

		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
		
}


