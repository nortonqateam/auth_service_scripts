package com.Norton.LMS.Test;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.LMS.Pages.LMSCANVAS;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

public class LMSlogin_Instructor_Test extends PropertiesFile{
	LMSCANVAS LMSpage;
	NciaSignInPage Sign_Page;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.getURL(LMSURL);
		
		
	}

	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify that a new user coming through LMS should be able to login using product homepage links")
	@Stories("Login in to LMS Existing Instructor")
	@Test()
	public void Login_LMS() throws Exception{
		LMSpage = new LMSCANVAS(driver);
		LMSpage.instloginLMSCANVAS();
		LMSpage.clicklink("Dashboard");
		Thread.sleep(5000);
		LMSpage.clickStartNewCouseButton();
		LMSpage.enterCourseName();
		LMSpage.clickCreateCourseButton();
	
		LMSpage.clickNavigationlink("Settings");
		LMSpage.clickcourseDetailsTab("Apps");
		LMSpage.clickViewAppConfigurations();
		LMSpage.clickAppButton();
		LMSpage.enterAppInfo();
		LMSpage.enterAppInfoLaunchURL();
		Thread.sleep(5000);
		LMSpage.clickSubmitButton();
		LMSpage.clickNavigationlink("Assignments");
		LMSpage.clickAssignmentlink();
		LMSpage.enterAssignmentName();
		LMSpage.enterPoints("10");
		LMSpage.selectAssignmentGroup("Assignments");
		LMSpage.selectGrades("Points");
		LMSpage.selectSubmissionType("External Tool");
		LMSpage.enterExternalURL();
		//LMSpage.clickChekboxLoadThisToolInANewTab();
		LMSpage.clickSaveandPublishButton();
		Thread.sleep(6000);
		
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait waitx = new WebDriverWait(driver, 100);
		waitx.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("tool_content")));
		List<WebElement> iframetags = driver.findElements(By.tagName("iframe"));
		
		
		System.out.println( "The Number of frames are " + iframetags.size());
		for(WebElement el : iframetags){
		      //Returns the Id of a frame.
		        System.out.println("Frame Id :" + el.getAttribute("id"));
		      //Returns the Name of a frame.
		        System.out.println("Frame name :" + el.getAttribute("name"));
		    }
		/*JavascriptExecutor exe  =(JavascriptExecutor)driver;
		Integer noOfFrames = Integer.parseInt(exe.executeScript("return window.length").toString());
		System.out.println("The JS Number of iframes are " + noOfFrames);
		WebElement frameElement  = driver.findElement(By.id("tool_content"));
		String parentWindow = driver.getWindowHandle();
		System.out.println("Page Window Title" + driver.getTitle());
		for(String handles : driver.getWindowHandles()){
			System.out.println(handles);
			driver.switchTo().window(handles);
		}*/
		
		//
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("tool_content"));
		//driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
		//driver.switchTo().frame("activity_window");
		/*String childtFrame = driver.getWindowHandle();
		System.out.println("Child frame Title " + driver.getTitle());*/
		driver.switchTo().frame("tool_content");
		//WebDriver subDriver =driver.switchTo().frame("activity_window");
		ReusableMethods.checkPageIsReady(driver);
		WebElement clicklogin = driver.findElement(By.xpath("//button[@id='login_dialog_button']"));
		ReusableMethods.clickElement(driver, clicklogin);
		WebElement element = driver.findElement(By.xpath("//div/legend[@class='login_dialog_step_header']"));
		System.out.println(element.getText());
		
		//System.out.println("Inside Frame" + frameTitle);
		/*WebElement clicklogin = driver.findElement(By.xpath("//button[@id='login_dialog_button']"));
		ReusableMethods.clickElement(driver, clicklogin);*/
		driver.findElement(By.id("login_dialog_button")).click();
		driver.findElement(By.id("register_login_choice_login")).click();
	    driver.findElement(By.id("username")).click();
	   
	    driver.findElement(By.id("username")).sendKeys("spatil@wwnorton.com");
	   
	    driver.findElement(By.id("password")).click();
	    
	    driver.findElement(By.id("password")).sendKeys("Welcome1");
		//driver.findElement(By.xpath("//input[contains(@type,'email')]")).click();
		WebElement getEleText =driver.findElement(By.xpath("//div[@id='login_dialog_top_msg']"));
		System.out.println(getEleText.getText());
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Sign_Page.yes_SignIn);
		SignIn.click();
		SignIn.perform();
		String Auth_Inst_Email = jsonobject
				.getAsJsonObject("Existing_Instructor")
				.get("Authorize_Instructor_Email").getAsString();

		String Auth_Inst_Password = jsonobject
				.getAsJsonObject("Existing_Instructor")
				.get("Authorize_Instructor_Password").getAsString();

		Sign_Page.Username.sendKeys(Auth_Inst_Email);
		Sign_Page.Password.sendKeys(Auth_Inst_Password);
		Sign_Page.SignInButton.click();
		
		/*LMSpage.Assignmentlink.click();
		LMSpage.ClickAssignments.click();
		LMSpage.LoadAssignmentButton.click();
		String ChildWindow = null;
	    Set<String> Windowhandles= driver.getWindowHandles();
	    String ParentWindow = driver.getWindowHandle();
	    Windowhandles.remove(ParentWindow);
	    String winHandle=Windowhandles.iterator().next();
	    if (winHandle!=ParentWindow){
	    	ChildWindow=winHandle;
	    }
	    driver.switchTo().window(ChildWindow);*/
	    
	}
	
	public void switchToFrame(WebDriver driver, String parentWindow) throws Exception {
		String childWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		Windowhandles.remove(parentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != parentWindow) {
			childWindow = winHandle;
		}
		driver.switchTo().window(childWindow);

		driver.switchTo().frame("tool_content");
	}
	
	public void getFrameWindow(WebDriver driver, String frameName) throws Exception {
		String childWindow = null;
		for (String winHandle : driver.getWindowHandles()) {
			childWindow = winHandle;
			driver.switchTo().window(winHandle);
		}
		
		driver.switchTo().frame(frameName);
	}
}
