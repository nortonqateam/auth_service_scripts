package com.Norton.LMS.Test;

import java.util.Set;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;
import Utilities.ReusableMethods;

import com.Norton.LMS.Pages.LMSCANVAS;
import com.Norton.Smartwork5.Pages.Create_New_Student_Set_Page;
import com.Norton.Smartwork5.Pages.GearMenu_Page;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;

public class LMSLogin_IndividualActivityLinks_Test extends PropertiesFile {

	LMSCANVAS LMSpage;
	
	GearMenu_Page gearMenuPage;
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();
	Create_New_Student_Set_Page createstudentSet;
	String StudentSetID;
	NciaSignInPage Page;

	@Parameters("Browser")
	@BeforeTest
	// Call to Properties file initiate Browser and set Test URL.
	public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.getURL(LMSURL);

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify that a new user coming through LMS should be able to login using product homepage links")
	@Stories("Login in to LMS Existing Instructor")
	@Test()
	public void Login_LMS() throws Exception {
		LMSpage = new LMSCANVAS(driver);
		LMSpage.instloginLMSCANVAS();
		LMSpage.clicklink("Dashboard");
		Thread.sleep(5000);
		LMSpage.clickStartNewCouseButton();
		LMSpage.enterCourseName();
		LMSpage.clickCreateCourseButton();

		LMSpage.clickNavigationlink("Settings");
		LMSpage.clickcourseDetailsTab("Apps");
		LMSpage.clickViewAppConfigurations();
		LMSpage.clickAppButton();
		LMSpage.enterAppInfo();
		LMSpage.enterAppInfoLaunchURL();
		Thread.sleep(5000);
		LMSpage.clickSubmitButton();
		LMSpage.clickNavigationlink("Assignments");
		LMSpage.clickAssignmentlink();
		LMSpage.enterAssignmentName();
		LMSpage.enterPoints("10");
		LMSpage.selectAssignmentGroup("Assignments");
		LMSpage.selectGrades("Points");
		LMSpage.selectSubmissionType("External Tool");
		LMSpage.clickChekboxLoadThisToolInANewTab();
		LMSpage.enterExternalURL();
		LMSpage.clickSaveandPublishButton();
		Thread.sleep(3000);
		LMSpage.clickAssignmenthButton();
		ReusableMethods.checkPageIsReady(driver);

		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		driver.switchTo().window(ChildWindow);
		ReusableMethods.checkPageIsReady(driver);
		
		Page = new NciaSignInPage(driver);
		String UserName = jsonobject.getAsJsonObject("LMSUser").get("LMSINSTUserLogin")
				.getAsString();
		String Password = jsonobject.getAsJsonObject("LMSUser")
				.get("LMSINSTUserPassword").getAsString();
		Page.signInUserAccount(UserName, Password);
		WebElement loader = driver.findElement(By.id("loading_screen"));
		ReusableMethods.loadingWaitDisapper(driver, loader);
		ReusableMethods.checkPageIsReady(driver);
		Page.clickOkButton();
				

		createstudentSet = new Create_New_Student_Set_Page(driver);
		createstudentSet.createNewStudentset();
		createstudentSet.createStudentset_information();
	  //  StudentSetID= createstudentSet.createStudentset_ID();
		//LogUtil.log(StudentSetID);
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait wait1 = new WebDriverWait(driver, 100);
		WebElement loaderele = driver.findElement(By.id("loading_screen"));
		ReusableMethods.loadingWaitDisapper(driver, loaderele);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("lti_connection_badge")));
		WebElement connectedLMSText = driver.findElement(By
				.id("lti_connection_badge"));
		String LMSText = connectedLMSText.getText();
		Assert.assertTrue(LMSText.trim(), true);
		Thread.sleep(4000);
		
		Page.gear_button_username.click();
		gearMenuPage = new GearMenu_Page(driver);
		gearMenuPage.clickConnectedtoLMSlink();
		Page.clickOkButton();
		driver.close();
		driver.switchTo().window(ParentWindow);

		LMSpage.clickNavigationlink("Settings");
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();

	}

	@AfterTest
	public void Logout() {
		PropertiesFile.tearDownTest();
	}
}