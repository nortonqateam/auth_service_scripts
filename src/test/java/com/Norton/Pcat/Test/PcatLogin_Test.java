package com.Norton.Pcat.Test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.Pcat.Pages.Pcat_Page;

import Utilities.PropertiesFile;

public class PcatLogin_Test extends PropertiesFile {
	
	Pcat_Page Pcat_page;
	

	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setPcatURL();
		
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("TC-62 User with existing ID and access to PCAT should be able to login to PCAT after DB migration")
	@Stories("Login in to PCAT")
	@Test
	public void Login_ExistingUser_RegCode() throws Exception{
		Pcat_page= new Pcat_Page(driver);
		Pcat_page.loginto_Pcat();
		Pcat_page.authorize_Instructor_Account_link();
		Pcat_page.authorize_Instructor_Account();
		Pcat_page.authorize_Instructor_getText();
		Pcat_page.SignOut_Pcat();
		driver.close();
		
	}
	
	@AfterTest
    public void Logout(){
		PropertiesFile.tearDownTest();
	}
}
