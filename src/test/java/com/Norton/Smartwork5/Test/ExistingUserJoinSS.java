package com.Norton.Smartwork5.Test;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Norton.Smartwork5.Pages.CreateNewStudentSet;
import com.Norton.Smartwork5.Pages.CreateNewStudentandJoin;
import com.Norton.Smartwork5.Pages.GearMenu_Page;
import com.Norton.Smartwork5.Pages.LoginPage;
import com.Norton.Smartwork5.Pages.ManageStudentSetsPage;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;

public class ExistingUserJoinSS extends PropertiesFile {
	String studentSetID;
	LoginPage SW5Login;
	String userName;
	String Password;
	GearMenu_Page gearMenu;
	CreateNewStudentandJoin CSS;
	// Read data from Json File
		ReadJsonFile readJasonObject = new ReadJsonFile();
		JsonObject jsonobject = readJasonObject.readJason();
	
	@BeforeTest
	@Parameters({"Browser"})
	public void setUp() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();		
		PropertiesFile.setSW5URL();

	}
	@Severity(SeverityLevel.NORMAL)
	@Description("Log in as existing Student, Join new student set.. ")
	@Stories("Log in as existing Student, Join new student set..")
	@Test
	public void createNewStudentSetTest() throws Exception {
		createNewStudentSet();
		existingUserJoinSS();
		
	}
	public String createNewStudentSet() throws Exception {
		LoginAsInstructor logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		CreateNewStudentSet CSS = new CreateNewStudentSet(driver);
		Thread.sleep(5000);
		CSS.SignIn_Register();
		CSS.ManageStudentSetlink();
		CSS.CreateStudentSetButton();
		CSS.createNewStudentset();
		CSS.createStudentset_information();
		studentSetID = CSS.createStudentset_ID();
        LogUtil.log("The StudentSetID", studentSetID);
        Thread.sleep(2000);
		ManageStudentSetsPage managestudentsetpage = new ManageStudentSetsPage(driver);
		managestudentsetpage.clickcloselink();
		Thread.sleep(5000);
		CSS.logoutSmartwork5();
	

	return studentSetID;
}
	public String existingUserJoinSS() throws Exception{
		
		SW5Login = new LoginPage(driver);
		userName = jsonobject.getAsJsonObject("InstructorLogin").get("Instloginemail").getAsString();
		Password = jsonobject.getAsJsonObject("InstructorLogin").get("Instloginpassword").getAsString();
		SW5Login.loginSW5(userName, Password);
		
		wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.invisibilityOf(SW5Login.Overlay));
		wait.until(ExpectedConditions.visibilityOf(SW5Login.gear_button_username));
		String LoginMail = SW5Login.gear_button_username.getText().toLowerCase();
		SW5Login.verifySignin(userName, LoginMail);

		Thread.sleep(2000);
		
		// Navigate to Smartwork5 page
		
		Thread.sleep(5000);
		CreateNewStudentandJoin CSSJoin = new CreateNewStudentandJoin(driver);
		CSSJoin.clickSW5Icon();
		CSSJoin.joinstudentset();
		CSSJoin.enterStudentSetID(studentSetID);
		CSSJoin.studentsetinfo(studentSetID);
		CSSJoin.logoutSmartwork5();

		return userName;
	}

}
