package com.Norton.Smartwork5.Test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.Norton.Smartwork5.Pages.CreateNewStudentSet;
import com.Norton.Smartwork5.Pages.CreateNewStudentandJoin;
import com.Norton.Smartwork5.Pages.ManageStudentSetsPage;

import Utilities.LogUtil;
import Utilities.PropertiesFile;
@Listeners({Utilities.TestListener.class })
public class CreateStudentSet extends PropertiesFile {
	String studentSetID;
	
	@BeforeTest
	@Parameters({"Browser"})
	public void setUp() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();		
		PropertiesFile.setSW5URL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Login to application as a Instructor , Create a New Student set. ")
	@Stories("Login to application as a Instructor , Create a New Student set.")
	@Test
	public void createNewStudentSetTest() throws Exception {
		createNewStudentSet();
		CreateUserandJoinSS();
	}
	public String createNewStudentSet() throws Exception {
			LoginAsInstructor logininst = new LoginAsInstructor();
			logininst.loginInstructor();
			CreateNewStudentSet CSS = new CreateNewStudentSet(driver);
			Thread.sleep(5000);
			CSS.SignIn_Register();
			CSS.ManageStudentSetlink();
			CSS.CreateStudentSetButton();
			CSS.createNewStudentset();
			CSS.createStudentset_information();
			studentSetID = CSS.createStudentset_ID();
            LogUtil.log("The StudentSetID", studentSetID);
            Thread.sleep(2000);
			ManageStudentSetsPage managestudentsetpage = new ManageStudentSetsPage(driver);
			managestudentsetpage.clickcloselink();
			Thread.sleep(5000);
			CSS.logoutSmartwork5();
		

		return studentSetID;
	}
	
public String CreateUserandJoinSS () throws Exception{
CreateNewStudentandJoin CSSJoin = new CreateNewStudentandJoin(driver);
	
	Thread.sleep(1000);
	String studentUserName = CSSJoin.CreateNewUser();
	CSSJoin.clickSW5Icon();
	CSSJoin.joinstudentset();
	CSSJoin.enterStudentSetID(studentSetID);
	CSSJoin.studentsetinfo(studentSetID);
	CSSJoin.logoutSmartwork5();

	return studentUserName;
	}
	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
