package com.Norton.Smartwork5.Test;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import Utilities.PropertiesFile;
import Utilities.ReadJsonFile;



import com.Norton.Smartwork5.Pages.CreateNewStudentandJoin;
import com.Norton.Smartwork5.Pages.GearMenu_Page;
import com.Norton.Smartwork5.Pages.LoginPage;
import com.Norton.Smartwork5.Pages.NciaSignInPage;
import com.google.gson.JsonObject;


@Listeners({Utilities.TestListener.class })
public class LoginAsInstructor extends PropertiesFile {

	LoginPage SW5Login;
	String userName;
	String Password;
	GearMenu_Page gearMenu;
	CreateNewStudentandJoin CSS;
	NciaSignInPage Sign_Page;
	
	@BeforeTest
	@Parameters({"Browser"})
	public void setUp() throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();		
		PropertiesFile.setSW5URL();

	}

	// Read data from Json File
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonobject = readJasonObject.readJason();

	// Allure Annotations

	@Severity(SeverityLevel.NORMAL)
	@Description("Login as a Instructor")
	@Stories("Login to application as a Instructor ")
	@Test(priority = 0)
	public void loginInstructor() throws Exception {
		SW5Login = new LoginPage(driver);
		userName = jsonobject.getAsJsonObject("InstructorLogin").get("Instloginemail").getAsString();
		Password = jsonobject.getAsJsonObject("InstructorLogin").get("Instloginpassword").getAsString();
		SW5Login.loginSW5(userName, Password);
		
		wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.invisibilityOf(SW5Login.Overlay));
		wait.until(ExpectedConditions.visibilityOf(SW5Login.gear_button_username));
		String LoginMail = SW5Login.gear_button_username.getText().toLowerCase();
		SW5Login.verifySignin(userName, LoginMail);

		Thread.sleep(2000);
		
		// Navigate to Smartwork5 page
		SW5Login.clickSW5Icon();
		Thread.sleep(5000);
	}

	@Test(priority = 1)
	public void logoutInstructor() throws Exception {
		SW5Login.logoutSW5();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
